<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company'];

// Load list of kitchens from this company
	$query = "SELECT * FROM lbc_foodtypes WHERE id_company = '$id_company' ORDER BY title ASC";
	$foodtypes = mysql_query($query, $db) or die(mysql_error());
	$row_foodtypes = mysql_fetch_assoc($foodtypes);
	$numberRowsFoodTypes  = mysql_num_rows($foodtypes);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

          <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>
		    
    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; if($id_company == 'lightblue') {sideBar('admin','type_food');} else {sideBar('settings','type_food');} ?>

            <section id="content">
                <div class="container">

<? if($_GET['s'] == "added") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully added a new type of food.
</div>
<? } ?>

<? if($_GET['s'] == "updated") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully updated the type of food.
</div>
<? } ?>

<? if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully deleted the type of food.
</div>
<? } ?>

<? if($_GET['s'] != "addnew") { ?>
                    <div class="card">

               <div class="card-header">
                <h2>Manage type of food<small>Click on the title for modifications</small></h2>

                  <ul class="actions">
                            <li>
                                <a href="manage_foodtypes.php?s=addnew">
                                    <i class="zmdi zmdi-plus"></i>
                                </a>
                            </li>
                  </ul>
               </div>

          <div class="card-body card-padding">
<? if($numberRowsFoodTypes != "0") { ?>

            <div class="contacts clearfix row">

			<? $n=1; do { ?>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="c-item">

                        <a href="update_foodtypes.php?id_foodtype=<? echo $row_foodtypes['id_foodtype']; ?>"  class="ci-avatar">
                           <img src="images/<? echo $row_foodtypes['image_url']; ?>" alt="" style="border:solid 1px #e2e2e2; height:130px;">
                        </a>

                        <div class="c-info">
                            <strong><a href="update_foodtypes.php?id_foodtype=<? echo $row_foodtypes['id_foodtype']; ?>" style="color:black;"><? echo $row_foodtypes['title']; ?></a> </strong><br>
                        </div>
                    </div>
                </div>
             <? $n++; } while ($row_foodtypes = mysql_fetch_assoc($foodtypes));?>

           </div>




						<? } else { ?>

                        <p align="center">No food type have been registered yet.</p>

						<? } ?>
                     </div>

                  </div>

                    <? } else { ?>
                  <form action="sql_foodtypes_add.php" method="post" enctype="multipart/form-data" id="addfoodtypes">

<div class="card">

    <div class="card-header">
    	<h2>Title</h2>

        <ul class="actions">
            <li>
                <a href="manage_foodtypes.php">
                <i class="zmdi zmdi-close"></i>
                </a>
            </li>
        </ul>
    </div>

    <div class="card-body card-padding">

        <div class="form-group">
          <div class="fg-line">
          <input type="text" class="form-control" name="title">
          </div>
        </div>

    </div>
</div>

<div class="card">

    <div class="card-header">
    	<h2>Upload image</h2>
    </div>

    <div class="card-body card-padding">

        <div class="fileinput fileinput-new" data-provides="fileinput">
        <span class="btn bgm-lightblue btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
        <input type="file" name="url_en" id="url_en"></span>
        <span class="fileinput-filename"></span><a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
        </div>

    </div>
</div>

<div class="card">

    <div class="card-header">
    	<h2>Keywords</h2>
    </div>

    <div class="card-body card-padding">

    <div class="form-group">
    <div class="fg-line">
    <textarea class="form-control auto-size" data-autosize-on="true" name="keywords"></textarea>
    </div>
    </div>

    </div>
</div>

    <div align="center">
    <button type="submit" class="btn btn-default bgm-lightblue btn-icon-text">
    <i class="zmdi zmdi-check-all"></i>Add new</button>
    </div>


					</form>
                      <? } ?>
                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

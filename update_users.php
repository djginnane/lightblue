<? include 'database.php'; $id_user = $_GET['id_user']; $id_company = $_SESSION['id_company'];

	// Load specific user from this company
	$query = "SELECT * FROM lbc_users WHERE id_user = '$id_user'";
	$users = mysql_query($query, $db) or die(mysql_error());
	$row_users = mysql_fetch_assoc($users);
	$numberRowsUsers  = mysql_num_rows($users);

	// Load all list of users from this company
	$query_users_all = "SELECT * FROM lbc_users WHERE id_company = '$id_company'";
	$users_all = mysql_query($query_users_all, $db) or die(mysql_error());
	$row_users_all = mysql_fetch_assoc($users_all);
	$numberRowsUsersAll  = mysql_num_rows($users_all);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

				<!-- Google Analytics -->
				<?php include ('gtag.php'); ?>

    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('settings','manage_users'); ?>

            <section id="content">
                <div class="container">

                    <div class="block-header">
                        <h2>Update user</h2>
                        <? if($numberRowsUsersAll != 1) { ?>
                        <ul class="actions">
                            <li>
                                <a onclick="return confirm('Are you sure you want to delete the account <? echo $row_users['username']; ?>?');" href="sql_user_delete.php?id_user=<? echo $row_users['id_user']; ?>">
                                    <i class="zmdi zmdi-delete"></i>
                                </a>
                            </li>
                        </ul>
                        <? } ?>
                    </div>


                  <form action="sql_user_update.php" method="post" id="updateuser">
                  <input type="hidden" name="id_user" value="<? echo $row_users['id_user']; ?>">
                  <input type="hidden" name="username" value="<? echo $row_users['username']; ?>">

                    <div class="card">
                        <div class="card-body card-padding">

                            <div class="row">
                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line disabled"><input type="text" class="input-sm form-control fg-input" disabled name="username" value="<? echo $row_users['username']; ?>" style="color:black;"></div>
                                  <label class="fg-label">Username</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="password" class="input-sm form-control fg-input" name="password" value="<? echo $row_users['password']; ?>"></div>
                                  <label class="fg-label">Password</label>
                                  </div>

                                </div>
                            </div>
 <div class="form-group fg-float">
                                        <div class="fg-line">
                                            <div class="select">
                                                <select class="form-control" name="type">
              <!--<option value="admin" <? // if($row_users['type'] == "admin") {echo "selected";} ?>>Admin</option>-->
              <option value="employee" <? if($row_users['type'] == "employee") {echo "selected";} ?>>Employee</option>
              <option value="steward" <? if($row_users['type'] == "steward") {echo "selected";} ?>>Steward</option>
              <option value="champion" <? if($row_users['type'] == "champion") {echo "selected";} ?>>Champion</option>
              <option value="secretarygeneral" <? if($row_users['type'] == "secretarygeneral") {echo "selected";} ?>>Secretary general</option>
                                                </select>
                                            </div>
                                        </div>
                                        <label class="fg-label">Account type</label>
                                    </div>
                        </div>
					</div>


                     <div class="card">
                        <div class="card-body card-padding">

                            <div class="row">
                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="name" value="<? echo $row_users['name']; ?>"></div>
                                  <label class="fg-label">Name</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="surname" value="<? echo $row_users['surname']; ?>"></div>
                                  <label class="fg-label">Surname</label>
                                  </div>

                                </div>

                                 <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="company" value="<? echo $row_users['company']; ?>"></div>
                                  <label class="fg-label">Company</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="department" value="<? echo $row_users['department']; ?>"></div>
                                  <label class="fg-label">Department</label>
                                  </div>

                                </div>

                                  <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="email" class="input-sm form-control fg-input" name="email" value="<? echo $row_users['email']; ?>"></div>
                                  <label class="fg-label">Email</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="phone" value="<? echo $row_users['phone']; ?>"></div>
                                  <label class="fg-label">Mobile</label>
                                  </div>

                                </div>
                            </div>

                        </div>
					</div>


<button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
<i class="zmdi zmdi-check-all"></i>Update User</button>
</form>


                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

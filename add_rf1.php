<?php
	/* Includes */
	include('mysqli.php');

	/* Variables */
	$id_user = $_SESSION['username'];
	$id_company = $_SESSION['id_company'];

	/* Query */
	$query = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
	$kitchen = $db->query($query);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

        <?php echo "<script>"; include 'scripts/checkselect.js'; echo "</script>"; ?>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Accept only numbers but NO decimals "." -->
        <script>
			function isNumberKey(evt){
	   		var charCode = (evt.which) ? evt.which : event.keyCode
	    	 if (charCode > 31 && (charCode < 48 || charCode > 57))
	        return false;
	    	return true;
			}
        </script>

        <!-- Form validation for empty fields -->
        <script>

		function validateForm() {

	    var kitchen = document.forms["add_waste"]["kitchen"].value;
			var shiftv = document.forms["add_waste"]["shift"].value;

			var weight_1 = document.forms["add_waste"]["weight_1"].value;
			var type_food_1 = document.forms["add_waste"]["type_food_1"].value;

			var weight_2 = document.forms["add_waste"]["weight_2"].value;
			var type_food_2 = document.forms["add_waste"]["type_food_2"].value;

			var weight_3 = document.forms["add_waste"]["weight_3"].value;
			var type_food_3 = document.forms["add_waste"]["type_food_3"].value;

			var weight_4 = document.forms["add_waste"]["weight_4"].value;
			var type_food_4 = document.forms["add_waste"]["type_food_4"].value;

			var weight_5 = document.forms["add_waste"]["weight_5"].value;
			var type_food_5 = document.forms["add_waste"]["type_food_5"].value;

			// Weight errors messages
			if((weight_1 != "") && ((weight_1.length < 2) ||(weight_1.length > 5))) {alert("Waste weight can't be inferior to 10g or superior to 99 999g"); return false;}
			if((weight_2 != "") && ((weight_2.length < 2) ||(weight_2.length > 5))) {alert("Waste weight can't be inferior to 10g or superior to 99 999g"); return false;}
			if((weight_3 != "") && ((weight_3.length < 2) ||(weight_3.length > 5))) {alert("Waste weight can't be inferior to 10g or superior to 99 999g"); return false;}
			if((weight_4 != "") && ((weight_4.length < 2) ||(weight_4.length > 5))) {alert("Waste weight can't be inferior to 10g or superior to 99 999g"); return false;}
			if((weight_5 != "") && ((weight_5.length < 2) ||(weight_5.length > 5))) {alert("Waste weight can't be inferior to 10g or superior to 99 999g"); return false;}

			// All required fields must be filled-in
			if(kitchen == null || kitchen == "" || shiftv == null || shiftv == "" || weight_1 == null || weight_1 == "" || type_food_1 == null || type_food_1 == "") {
				alert("All required fields must be filled-in.");
				return false;
			}

			// Food waste type must be selected
			if (document.getElementById("spoilage").checked == true || document.getElementById("preparation").checked == true || document.getElementById("buffet").checked == true) {

				return true;

			} else {

				alert("Please select a food waste type.");
				return false;
			}
		}
		</script>

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>

    </head>
    <body>

       <?php include 'header.php';?>

       <section id="main">

           <?php include('sidebar.php'); sideBar('wasterecords','rf1'); ?>

            <section id="content">
                <div class="container">


	<?php if($_GET['s'] == "added") { ?>
		<div class="alert alert-success alert-dismissible" role="alert" id="hide">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Waste(s) have been recorded successfully.
		</div>
	<?php } ?>

	<?php if($_GET['s'] == "updated") { ?>
		<div class="alert alert-success alert-dismissible" role="alert" id="hide">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Waste(s) have been updated successfully.
		</div>
	<?php } ?>

	<?php if($_GET['s'] == "deleted") { ?>
		<div class="alert alert-success alert-dismissible" role="alert" id="hide">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		Waste(s) have been deleted successfully.
		</div>
	<?php } ?>

                     <div class="block-header">
                        <h2>RF1 - Food waste record</h2>

                        <ul class="actions">
                        <li class="dropdown action-show">
                                    <a href="" data-toggle="dropdown">
                                        <i class="zmdi zmdi-info" ></i>
                                    </a>

                                    <div class="dropdown-menu pull-right" style="width:320px;">
                                        <p class="p-20">
<strong>Spoilage</strong>
<br><br>
Anything from the kitchen + STORAGE that has gone off (out of date) or has been contaminated and is unusable, including front-of house items e.g. melted butter or mouldy bread.
<br><br>
<strong>Prep waste</strong>
<br><br>
Anything that could be used but is thrown out. This includes meals cooked for customers that don’t get served (e.g. the food is overcooked and not suitable to serve, or over-preparation).
<br><br>
<strong>Customer waste</strong>
<br><br>
Anything that comes back from the customer, including meals that have been untouched.
<br><br>
<strong>Buffet waste</strong>
<br><br>
Leftover food from the buffet line that cannot be safely stored and reused.
<br><br>
<strong>Non consumable waste</strong>
<br><br>
Fruit and vegetables peelings, poultry and fish skin, bones, etc. Dispose of in a separate bin, as these category of waste should not be mixed with Food waste. It is useful to record this information for future plans to dispose of food waste via composting and anaerobic digestion.
                                        </p>
                                    </div>
                                </li>
                           <li>
                                <a href="manage_rf1.php">
                                    <i class="zmdi zmdi-view-list"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

<form action="sql_add_rf1.php" method="POST" id="add_waste" onsubmit="return validateForm()">

<!-- Hidden inputs -->
<input type="hidden" name="id_user" value="<?php echo $_SESSION['username']; ?>">
<input type="hidden" name="id_company" value="<?php echo $_SESSION['id_company']; ?>">
<!-- Hidden inputs -->

                   <div class="card">

                        <div class="card-body card-padding">

                            <div class="row">
                                <div class="col-sm-4">

                                  <div class="input-group form-group" >
                                  <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                  <div class="dtp-container fg-line">
     							  <input type='text' class="form-control date-picker" placeholder="Date" id="date" name="date_waste" value="<?php if($_SESSION['date_waste'] == "") { echo date('d/m/Y');} else {echo $_SESSION['date_waste'];}?>">
                                  </div>
                                  </div>

                                </div>

																<div class="col-sm-4">
																	<div class="input-group form-group">
																		<span class="input-group-addon"><i class="zmdi zmdi-cutlery"></i></span>
																			<div class="fg-line select">
																			<select class="form-control" name="kitchen">
																				<?php if($_SESSION['kitchen'] == "") {?><option value="">Select recording station / kitchen</option><?php } ?>
																				<?php if($kitchen->num_rows > 0) { ?>
																						<?php while($row_kitchen = $kitchen->fetch_assoc()) { ?>
																						<option <?php if($_SESSION['kitchen'] == $row_kitchen['kitchen']) {echo "selected";} ?> value="<?php echo $row_kitchen['kitchen']; ?>"><?php echo $row_kitchen['kitchen']; ?></option>
																						<?php } ?>
																				<?php } ?>
																			</select>
																		</div>
																	</div>
		                            </div>

                            <div class="col-sm-4">

								<div class="input-group form-group">
								<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
								<div class="fg-line select">
									<select class="form-control" name="shift" required>
										<?php if($_SESSION['shift'] == "") {?><option value="">Select shift</option> <?php } ?>
										<option <?php if($_SESSION['shift'] == "Breakfast") {echo "selected";} ?> value="Breakfast">Breakfast</option>
										<option <?php if($_SESSION['shift'] == "Lunch") {echo "selected";} ?> value="Lunch">Lunch</option>
										<option <?php if($_SESSION['shift'] == "Dinner") {echo "selected";} ?> value="Dinner">Dinner</option>
										<option <?php if($_SESSION['shift'] == "Night shift") {echo "selected";} ?> value="Night shift">Night shift</option>
									</select>
								</div>
								</div>

                            </div>
                            </div>
                        </div>
					</div>

                     <div class="card">


                       <div class="card-body card-padding">
                       <br><br>
                        <div class="row">

                        <div class="col-sm-4 m-b-5">

                       		<label class="radio radio-inline m-r-20">
                                <input type="radio" name="type_waste" id="spoilage" value="spoilage" onClick="TypeA(this); checkReasonType(this);">
                               <i class="input-helper"></i>
                               <div style="margin-top:-10px;">
                                <i class="zmdi zmdi-delete" style="color:#000; font-size:20px;"></i>
                                <strong><span style="color:#000; font-size:20px;">   SPOILAGE</span></strong>
                                </div>
                            </label>
                       	</div>

                        <div class="col-sm-4 m-b-5">
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="type_waste" id="preparation" value="preparation" onClick="TypeA(this); checkReasonType(this);">
                                <i class="input-helper"></i>
                                <div style="margin-top:-10px;">
                                <i class="zmdi zmdi-delete" style="color:#FFC107;  font-size:20px;"></i>
                                <strong><span style="color:#FFC107;  font-size:20px;">   PREPARATION</span></strong>
                               </div>


                            </label>
                       	</div>

                        <div class="col-sm-4 m-b-5">
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="type_waste" id="buffet" value="buffet" onClick="TypeB(this); checkReasonType(this);">
                                <i class="input-helper bgm-black"></i>
                                <div style="margin-top:-10px;">
                                <i class="zmdi zmdi-delete" style="color:#F44336; font-size:20px;"></i>
                                <strong><span style="color:#F44336; font-size:20px;">   BUFFET</span></strong>
                                </div>
                            </label>
                       	</div>
                        </div>
                        <br><br>

	<div class="row">
		<div class="col-sm-6">
			<div class="input-group">
				<div class="fg-line">
					<input type="text" class="form-control" placeholder="eg. 355" name="weight_1" onKeyPress="return isNumberKey(event)" autocomplete="off">
				</div>
				<span class="input-group-addon first"><strong>g</strong></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
				<div class="fg-line select">
					<select class="form-control" name="type_food_1" id="type_food">
						<option value="" selected="">Select type</option>
						<option value="Staple food">Staple food</option>
						<option value="Vegetable">Vegetable</option>
						<option value="Fruit">Fruit</option>
						<option value="Meat">Meat</option>
						<option value="Seafood">Seafood</option>
						<option value="Dairy">Dairy</option>
						<option value="Others">Others</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="input-group">
				<div class="fg-line">
					<input type="text" class="form-control" placeholder="eg. 355" name="weight_2" onKeyPress="return isNumberKey(event)" autocomplete="off">
				</div>
				<span class="input-group-addon first"><strong>g</strong></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
				<div class="fg-line select">
					<select class="form-control" name="type_food_2" id="type_food">
						<option value="" selected="">Select type</option>
						<option value="Staple food">Staple food</option>
						<option value="Vegetable">Vegetable</option>
						<option value="Fruit">Fruit</option>
						<option value="Meat">Meat</option>
						<option value="Seafood">Seafood</option>
						<option value="Dairy">Dairy</option>
						<option value="Others">Others</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="input-group">
				<div class="fg-line">
					<input type="text" class="form-control" placeholder="eg. 355" name="weight_3" onKeyPress="return isNumberKey(event)" autocomplete="off">
				</div>
				<span class="input-group-addon first"><strong>g</strong></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
				<div class="fg-line select">
					<select class="form-control" name="type_food_3" id="type_food">
						<option value="" selected="">Select type</option>
						<option value="Staple food">Staple food</option>
						<option value="Vegetable">Vegetable</option>
						<option value="Fruit">Fruit</option>
						<option value="Meat">Meat</option>
						<option value="Seafood">Seafood</option>
						<option value="Dairy">Dairy</option>
						<option value="Others">Others</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="input-group">
				<div class="fg-line">
					<input type="text" class="form-control" placeholder="eg. 355" name="weight_4" onKeyPress="return isNumberKey(event)" autocomplete="off">
				</div>
				<span class="input-group-addon first"><strong>g</strong></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
				<div class="fg-line select">
					<select class="form-control" name="type_food_4" id="type_food">
						<option value="" selected="">Select type</option>
						<option value="Staple food">Staple food</option>
						<option value="Vegetable">Vegetable</option>
						<option value="Fruit">Fruit</option>
						<option value="Meat">Meat</option>
						<option value="Seafood">Seafood</option>
						<option value="Dairy">Dairy</option>
						<option value="Others">Others</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6">
			<div class="input-group">
				<div class="fg-line">
					<input type="text" class="form-control" placeholder="eg. 355" name="weight_5" onKeyPress="return isNumberKey(event)" autocomplete="off">
				</div>
				<span class="input-group-addon first"><strong>g</strong></span>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
				<div class="fg-line select">
					<select class="form-control" name="type_food_5" id="type_food">
						<option value="" selected="">Select type</option>
						<option value="Staple food">Staple food</option>
						<option value="Vegetable">Vegetable</option>
						<option value="Fruit">Fruit</option>
						<option value="Meat">Meat</option>
						<option value="Seafood">Seafood</option>
						<option value="Dairy">Dairy</option>
						<option value="Others">Others</option>
					</select>
				</div>
			</div>
		</div>
	</div>

	</div></div>

					<button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
						<i class="zmdi zmdi-cloud-upload"></i>Record waste
					</button>

        </form>

                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

<?php

  /* Includes */
  include('database.php');
  include('functions.php');
  include('sidebar.php');
  include('functions/es_summary.php');

  if($_SESSION['baseline'] == "true"){
    $money_saved = $_SESSION['money_saved'];
    $meals_rescued = $_SESSION['meals_rescued'];
    $carbon_reduction = $_SESSION['carbon_reduction'];
  }

  /* Variables */
  $id_user = $_SESSION['username'];
  $id_company = $_SESSION['id_company'];
  $fw_preconsumer_percent = $_SESSION['fw_preconsumer_percent'];

  /* Timezone */
  date_default_timezone_set("Asia/Bangkok");

  /* Manage dates */

  // If date is sent as GET variables
  if(($_GET['date_from'] != '') && ($_GET['date_from'] != '')) {
    $_SESSION['date_from'] = $_GET['date_from'];
    $date_from = $_GET['date_from'];

    $_SESSION['date_to'] = $_GET['date_to'];
    $date_to = $_GET['date_to'];
  }

  // If date is not sent as GET, check if there is a SESSION value
  elseif (($_SESSION['date_from'] != '') && ($_SESSION['date_from'] != '')) {
    $date_from = $_SESSION['date_from'];
    $date_to = $_SESSION['date_to'];
  }

  // Else, if no GET or SESSION value load minimum and maximum value and set as default
  else {

    $db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
    if ($db->connect_error) { die("Connection failed: " . $db->connect_error);}

    // Minimum date
    $sql = "SELECT MIN(date_waste) FROM lbc_rf3 WHERE id_company = '$id_company'";
    $result = $db->query($sql); $from = mysqli_fetch_array($result);

    $_SESSION['date_from'] = $from[0];
    $date_from = $from[0];

    // Maximum date
    $sql_to = "SELECT MAX(date_waste) FROM lbc_rf3 WHERE id_company = '$id_company'";
    $result_to = $db->query($sql_to); $to = mysqli_fetch_array($result_to);

    $_SESSION['date_to'] = $to[0];
    $date_to = $to[0];
  }

  // Dates displayed
  $date_from_display = date("M jS, Y", strtotime($date_from));
  $date_to_display = date("M jS, Y", strtotime($date_to));
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

        <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include('gtag.php'); ?>
    </head>

    <body>

      <?php include('header.php');?>

      <section id="main">

        <?php sideBar('executivesummary','report_total'); ?>

        <section id="content">
        <div class="container">

        <div class="block-header">
            <h2>Executive Summary of food waste<small><?php echo $date_from_display.' to '. $date_to_display;?></small></h2>

            <ul class="actions">
            <li><a href="#" onclick="window.print();"><i class="zmdi zmdi-print"></i></a></li>
            </ul>
        </div>


        <div class="card">
            <div class="p-20">
                <div class="row">
                    <div class="col-sm-6" style="margin-bottom: 15px;">

                        <?php if($_SESSION['baseline'] == "true"){?>
                          <strong>Average daily food waste</strong>
                          <h3 class="m-0 f-400"><?php AverageDailyFoodWaste();?> kg</h3><br>
                        <?php } else { ?>
                          <strong>Total Food Waste</strong>
                          <h3 class="m-0 f-400"><?php TotalFoodWaste();?> kg</h3><br>
                        <?php } ?>

                        <strong>% Pre-consumer Food Waste</strong>
                        <h3 class="m-0 f-400"><?php echo $fw_preconsumer_percent; ?></h3><br>

                        <strong> Food Waste per cover</strong>
                        <h3 class="m-0 f-400"><?php allWasteCover('lbc_rf3', $id_company,'Canteen'); ?> g/cover</h3>
                    </div>

                    <div class="col-sm-6">

                        <?php if($_SESSION['baseline'] == "true"){?>
                          <img src="images/co2-inside-cloud.png" width="30" height="30"/>
                          <strong>  &nbsp;&nbsp;Carbon Reduction</strong>
                          <h3 class="m-0 f-400"><?php echo number_format($carbon_reduction,0,".",","); ?> kg</h3><br>
                        <?php } else { ?>
                          <img src="images/co2-inside-cloud.png" width="30" height="30"/>
                          <strong>  &nbsp;&nbsp;Co2 Equivalent</strong>
                          <h3 class="m-0 f-400"><?php ExecutiveSummaryCO2Equivalent($id_company); ?> kg</h3><br>
                        <?php } ?>

                        <?php if($_SESSION['baseline'] == "true"){?>
                          <img src="images/touching-belly-silhouette.png" width="30" height="30"/>
                          <strong>  &nbsp;&nbsp;Number of meals rescued</strong>
                          <h3 class="m-0 f-400"><?php echo number_format($meals_rescued,0,".",","); ?> meals</h3><br>
                        <?php } else { ?>
                          <img src="images/touching-belly-silhouette.png" width="30" height="30"/>
                          <strong>  &nbsp;&nbsp;Meals that can be rescued</strong>
                          <h3 class="m-0 f-400"><?php ExecutiveSummaryFamilyFedEquivalent($id_company); ?> meals</h3><br>
                        <?php } ?>

                        <?php if($_SESSION['baseline'] == "true"){ ?>
                          <img src="images/give-money.png" width="30" height="30"/>
                          <strong> &nbsp;&nbsp;Money saved</strong>
                          <h3 class="m-0 f-400"><?php echo number_format($money_saved,0,".",","); ?> USD</h3>
                        <?php } else { ?>
                          <img src="images/give-money.png" width="30" height="30"/>
                          <strong> &nbsp;&nbsp;Money loss</strong>
                          <h3 class="m-0 f-400"><?php ExecutiveSummaryCashEquivalent($id_company); ?> USD</h3>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>



        <div class="card">
            <iframe height="400px" width="100%" src="chart.php?type=category_total01" frameborder="0"></iframe>
        </div>


        <div class="card">
            <div class="card-body card-padding">
                <div class="row">
                    <p class="f-500 m-b-20 c-black">Most discarded type of food</p>

                    <ul class="list-group">
                        <?php
                        /* Connect to DB */
                        $db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
                        if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

                        /* Query */
                        $sql = "SELECT *, SUM(weight) FROM lbc_rf1 WHERE id_company = '$id_company' GROUP BY type_food ORDER BY SUM(weight) DESC";
                        if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

                        $numRows = $result->num_rows;
                        $affectedRows = $db->affected_rows;

                        /* Total weight */
                        $sql_total = "SELECT *, SUM(weight) FROM lbc_rf1 WHERE id_company = '$id_company' AND
                        (type_food = 'Staple food' OR
                        type_food = 'Vegetable' OR
                        type_food = 'Others' OR
                        type_food = 'Meat' OR
                        type_food = 'Seafood' OR
                        type_food = 'Dairy' OR
                        type_food = 'Fruit')";
                        if(!$result_total = $db->query($sql_total)){ die('There was an error running the query [' . $db->error . ']');}
                        $row_total = mysqli_fetch_assoc($result_total);
                        $type_food_total = $row_total['SUM(weight)'] / 1000;

                        /* Display results */
                        $i = 1;

                        while($row = $result->fetch_assoc()){

                            if($row['type_food'] == "Staple food") { $icon = '<img src="images/icons/staplefood.png" width="30">'; }
                            elseif($row['type_food'] == "Vegetable") { $icon = '<img src="images/icons/vegetable.png" width="30">'; }
                            elseif($row['type_food'] == "Fruit") { $icon = '<img src="images/icons/fruit.png" width="30">'; }
                            elseif($row['type_food'] == "Meat"){ $icon = '<img src="images/icons/meat.png" width="30">'; }
                            elseif($row['type_food'] == "Seafood") { $icon = '<img src="images/icons/seafood.png" width="30">'; }
                            elseif($row['type_food'] == "Dairy") { $icon = '<img src="images/icons/dairy.png" width="30">'; }
                            elseif($row['type_food'] == "Others") { $icon = '<img src="images/icons/others.png" width="30">'; }
                            else {$icon = '';}

                            if($row['type_food'] != "") {
                                echo '<li class="list-group-item">'.$icon;
                                echo "       ".$row['type_food']." ("; ;
                                $weightKG_tf = $row['SUM(weight)'] / 1000;
                                $weightPercent = ($weightKG_tf / $type_food_total) * 100;
                                echo number_format($weightPercent,0,",",".")." %)";
                                echo '</li>';
                                $i++;
                            }
                        }?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <iframe height="400px" width="100%" src="chart.php?type=category_total02" frameborder="0"></iframe>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="card">
                    <iframe height="400px" width="100%" src="chart.php?type=category_total03" frameborder="0"></iframe>
                </div>
            </div>

        </div>
        </div>

        </section>
        </section>

      <!-- Javascript Libraries -->
      <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
      <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

      <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
      <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
      <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
      <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
      <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

      <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
      <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
      <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
      <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
      <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
      <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
      <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

      <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
      <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
      <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
      <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
      <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
      <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

      <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
      <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
      <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
      <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

      <script src="scripts/js/functions.js"></script>

    </body>
  </html>

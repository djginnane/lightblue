<? include 'database.php'; $id_user = $_SESSION['username'];?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <link href="vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css" rel="stylesheet">
        <link href="vendors/bower_components/mediaelement/build/mediaelementplayer.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include ('gtag.php'); ?>

    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('help',''); ?>

            <section id="content">
                <div class="container">

                    <div class="card">

               <div class="card-header">
                  <h2>Tutorials<small>Food Waste Program</small></h2>
               </div>

                  <div class="card-body card-padding">
                  <div class="row">
                  <div class="col-md-6 m-b-20">

                  <p class="f-500 c-black m-b-20">Training of Implementation</p>

                  <iframe width="100%" height="300" src="https://www.youtube.com/embed/-Xc-RyL_TyQ" frameborder="0" allowfullscreen></iframe>

                  </div>

                  <div class="col-md-6 m-b-20">

                 <p class="f-500 c-black m-b-20">No Food Here</p>

                 <iframe width="100%" height="300" src="https://www.youtube.com/embed/RRat3rh0vHw" frameborder="0" allowfullscreen></iframe>

                  </div>

                  <div class="col-md-6 m-b-20">

                  <p class="f-500 c-black m-b-20">Preparation Food Waste</p>

                  <iframe width="100%" height="300" src="https://www.youtube.com/embed/kvQ4G9vcVDk" frameborder="0" allowfullscreen></iframe>

                  </div>

                  <div class="col-md-6 m-b-20">

                  <p class="f-500 c-black m-b-20">Preparation Food Waste (Thai)</p>

                  <iframe width="100%" height="300" src="https://www.youtube.com/embed/FrDY_OEYScQ" frameborder="0" allowfullscreen></iframe>

                  </div>

                  <div class="col-md-6 m-b-20">

                  <p class="f-500 c-black m-b-20">Spoilage Food Waste</p>

                  <iframe width="100%" height="300" src="https://www.youtube.com/embed/g2-4WekeRTQ" frameborder="0" allowfullscreen></iframe>

                  </div>

                  <div class="col-md-6 m-b-20">

                  <p class="f-500 c-black m-b-20">Stewards Instructional Video</p>

                  <iframe width="100%" height="300" src="https://www.youtube.com/embed/fDBzEyABd_0" frameborder="0" allowfullscreen></iframe>

                  </div>
                  </div>

                </div>
                </div>

                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js"></script>
        <script src="vendors/bower_components/mediaelement/build/mediaelement-and-player.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

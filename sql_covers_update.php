<?php
		/* Includes */
		include('mysqli.php');

		/* Variables */
		$id_covers = mysqli_real_escape_string($db,$_POST['id_covers']);
		$id_company = mysqli_real_escape_string($db,$_POST['id_company']);

		$date_waste = mysqli_real_escape_string($db,$_POST['date_waste']);
		$date_waste = date_create_from_format('d/m/Y', $date_waste);
		$date_waste = date_format($date_waste, 'Y/m/d');

		$kitchen = mysqli_real_escape_string($db,$_POST['kitchen']);
		$shift = mysqli_real_escape_string($db,$_POST['shift']);
		$covers = mysqli_real_escape_string($db,$_POST['covers']);

		if($id_company == $_SESSION['id_company']){

			$sql = "UPDATE lbc_covers SET
			date_waste = '$date_waste',
			kitchen = '$kitchen',
			shift = '$shift',
			covers = '$covers'
			WHERE id_covers = '$id_covers'";
			if ($db->query($sql) === TRUE) {header('Location: manage_covers.php?s=updated');}
			else {echo "Error updating record: " . $db->error;}
			
		}

		else echo "<br><br><br><p align='center' style='font-family:arial; font-size: 14px;'><strong>Security Error</strong><br>The covers could not be updated due to a security issue. Please contact your administrator if this error is displayed.</p>";
?>

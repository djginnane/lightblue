<?php
	
	// VARIABLES
	if($_GET['database'] != "") {$database = $_GET['database'];} else {$database = "NULL";}
	
	if($_GET['date_waste_from'] != "") {$date_waste_from = $_GET['date_waste_from'];} else {$date_waste_from = "NULL";}
	if($_GET['date_waste_to'] != "") {$date_waste_to = $_GET['date_waste_to'];} else {$date_waste_to = "NULL";}
	if($_GET['kitchen'] != "") {$kitchen = $_GET['kitchen'];} else {$kitchen = "NULL";}
	if($_GET['shift'] != "") {$shift = $_GET['shift'];} else {$shift = "NULL";}
	if($_GET['id_user'] != "") {$id_user = $_GET['id_user'];} else {$id_user = "NULL";}
	if($_GET['type_waste'] != "") {$type_waste = $_GET['type_waste'];} else {$type_waste = "NULL";}
	
	if($_GET['type_food'] != "") {$type_food = $_GET['type_food'];} else {$type_food = "NULL";}
	if($_GET['reason_waste'] != "") {$reason_waste = $_GET['reason_waste'];} else {$reason_waste = "NULL";}
	if($_GET['origin_waste'] != "") {$origin_waste = $_GET['origin_waste'];} else {$origin_waste = "NULL";}
	
	
	// DATABASE
	include 'database.php'; $id_company = $_SESSION['id_company'];
	
	// WHERE Query
	
	if (($date_waste_from != "NULL") && ($date_waste_to != "NULL")) {
		
	$date_waste_from = date_create_from_format('d/m/Y', $date_waste_from);
	$date_waste_from = date_format($date_waste_from, 'Y/m/d');
	
	$date_waste_to = date_create_from_format('d/m/Y', $date_waste_to);
	$date_waste_to = date_format($date_waste_to, 'Y/m/d');
	
	$where = " WHERE date_waste BETWEEN '$date_waste_from' AND '$date_waste_to'";
	}
	
	if ($id_company != "NULL") {
		if ($where != "") {$where.= " AND id_company = '$id_company'";}
		else {$where.= " WHERE id_company = '$id_company'";}
		
	}
	
	if ($kitchen != "NULL") {
		if ($where != "") {$where.= " AND kitchen = '$kitchen'";}
		else {$where.= " WHERE kitchen = '$kitchen'";}
		
	}
	
	if ($shift != "NULL") {
		if ($where != "") {$where.= " AND shift = '$shift'";}
		else {$where.= " WHERE shift = '$shift'";}
		
	}
	
	if ($id_user != "NULL") {
		if ($where != "") {$where.= " AND id_user = '$id_user'";}
		else {$where.= " WHERE id_user = '$id_user'";}
		
	}
	
	if ($type_waste != "NULL") {
		if ($where != "") {$where.= " AND type_waste = '$type_waste'";}
		else {$where.= " WHERE type_waste = '$type_waste'";}
		
	}
	
	if (($type_food != "NULL") && ($database != "lbc_rf3")) {
		if ($where != "") {$where.= " AND type_food LIKE '%$type_food%'";}
		else {$where.= " WHERE type_food LIKE '%$type_food%'";}
		
	}
	
	if (($reason_waste != "NULL") && ($database == "lbc_rf1")) {
		if ($where != "") {$where.= " AND reason_waste LIKE '%$reason_waste%'";}
		else {$where.= " WHERE reason_waste LIKE '%$reason_waste%'";}
		
	}
	
	if (($origin_waste != "NULL") && ($database == "lbc_rf1")) {
		if ($where != "") {$where.= " AND origin_waste LIKE '%$origin_waste%'";}
		else {$where.= " WHERE origin_waste LIKE '%$origin_waste%'";}
		
	}
	
	
	// SQL QUERY
	$query = "SELECT * FROM $database $where ORDER BY date_waste ASC";
	$rf3 = mysql_query($query, $db) or die(mysql_error());
	$row_rf3 = mysql_fetch_assoc($rf3);
	$numberRowsRF3  = mysql_num_rows($rf3);
	

	require_once 'scripts/PHPExcel/PHPExcel.php';
	
	$objPHPExcel = new PHPExcel();
	
	if ($numberRowsRF3 > 0) {
		
							$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Date');
							$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Kitchen');
							$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Shift');
							$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Category');
							$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Weight');
							if(($database == "lbc_rf1")||($database == "lbc_rf4")) {$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Type');}
							
		
							
							$n=1; do {  
												
								$date_waste = date("d M Y", strtotime($row_rf3['date_waste']));
								$line = $n + 1;
								
								$objPHPExcel->getActiveSheet()->setCellValue('A'.$line, $date_waste);
								$objPHPExcel->getActiveSheet()->setCellValue('B'.$line, $row_rf3['kitchen']);
								$objPHPExcel->getActiveSheet()->setCellValue('C'.$line, $row_rf3['shift']);
								$objPHPExcel->getActiveSheet()->setCellValue('D'.$line, $row_rf3['type_waste']);
								
								if(($database == "lbc_rf1")||($database == "lbc_rf4")){ $weight = $row_rf3['weight']/1000;} else {$weight = $row_rf3['weight'];}
								$objPHPExcel->getActiveSheet()->setCellValue('E'.$line, number_format($weight,2,'.',''));
						
								if(($database == "lbc_rf1")||($database == "lbc_rf4")) {$objPHPExcel->getActiveSheet()->setCellValue('F'.$line, $row_rf3['type_food']);}
					
							$n++; } while ($row_rf3 = mysql_fetch_assoc($rf3)); 
	}
		
	
	$objPHPExcel->getActiveSheet()->setTitle('Executive Summary');
	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(
    	PHPExcel_Style_Alignment::HORIZONTAL_CENTER
	);
	
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="ExecutiveSummary.xlsx"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
?>

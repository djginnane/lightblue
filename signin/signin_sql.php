<?php
	// Connection to database
	include('database.php');

	// Static variables
	$username = $_POST["username"];
	$password = $_POST["password"];

	// Check is there is an account with this email and password
	$sql = "SELECT * FROM lbc_users WHERE username = '$username'";
  $result = $db->query($sql);

  $row = mysqli_fetch_assoc($result);
  $nb_rows = $result->num_rows;

	if($nb_rows != 0) {

	// Check if password if correct
	if($password == $row['password']) {

		session_start();

		// Get baseline of company if exists
		$id_company = $row['id_company'];

		$sql_baseline = "SELECT * FROM lbc_companies WHERE id_company = '$id_company'";
	  $result_baseline = $db->query($sql_baseline); $row_baseline = mysqli_fetch_assoc($result_baseline);

		$_SESSION['baseline_start'] = $row_baseline['baseline_start'];
		$_SESSION['baseline_end'] = $row_baseline['baseline_end'];
		$_SESSION['cost_kilo'] = $row_baseline['cost_kilo'];

		if(($_SESSION['baseline_start'] != "")&&($_SESSION['baseline_end'] != "")&&($_SESSION['cost_kilo'] != "")) {
			$_SESSION['baseline'] = "true";
		}

		// Session variables
		$_SESSION['username'] = $username;
		$_SESSION['id_user'] = $row['id_user'];
		$_SESSION['timeout'] = time();
		$id_user = $row['id_user'];

		$_SESSION['id_company'] = $row['id_company'];
		$_SESSION['name'] = $row['name'];
		$_SESSION['company'] = $row['company'];

		// Access restrictions
		$_SESSION['WRRF1'] = $row['WRRF1'];
		$_SESSION['WRRF2'] = $row['WRRF2'];
		$_SESSION['WRRF3'] = $row['WRRF3'];
		$_SESSION['WRRF4'] = $row['WRRF4'];
		$_SESSION['WRCovers'] = $row['WRCovers'];
		$_SESSION['ESSummary'] = $row['ESSummary'];
		$_SESSION['ESOutlets'] = $row['ESOutlets'];
		$_SESSION['ESCanteens'] = $row['ESCanteens'];
		$_SESSION['ESBanquet'] = $row['ESBanquet'];
		$_SESSION['ESOndemand'] = $row['ESOndemand'];
		$_SESSION['SETKitchens'] = $row['SETKitchens'];
		$_SESSION['SETUsers'] = $row['SETUsers'];
		$_SESSION['SETTypeFood'] = $row['SETTypeFood'];
		$_SESSION['SETSimplifiedRF4'] = $row['SETSimplifiedRF4'];
		$_SESSION['SETSimplifiedES'] = $row['SETSimplifiedES'];

	// Redirect the user
	if($_SESSION['baseline'] == true){
		header("Location: ../functions/baseline.php");
	} else {
		header("Location: ../functions/variables.php");
	}

		} else header("Location: index.php?s=password");

	} else header("Location: index.php?s=account");
?>

<?php
      /* Maintenance mode */
      $maintenance = 'false';
      if($maintenance == 'true'){$access_maintenance = $_GET['access_maintenance'];}
?>

<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

        <!-- Vendor CSS -->
        <link href="../scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="../scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="../scripts/css/app.min.1.css" rel="stylesheet">
        <link href="../scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include ('../gtag.php'); ?>
    </head>

    <body class="login-content">

        <!-- Login -->
        <div class="lc-block toggled" id="l-login">
        <form action="signin_sql.php" method="post" id="signin" title="signin">

    <?php if($_GET['s'] == "logout") {?><p align="center" style="color:green;">You have been disconnected successfully.</p><?php } ?>
    <?php if($_GET['s'] == "disconnected") {?><p align="center">Login required</p><?php } ?>
	  <?php if($_GET['s'] == "password") {?><p align="center" style="color:red;">Incorrect password, please try again.</p><?php } ?>
    <?php if($_GET['s'] == "account") {?><p align="center" style="color:red;">There is no account registered with this username.</p><?php } ?>
    <?php if($maintenance == "true") {?><p align="center" style="color:red;"><b>Maintenance in progress</b><br>We will be back online shortly.</p><?php } ?>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Username" name="username">
                </div>
            </div>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                </div>
            </div>

            <div class="clearfix"></div>


            <?php if(($maintenance == 'false')||(($maintenance == 'true')&&($access_maintenance == 'true'))){?>
            <a href="javascript:{}" class="btn btn-login btn-danger btn-float bgm-black" onclick="document.getElementById('signin').submit(); return false;">
            <i class="zmdi zmdi-arrow-forward"></i></a>
            <?php } ?>

            <!--<ul class="login-navigation">
                <li data-block="#l-forget-password" class="bgm-orange">Forgot Password?</li>
            </ul>-->
            </form>
        </div>


        <!-- Forgot Password
        <div class="lc-block" id="l-forget-password">
            <p class="text-left">Enter your email address below to receive a temporary password</p>

            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line">
                    <input type="text" class="form-control" placeholder="Email Address">
                </div>
            </div>

            <a href="" class="btn btn-login btn-danger btn-float bgm-black"><i class="zmdi zmdi-arrow-forward"></i></a>

            <ul class="login-navigation">
                <li data-block="#l-login" class="bgm-orange">Login</li>
            </ul>
        </div> -->



        <!-- Javascript Libraries -->
        <script src="../scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="../scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="../scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="../scripts/js/functions.js"></script>

    </body>
</html>

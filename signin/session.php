<?php
	session_start();

	if($_SESSION['username'] == "") {header('Location: /signin/index.php?s=disconnected');}
	elseif($_SESSION['username'] != "") {$username = $_SESSION['username'];}

	// Session timeout
	if ($_SESSION['timeout'] + 1440 * 60 < time()) { session_destroy(); header('Location: /signin/index.php?s=disconnected');}
	if ($_SESSION['timeout'] + 1440 * 60 >= time()) {$_SESSION['timeout'] = time();}
?>

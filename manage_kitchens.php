<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company'];

// Load list of kitchens from this company
	$query = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
	$kitchen = mysql_query($query, $db) or die(mysql_error());
	$row_kitchen = mysql_fetch_assoc($kitchen);
	$numberRowsKitchen  = mysql_num_rows($kitchen);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

         <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->
		
		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>

    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('settings','manage_kitchens'); ?>

            <section id="content">
                <div class="container">

<? if($_GET['s'] == "added") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully added a new recording station / kitchen.
</div>
<? } ?>

<? if($_GET['s'] == "updated") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully updated the recording station / kitchen information.
</div>
<? } ?>

<? if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully deleted the recording station / kitchen.
</div>
<? } ?>

<? if($_GET['s'] != "addnew") { ?>
                    <div class="card">

               <div class="card-header">
                  <!--<h2>Manage recording station / Kitchens<small>List</small></h2>-->

                  <ul class="actions">
                            <li>
                                <a href="manage_kitchens.php?s=addnew">
                                    <i class="zmdi zmdi-plus"></i>
                                </a>
                            </li>
                  </ul>
               </div>

                  <div class="card-body card-padding">
                   <? if($numberRowsKitchen != "0") { ?>
                   <div class="card-body table-responsive">

                           <table class="table table-hover">
                             <thead>
                                    <tr>
                                        <th>Recording station</th>
                                        <th>Type</th>
                                    </tr>
                             </thead>
                                <tbody>

                     <? $n=1; do { ?>
                    <tr>
                    <td><a href="update_kitchens.php?id_kitchen=<? echo $row_kitchen['id_kitchen']; ?>"><? echo $row_kitchen['kitchen']; ?></a></td>
                    <td><? echo $row_kitchen['type']; ?></td>
                    </tr>
                                <? $n++; } while ($row_kitchen = mysql_fetch_assoc($kitchen));?>


                                </tbody>
                            </table>

                        </div>
						<? } else { ?>
                        <p align="center">No recording station / kitchens have been registered yet.</p>
                          <? } ?>

                  </div>
                  </div>

                    <? } else { ?>
                  <form action="sql_kitchen_add.php" method="post" id="addkitchen">
                    <div class="card">

                    <div class="card-header">
                  <h2>Add new recording station / kitchen</h2>

                   <ul class="actions">
                            <li>
                                <a href="manage_kitchens.php">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                  </ul>
                  </div>

                        <div class="card-body card-padding">
<div class="row">
<div class="col-sm-6">
                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="kitchen"></div>
                                  <label class="fg-label">Name</label>
                                  </div>
</div>
<div class="col-sm-6">
 							<div class="form-group">

                             <div class="select">
              <select class="form-control" name="type">
              <option value="Outlet" selected>Outlet</option>
              <option value="Canteen">Canteen</option>
              <option value="Banquet">Banquet</option>
                                                </select>
                                            </div>

                                    </div>

</div>

                                <div align="center">
                                <button type="submit" class="btn btn-default bgm-lightblue btn-icon-text">
                                <i class="zmdi zmdi-check-all"></i>Add new</button>
                                </div>
                            </div>

                        </div>
					</form>
                      <? } ?>
                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

<?php
	/*  Includes */
	include 'mysqli.php';

	/*  Variables */
	$id_user = $_SESSION['username'];
	$id_company = $_SESSION['id_company'];
	$id_covers = $_GET['id_covers'];

	/* Timezone */
	date_default_timezone_set("Asia/Bangkok");

	/*  Load covers data */
	$sql = "SELECT * FROM lbc_covers WHERE id_covers = '$id_covers'";
	$result = $db->query($sql);
	$row = mysqli_fetch_assoc($result);

	$kitchens = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
	$result_kitchens = $db->query($kitchens);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Accept only numbers or decimals "." -->
        <script>
				function isNumberKey(evt){
		   		var charCode = (evt.which) ? evt.which : event.keyCode
		    	if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
		        return false;
		    	return true;
				}
        </script>

				<!-- Google Analytics -->
				<?php include ('gtag.php'); ?>

    </head>
    <body>

	<?php include 'header.php';?>

        <section id="main">
          <?php include('sidebar.php'); sideBar('wasterecords','addcovers'); ?>

            <section id="content">
                <div class="container">

                    <div class="card">

               <div class="card-header">
                  <h2>Update covers</h2>

                  <ul class="actions">
                            <li>
                                <a onclick="return confirm('Are you sure you want to delete this waste record?');" href="sql_covers_delete.php?id_company=<?php echo $row['id_company']; ?>&id_covers=<?php echo $row['id_covers']; ?>">
                                    <i class="zmdi zmdi-delete"></i>
                                </a>
                            </li>
                  </ul>
               </div>
		 <form action="sql_covers_update.php" method="post" id="update_covers">
				 <input type="hidden" name="id_covers" value="<?php echo $row['id_covers']; ?>">
				 <input type="hidden" name="id_company" value="<?php echo $row['id_company']; ?>">

		                  <div class="card-body card-padding">


												<div class="row">
													<div class="col-sm-6">
														<p class="c-black f-500">Date</p>
														<div class="input-group form-group">
															<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
															<div class="dtp-container fg-line">
																<input type='text' class="form-control date-picker" placeholder="Date" id="date" name="date_waste" value="<?php echo date("d/m/Y", strtotime($row['date_waste']));?>">
															</div>
														</div>
													</div>


													<div class="col-sm-6">
														<p class="c-black f-500">Recording station</p>
														<div class="input-group form-group">
															<span class="input-group-addon"><i class="zmdi zmdi-cutlery"></i></span>
															<div class="fg-line select">
																<select class="form-control" name="kitchen" required>
																	<?php if ($result_kitchens->num_rows > 0) { ?>
																		<?php while($row_kitchens = $result_kitchens->fetch_assoc()) { ?>
																		<option <?php if($row['kitchen'] == $row_kitchens['kitchen']) {echo "selected";} ?> value="<?php echo $row_kitchens['kitchen']; ?>"><?php echo $row_kitchens['kitchen']; ?></option>
																		<?php } ?>
																	<?php } ?>
																</select>
															</div>
														</div>
													</div>



													<div class="col-sm-6">
														<p class="c-black f-500">Shift</p>
														<div class="input-group form-group">
															<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
															<div class="fg-line select">
																<select class="form-control" name="shift" required>
																	<option <?php if($row['shift'] == "Breakfast") {echo "selected";} ?> value="Breakfast">Breakfast</option>
																	<option <?php if($row['shift'] == "Lunch") {echo "selected";} ?> value="Lunch">Lunch</option>
																	<option <?php if($row['shift'] == "Dinner") {echo "selected";} ?> value="Dinner">Dinner</option>
																	<option <?php if($row['shift'] == "Night shift") {echo "selected";} ?> value="Night shift">Night shift</option>
																</select>
															</div>
														</div>
													</div>

														<div class="col-sm-6">
																<p class="c-black f-500">Covers</p>
																<input type="text" class="form-control" placeholder="eg. 40" name="covers" value="<?php echo $row['covers']; ?>" onKeyPress="return isNumberKey(event)">
														</div>
												</div>


		                  </div>
		                  </div>

		<button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
		<i class="zmdi zmdi-check-all"></i>Update</button>
		</form>
                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

<?php

  /* Includes */
  include('mysqli.php');

  /* Variables */
  $id_company = mysqli_real_escape_string($db,$_POST['id_company']);
  $cost_kilo = mysqli_real_escape_string($db,$_POST['cost_kilo']);

  /* Check user's access */
  if ($_SESSION['id_company'] == "lightblue") {

    /* Update query and redirect user */
    $sql = "UPDATE lbc_companies SET cost_kilo = '$cost_kilo' WHERE id_company = '$id_company'";
    if ($db->query($sql) === TRUE) {
      $_SESSION['cost_kilo'] = $cost_kilo;
      header("Location: manage_accounts_users.php");
    } else {
      echo "Error updating record: " . $db->error;
    }

  } else {
    echo "You don't have access to this feature. Please contact your administrator for more information.";
  }
?>

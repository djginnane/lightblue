<?php

		/* Database */
		include 'mysqli.php';

		/* Company to duplicate */
		$id_company = $_POST['id_company'];
		$id_company_copy = $id_company."_copy";

		if($_SESSION['id_company'] == 'lightblue') {

						/* Delete any previous copy of the same company */
						$sql_del_company = "DELETE FROM lbc_companies WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_company) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_users = "DELETE FROM lbc_users WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_users) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_covers = "DELETE FROM lbc_covers WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_covers) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_foodtypes = "DELETE FROM lbc_foodtypes WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_foodtypes) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_kitchens = "DELETE FROM lbc_kitchens WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_kitchens) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_rf1 = "DELETE FROM lbc_rf1 WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_rf1) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_rf2 = "DELETE FROM lbc_rf2 WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_rf2) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_rf3 = "DELETE FROM lbc_rf3 WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_rf3) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

						$sql_del_rf4 = "DELETE FROM lbc_rf4 WHERE id_company = '$id_company_copy'";
					  if ($db->query($sql_del_rf4) === TRUE) {} else {echo "Error deleting record: " . $db->error;}


						/* lbc_companies */
						$sql_companies = "SELECT * FROM lbc_companies WHERE id_company = '$id_company'";
					  $result_companies = $db->query($sql_companies);

						while($row_companies = $result_companies->fetch_assoc()) {
							$baseline_start = $row_companies['baseline_start'];
							$baseline_end = $row_companies['baseline_start'];

							$sql = "INSERT INTO lbc_companies (id_company, baseline_start, baseline_end) VALUES ('$id_company_copy', '$baseline_start', '$baseline_end')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						/* lbc_users */
						$sql_users = "SELECT * FROM lbc_users WHERE id_company = '$id_company'";
					  $result_users = $db->query($sql_users);

						while($row_users = $result_users->fetch_assoc()) {
							$username = $row_users['username']."_copy";
							$password = $row_users['password'];
							$type = $row_users['type'];
							$name = $row_users['name'];
							$surname = $row_users['surname'];
							$company = $row_users['company'];
							$department = $row_users['department'];
							$email = $row_users['email'];
							$phone = $row_users['phone'];
							$WRRF1 = $row_users['WRRF1'];
							$WRRF2 = $row_users['WRRF2'];
							$WRRF3 = $row_users['WRRF3'];
							$WRRF4 = $row_users['WRRF4'];
							$WRCovers = $row_users['WRCovers'];
							$ESSummary = $row_users['ESSummary'];
							$ESOutlets = $row_users['ESOutlets'];
							$ESCanteens = $row_users['ESCanteens'];
							$ESBanquet = $row_users['ESBanquet'];
							$ESOndemand = $row_users['ESOndemand'];
							$SETKitchens = $row_users['SETKitchens'];
							$SETUsers = $row_users['SETUsers'];
							$SETTypeFood = $row_users['SETTypeFood'];
							$SETSimplifiedRF4 = $row_users['SETSimplifiedRF4'];
							$SETSimplifiedES = $row_users['SETSimplifiedES'];

							$sql = "INSERT INTO lbc_users (id_company, username, password, type, name, surname, company, department, email, phone, WRRF1, WRRF2, WRRF3, WRRF4, WRCovers, ESSummary, ESCanteens, ESBanquet, ESOndemand, SETKitchens, SETUsers, SETTypeFood, SETSimplifiedRF4, SETSimplifiedES)
							VALUES ('$id_company_copy', '$username', '$password', '$type', '$name', '$surname', '$company', '$department', '$email', '$phone', '$WRRF1', '$WRRF2', '$WRRF3', '$WRRF4', '$WRCovers', '$ESSummary', '$ESCanteens', '$ESBanquet', '$ESOndemand',
							 '$SETKitchens', '$SETUsers', '$SETTypeFood', '$SETSimplifiedRF4', '$SETSimplifiedES')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						/* lbc_rf3 */
						$sql_rf3 = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company'";
					  $result_rf3 = $db->query($sql_rf3);

						while($row_rf3 = $result_rf3->fetch_assoc()) {
							$id_user = $row_rf3['id_user']."_copy";
							$date_added = $row_rf3['date_added'];
							$date_waste = $row_rf3['date_waste'];
							$kitchen = $row_rf3['kitchen'];
							$shift = $row_rf3['shift'];
							$type_waste = $row_rf3['type_waste'];
							$weight = $row_rf3['weight'];

							$sql = "INSERT INTO lbc_rf3 (id_user, id_company, date_added, date_waste, kitchen, shift, type_waste, weight) VALUES ('$id_user', '$id_company_copy', '$date_added', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						/* lbc_rf1 */
						$sql_rf1 = "SELECT * FROM lbc_rf1 WHERE id_company = '$id_company'";
					  $result_rf1 = $db->query($sql_rf1);

						while($row_rf1 = $result_rf1->fetch_assoc()) {
							$id_user = $row_rf1['id_user']."_copy";
							$date_added = $row_rf1['date_added'];
							$date_waste = $row_rf1['date_waste'];
							$kitchen = $row_rf1['kitchen'];
							$shift = $row_rf1['shift'];
							$type_waste = $row_rf1['type_waste'];
							$weight = $row_rf1['weight'];
							$type_food = $row_rf1['type_food'];
							$reason_waste = $row_rf1['reason_waste'];
							$origin_waste = $row_rf1['origin_waste'];

							$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_added, date_waste, kitchen, shift, type_waste, weight, type_food, reason_waste, origin_waste) VALUES ('$id_user', '$id_company_copy', '$date_added', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight', '$type_food', '$reason_waste', '$origin_waste')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						/* lbc_rf4 */
						$sql_rf4 = "SELECT * FROM lbc_rf4 WHERE id_company = '$id_company'";
					  $result_rf4 = $db->query($sql_rf4);

						while($row_rf4 = $result_rf4->fetch_assoc()) {
							$id_user = $row_rf4['id_user']."_copy";
							$date_added = $row_rf4['date_added'];
							$date_waste = $row_rf4['date_waste'];
							$kitchen = $row_rf4['kitchen'];
							$shift = $row_rf4['shift'];
							$type_waste = $row_rf4['type_waste'];
							$weight = $row_rf4['weight'];
							$type_food = $row_rf4['type_food'];

							$sql = "INSERT INTO lbc_rf4 (id_user, id_company, date_added, date_waste, kitchen, shift, type_waste, weight, type_food) VALUES ('$id_user', '$id_company_copy', '$date_added', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight', '$type_food')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						/* lbc_rf2 */
						$sql_rf2 = "SELECT * FROM lbc_rf2 WHERE id_company = '$id_company'";
					  $result_rf2 = $db->query($sql_rf2);

						while($row_rf2 = $result_rf2->fetch_assoc()) {
							$date_waste = $row_rf2['date_waste'];
							$kitchen = $row_rf2['kitchen'];
							$shift = $row_rf2['shift'];
							$approval_user = $row_rf2['approval_user']."_copy";
							$approval_date = $row_rf2['approval_date'];
							$approval_status = $row_rf2['approval_status'];
							$covers = $row_rf2['covers'];

							$sql = "INSERT INTO lbc_rf2 (id_company, date_waste, kitchen, shift, approval_user, approval_date, approval_status, covers) VALUES ('$id_company_copy', '$date_waste', '$kitchen', '$shift', '$approval_user', '$approval_date', '$approval_status', '$covers')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						/* lbc_covers */
						$sql_covers = "SELECT * FROM lbc_covers WHERE id_company = '$id_company'";
					  $result_covers = $db->query($sql_covers);

						while($row_covers = $result_covers->fetch_assoc()) {

							$id_user = $row_covers['id_user']."_copy";
							$date_added = $row_covers['date_added'];
							$date_waste = $row_covers['date_waste'];
							$kitchen = $row_covers['kitchen'];
							$shift = $row_covers['shift'];
							$covers = $row_covers['covers'];

							$sql = "INSERT INTO lbc_covers (id_user, id_company, date_added, date_waste, kitchen, shift, covers) VALUES ('$id_user', '$id_company_copy', '$date_added', '$date_waste', '$kitchen', '$shift', '$covers')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						/* lbc_kitchens */
						$sql_kitchens = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company'";
					  $result_kitchens = $db->query($sql_kitchens);

						while($row_kitchens = $result_kitchens->fetch_assoc()) {

							$kitchen = $row_kitchens['kitchen'];
							$type = $row_kitchens['type'];

							$sql = "INSERT INTO lbc_kitchens (id_company, kitchen, type) VALUES ('$id_company_copy', '$kitchen', '$type')";
							if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
						}

						echo "<script language=\"JavaScript\"> document.location.replace(\"manage_accounts.php\");</script>";
		}
?>

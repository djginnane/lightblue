<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company'];

// Load list of kitchens from this company
	$query = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
	$kitchen = mysql_query($query, $db) or die(mysql_error());
	$row_kitchen = mysql_fetch_assoc($kitchen);
	$numberRowsKitchen  = mysql_num_rows($kitchen);

// Load list of shifts without covers registered yet
	$query_rf2_no = "SELECT * FROM lbc_rf2 WHERE id_company = '$id_company' AND covers = 'no' ORDER BY date_waste ASC";
	$rf2_no = mysql_query($query_rf2_no, $db) or die(mysql_error());
	$row_rf2_no = mysql_fetch_assoc($rf2_no);
	$numberRowsrf2_no  = mysql_num_rows($rf2_no);

?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Accept only numbers but NO decimals "." -->
        <script>
		function isNumberKey(evt){
   		var charCode = (evt.which) ? evt.which : event.keyCode
    	 if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    	return true;
		}
        </script>

        <!-- Form validation for empty fields -->
        <script>
	function validateForm() {
    	var kitchen = document.forms["add_covers"]["kitchen"].value;
		var shiftv = document.forms["add_covers"]["shift"].value;
		var covers = document.forms["add_covers"]["covers"].value;

		if (kitchen == null || kitchen == "" || shiftv == null || shiftv == "" || covers == null || covers == "") {
		alert("All required fields must be filled-in.");
		return false;}

	}
		</script>

         <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>

    </head>
    <body>

       <? include 'header.php';?>

       <section id="main">

           <? include 'sidebar.php'; sideBar('wasterecords','addcovers'); ?>

            <section id="content">
                <div class="container">

<? if($_GET['s'] == "added") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Covers have been recorded successfully.
</div>
<? } ?>

<? if($_GET['s'] == "updated") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Covers have been updated successfully.
</div>
<? } ?>

<? if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Covers have been deleted successfully.
</div>
<? } ?>


                     <div class="block-header">
                        <h2>Add / update covers</h2>

                        <ul class="actions">
                            <li>
                                <a href="manage_covers.php">
                                    <i class="zmdi zmdi-view-list"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

<form action="sql_add_covers.php" method="post" id="add_covers" onsubmit="return validateForm()">

<!-- Hidden inputs -->
<input type="hidden" name="id_user" value="<? echo $_SESSION['username']; ?>">
<input type="hidden" name="id_company" value="<? echo $_SESSION['id_company']; ?>">
<!-- Hidden inputs -->

                  <div class="card">


                        <div class="card-body card-padding">

                            <div class="row">
                                <div class="col-sm-4">

                                  <div class="input-group form-group" >
                                  <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                  <div class="dtp-container fg-line">
     							  <input type='text' class="form-control date-picker" placeholder="Date" id="date" name="date_waste" value="<? if($_SESSION['date_waste'] == "") { echo date('d/m/Y');} else {echo $_SESSION['date_waste'];}?>">
                                  </div>
                                  </div>

                                </div>

                            <div class="col-sm-4">


<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-cutlery"></i></span>
<div class="fg-line select">
<select class="form-control" name="kitchen">
<? if($_SESSION['kitchen'] == "") {?><option value="">Select recording station / kitchen</option><? } ?>
<? if($numberRowsKitchen != "0") { $n=1; do { ?>
<option <? if($_SESSION['kitchen'] == $row_kitchen['kitchen']) {echo "selected";} ?> value="<? echo $row_kitchen['kitchen']; ?>"><? echo $row_kitchen['kitchen']; ?></option>
<? $n++; } while ($row_kitchen = mysql_fetch_assoc($kitchen)); }?>
</select>
</div>
</div>

                            </div>

                            <div class="col-sm-4">

<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
<div class="fg-line select">
<select class="form-control" name="shift">
<? if($_SESSION['shift'] == "") {?><option value="">Select shift</option> <? } ?>
<option <? if($_SESSION['shift'] == "Breakfast") {echo "selected";} ?> value="Breakfast">Breakfast</option>
<option <? if($_SESSION['shift'] == "Lunch") {echo "selected";} ?> value="Lunch">Lunch</option>
<option <? if($_SESSION['shift'] == "Dinner") {echo "selected";} ?> value="Dinner">Dinner</option>
<option <? if($_SESSION['shift'] == "Night shift") {echo "selected";} ?> value="Night shift">Night shift</option>
</select>
</div>
</div>

                            </div>
                            </div>




                             <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-lg form-control fg-input" name="covers" onKeyPress="return isNumberKey(event)" autocomplete="off"></div>
                                  <label class="fg-label">Number of covers</label>
                             </div>

                        <div align="center"><button type="submit" class="btn btn-primary bgm-lightblue btn-icon-text">
<i class="zmdi zmdi-cloud-upload"></i>Add number of covers</button>  </div>

                       </div>
                      </div>


                    </form>


                     <? if ($numberRowsrf2_no > 0) { ?>
                    <div class="card">

                        <div class="card-header">
                            <h2>No covers added yet<small>List of shifts</small></h2>
                        </div>

                        <div class="table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center">Date</th>
                                        <th align="center">Recording station</th>
                                        <th align="center">Shift</th>
                                        <th align="center"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                <? $n=1; do { ?>
                    <tr>
                    <td><? echo date("d/m/y", strtotime($row_rf2_no['date_waste'])); ?></td>
                    <td><? echo $row_rf2_no['kitchen']; ?></td>
                    <td><? echo $row_rf2_no['shift']; ?></td>

                    <td>

					<form id="rf2" name="rf2" method="post" action="sql_add_covers.php">
                    <!-- Hidden inputs -->
                    <input type="hidden" name="date_waste" value="<? echo date("d/m/Y", strtotime($row_rf2_no['date_waste'])); ?>" />
                    <input type="hidden" name="kitchen" value="<? echo $row_rf2_no['kitchen']; ?>" />
                    <input type="hidden" name="shift" value="<? echo $row_rf2_no['shift']; ?>" />
                    <input type="hidden" name="id_user" value="<? echo $_SESSION['username']; ?>">
                    <input type="hidden" name="id_company" value="<? echo $_SESSION['id_company']; ?>">
                    <!-- Hidden inputs -->

               <div class="row">

                    <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <input type="text" class="form-control" placeholder="Number of covers" name="covers" onKeyPress="return isNumberKey(event)" autocomplete="off">
                                        </div>
                                    </div>
                    </div>

                	 <!--
                     <div class="form-group fg-float">
                            <div class="fg-line"><input type="text" class="input-lg form-control fg-input" name="cover"></div>
                            <label class="fg-label">Number of covers</label>
                    </div>
                    -->

                    <div class="col-sm-6">

                    <button type="submit" class="btn btn-warning bgm-lightblue waves-effect"><i class="zmdi zmdi-arrow-forward"></i></button>

                    <!--<button  class="btn btn-primary btn-xs bgm-lightblue waves-effect">Add covers</button>-->
                    </div>
              </div>
                    </form>

                    </td>
                    </tr>
                                <? $n++; } while ($row_rf2_no = mysql_fetch_assoc($rf2_no));?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                  <? } ?>

                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

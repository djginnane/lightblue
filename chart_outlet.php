<? include 'database.php'; include 'functions.php'; $id_company = $_SESSION['id_company'];?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
	<!-- HighCharts -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="//code.highcharts.com/highcharts.js"></script>
    <!-- HighCharts -->

	<!-- Google Analytics -->
	<?php include ('gtag.php'); ?>
	
</head>

<body>

<? if($_GET['type'] == "recordingpoints") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: { type: 'line'},
        title: {text: 'Recording Points'},
		xAxis: {categories: [<? listOfDays($_SESSION['id_company'], 'lbc_rf3'); ?>], labels: {style: {fontSize:'14px'}}},
        yAxis: { title: {text: 'KG'}, labels: {style: {fontSize:'13px'}}},

		colors: ['#efefef', '#fbbe40', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
             '#FF9655', '#FFF263'],

	   credits: { enabled: false},

        series: [{
			type:'column',
            name: 'Total',
            data: [<?

			// MySql Database
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

				// Load list of days
				$sql_days_total = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days_total = $db->query($sql_days_total)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days_total = mysqli_fetch_assoc($result_days_total);
				$numRows_days_total = $result_days_total->num_rows;

				$n=1; do {

					dailyWasteTotal('lbc_rf3', $id_company, $row_days_total['date_waste']);
					if($n < $numRows_days_total) {echo ",";}	 $n++;

				} while ($row_days_total = mysqli_fetch_assoc($result_days_total));

			?>]
        },

		<?

		// Load list of kitchens
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type='Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
		$row=mysqli_fetch_assoc($result);

		$numRows = $result->num_rows;

		// Display result
		$i = 1; do {

			echo "{ name: '".$row['kitchen']."',
					data: [";

				// Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteKitchen('lbc_rf3', $id_company, $row_days['date_waste'], $row['kitchen']);
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));

			echo "] }"; if($i < $numRows) {echo ",";}	 $i++;

		} while ($row = mysqli_fetch_assoc($result)); ?>

		]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "shift") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: { type: 'line'},
        title: {text: 'Shifts'},
		xAxis: {categories: [<? listOfDays($_SESSION['id_company'], 'lbc_rf3'); ?>], labels: {style: {fontSize:'14px'}}},
        yAxis: { title: {text: 'KG'}, labels: {style: {fontSize:'13px'}}},

		colors: ['#efefef', '#fbbe40', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
             '#FF9655', '#FFF263'],

		credits: { enabled: false},

        series: [{
			type:'column',
            name: 'Total',
            data: [<?

			// MySql Database
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

				// Load list of days
				$sql_days_total = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days_total = $db->query($sql_days_total)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days_total = mysqli_fetch_assoc($result_days_total);
				$numRows_days_total = $result_days_total->num_rows;

				$n=1; do {

					dailyWasteTotal('lbc_rf3', $id_company, $row_days_total['date_waste']);
					if($n < $numRows_days_total) {echo ",";}	 $n++;

				} while ($row_days_total = mysqli_fetch_assoc($result_days_total));

			?>]
        },{
            name: 'Breakfast',
            data: [


			<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteShift('lbc_rf3', $id_company, $row_days['date_waste'], 'Breakfast');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>


			]
        }, {
            name: 'Lunch',
            data: [


			<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteShift('lbc_rf3', $id_company, $row_days['date_waste'], 'Lunch');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>


			]
        }, {
            name: 'Dinner',
            data: [


			<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteShift('lbc_rf3', $id_company, $row_days['date_waste'], 'Dinner');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>


			]
        }, {
            name: 'Night Shift',
            data: [


			<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteShift('lbc_rf3', $id_company, $row_days['date_waste'], 'Night Shift');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>


			]
        }]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "category") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: { type: 'line'},
        title: {text: 'Category of Waste'},
		xAxis: {categories: [<? listOfDays($_SESSION['id_company'], 'lbc_rf3'); ?>], labels: {style: {fontSize:'14px'}}},
        yAxis: { title: {text: 'KG'}, labels: {style: {fontSize:'13px'}}},

		colors: ['#efefef', '#000', '#FFC107', '#2196F3', '#3F51B5'],

		credits: { enabled: false},

        series: [{
			type:'column',
            name: 'Total',
            data: [<?

			// MySql Database
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

				// Load list of days
				$sql_days_total = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days_total = $db->query($sql_days_total)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days_total = mysqli_fetch_assoc($result_days_total);
				$numRows_days_total = $result_days_total->num_rows;

				$n=1; do {

					dailyWasteTotal('lbc_rf3', $id_company, $row_days_total['date_waste']);
					if($n < $numRows_days_total) {echo ",";}	 $n++;

				} while ($row_days_total = mysqli_fetch_assoc($result_days_total));

			?>]
        },{
            name: 'Spoilage',
            data: [<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteCategory('lbc_rf3', $id_company, $row_days['date_waste'], 'Spoilage');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>]
        }, {
            name: 'Preparation',
            data: [<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteCategory('lbc_rf3', $id_company, $row_days['date_waste'], 'Preparation');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>]
        }, {
            name: 'Plate',
            data: [<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteCategory('lbc_rf3', $id_company, $row_days['date_waste'], 'Plate');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>]
        }, {
            name: 'Buffet',
            data: [<? // Load list of days
				$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days=mysqli_fetch_assoc($result_days);
				$numRows_days = $result_days->num_rows;

				$n=1; do {

					dailyWasteCategory('lbc_rf3', $id_company, $row_days['date_waste'], 'Buffet');
					if($n < $numRows_days) {echo ",";}	 $n++;

				} while ($row_days = mysqli_fetch_assoc($result_days));
			?>]
        }]
    });
});
</script>
<? } ?>

<div id="container" style="width:100%; height:350px;"></div>

</body>
</html>

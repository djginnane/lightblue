<?php

	/* Includes */
	include('mysqli.php');

	/* Variables */
	$id_kitchen = mysqli_real_escape_string($db,$_POST['id_kitchen']);
	$kitchen = mysqli_real_escape_string($db,ucwords($_POST['kitchen']));
	$type = mysqli_real_escape_string($db,$_POST['type']);
	$id_company = mysqli_real_escape_string($db,$_POST['id_company']);
	$original_kitchen = mysqli_real_escape_string($db,$_POST['original_kitchen']);

	if(($id_kitchen != "")&&($kitchen != "")&&($id_company == $_SESSION['id_company'])){

		/* Update lbc_kitchens */
		$sql = "UPDATE lbc_kitchens SET kitchen = '$kitchen' WHERE (kitchen = '$original_kitchen' AND id_company = '$id_company')";
		if ($db->query($sql) === TRUE) {} else {echo "Error updating record: " . $db->error;}

		/* Update lbc_covers */
		$sql = "UPDATE lbc_covers SET kitchen = '$kitchen' WHERE (kitchen = '$original_kitchen' AND id_company = '$id_company')";
		if ($db->query($sql) === TRUE) {} else {echo "Error updating record: " . $db->error;}

		/* Update lbc_rf1 */
		$sql = "UPDATE lbc_rf1 SET kitchen = '$kitchen' WHERE (kitchen = '$original_kitchen' AND id_company = '$id_company')";
		if ($db->query($sql) === TRUE) {} else {echo "Error updating record: " . $db->error;}

		/* Update lbc_rf2 */
		$sql = "UPDATE lbc_rf2 SET kitchen = '$kitchen' WHERE (kitchen = '$original_kitchen' AND id_company = '$id_company')";
		if ($db->query($sql) === TRUE) {} else {echo "Error updating record: " . $db->error;}

		/* Update lbc_rf3 */
		$sql = "UPDATE lbc_rf3 SET kitchen = '$kitchen' WHERE (kitchen = '$original_kitchen' AND id_company = '$id_company')";
		if ($db->query($sql) === TRUE) {} else {echo "Error updating record: " . $db->error;}

		/* Update lbc_rf4 */
		$sql = "UPDATE lbc_rf4 SET kitchen = '$kitchen' WHERE (kitchen = '$original_kitchen' AND id_company = '$id_company')";
		if ($db->query($sql) === TRUE) {} else {echo "Error updating record: " . $db->error;}

		/* Redirect user */
		header('Location: manage_kitchens.php?s=updated');

	} else {
		echo "<br><br><br><p align='center' style='font-family:arial; font-size: 14px;'><strong>Error updating the kitchen name.</strong><br>Please go back to the previous page, make sure that kitchen name is entered and try again.<br><br></p>";
	}

?>

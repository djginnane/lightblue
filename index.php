<?
include 'database.php'; $id_user = $_SESSION['username'];?>

<? // Database query and variables
$query_news = "SELECT * FROM lbc_dashboard WHERE id_dashboard = '1'";
$result = mysql_query($query_news, $db) or die(mysql_error());

$row_result = mysql_fetch_assoc($result); // Use format $row_result['id_promo'];
$totalRows = mysql_num_rows($result); ?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include ('gtag.php'); ?>

    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('home','home'); ?>

            <section id="content">
                <div class="container">

                <? if($_GET['s'] == "change_password") { ?>
              <div class="card">
                  <div class="card-header">
                  	<h2>Change password</h2>
                  </div>

                  <div class="card-body card-padding">

                    <form action="signin/sql_changepassword.php" method="post" id="changepassword" >
                      <div class="form-group fg-float">

                      <div class="fg-line"><input type="password" class="input-lg form-control fg-input" name="password"></div>
                      <label class="fg-label">New password</label>
                 	  </div>

                      <div align="center" id="update_button">
                      <button type="submit" class="btn btn-default btn-icon-text bgm-lightblue waves-effect">
                      <i class="zmdi zmdi-refresh"></i>Update
                      </button>
                      </div>
                     </form>
                  </div>
             </div>
             <? } else { ?>

                       <div class="block-header">
                        <h2>Welcome <? echo $_SESSION['name'];?></h2>

                        <ul class="actions">
                            <li>
                                <a href="">
                                    <i class="zmdi zmdi-refresh"></i>
                                </a>
                            </li>
                        </ul>

                    </div>

                    <div class="card">

               <div class="card-header">
                  <h2>What's new at LightBlue<small>Latest news</small></h2>
               </div>

                  <div class="card-body card-padding">

<? if($_SESSION['id_company'] == "lightblue") { ?>
<form action="sql_news.php" method="post" id="update_content" >
<textarea id="news" name="news"  class="html-editor form-control" rows="10">
  <? echo $row_result['news'];   ?>
</textarea>
<? } else  {echo $row_result['news']; }  ?>

                  </div>
                  </div>

                  <div class="card">

               <div class="card-header">
                  <h2>What's new in the Sustainable World<small>Latest news</small></h2>
               </div>

                  <div class="card-body card-padding">

<? if($_SESSION['id_company'] == "lightblue") { ?>
<textarea id="news_industry" name="news_industry"  class="html-editor form-control" rows="10">
  <? echo $row_result['news_industry'];   ?>
</textarea>
<? } else  {echo $row_result['news_industry']; }  ?>

                  </div>
                  </div>

                  <? if($_SESSION['id_company'] == "lightblue") { ?>
                 <button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
                <i class="zmdi zmdi-check-all"></i>Update content</button>
                </form>
                 <? } ?>

                  <? } //other than change password ?>
                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

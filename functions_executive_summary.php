<?php

    function TotalFoodWasteBetweenTwoDates($start_date, $end_date) {

        /* Database */
        $db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
        if ($db->connect_error) { die("Connection failed: " . $db->connect_error);}

        /* Variables */
        session_start();
        $id_company = $_SESSION['id_company'];

    		/* Query */
    		$sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company' AND date_waste BETWEEN '$start_date' AND '$end_date'";
    		$result = $db->query($sql);

        $weight = mysqli_fetch_array($result);
    		echo number_format($weight[0],2,","," ");
    }

    function TotalFoodWastePerCoverBetweenTwoDates($start_date, $end_date, $type) {

        /* Database */
        $db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
        if ($db->connect_error) { die("Connection failed: " . $db->connect_error);}

        /* Variables */
        session_start();
        $id_company = $_SESSION['id_company'];

    		// Calculate total weight
    		$sql = "SELECT SUM(weight)
    		FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
    		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
    		AND lbc_kitchens.type != '$type' AND lbc_rf3.date_waste BETWEEN '$start_date' AND '$end_date'";

    		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];

    		// Calculate total covers
    		$covers = "SELECT SUM(covers)
    		FROM lbc_covers RIGHT JOIN lbc_kitchens ON lbc_covers.kitchen = lbc_kitchens.kitchen
    		WHERE lbc_covers.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
    		AND lbc_kitchens.type != '$type' AND lbc_covers.date_waste BETWEEN '$start_date' AND '$end_date'";

    		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];

    		// Calculate total
    		$ratio = ($total_weight / $total_cov) * 1000;
    		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,","," ");}


    }

    function TotalCoversBetweenTwoDates($start_date, $end_date, $type) {

        /* Database */
        $db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
        if ($db->connect_error) { die("Connection failed: " . $db->connect_error);}

        /* Variables */
        session_start();
        $id_company = $_SESSION['id_company'];

    		// Calculate total covers
    		$covers = "SELECT SUM(covers)
    		FROM lbc_covers RIGHT JOIN lbc_kitchens ON lbc_covers.kitchen = lbc_kitchens.kitchen
    		WHERE lbc_covers.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
    		AND lbc_kitchens.type != '$type' AND lbc_covers.date_waste BETWEEN '$start_date' AND '$end_date'";

    		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];
        echo number_format($total_cov,0,","," ");
    }

?>

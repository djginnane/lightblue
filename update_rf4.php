<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company']; $id_record = $_GET['id_record'];

// Load list of records
	$query = "SELECT * FROM lbc_rf4 WHERE id_waste = '$id_record'";
	$rf4 = mysql_query($query, $db) or die(mysql_error());
	$row_rf4 = mysql_fetch_assoc($rf4);
	$numberRowsrf4  = mysql_num_rows($rf4);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Accept only numbers or decimals "." -->
        <script>
		function isNumberKey(evt){
   		var charCode = (evt.which) ? evt.which : event.keyCode
    	if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
        return false;
    	return true;
		}
        </script>

				<!-- Google Analytics -->
				<?php include ('gtag.php'); ?>

    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('wasterecords','rf4'); ?>

            <section id="content">
                <div class="container">

                    <div class="card">

               <div class="card-header">
                  <h2>RF3 - Update record<small><? echo $row_rf4['kitchen']; ?> - <? echo $row_rf4['shift']; ?> - <? echo date("d/m/y", strtotime($row_rf4['date_waste'])); ?></small></h2>

                  <ul class="actions">
                            <li>
                                <a onclick="return confirm('Are you sure you want to delete this waste record?');" href="sql_waste_rf4_delete.php?id_waste=<? echo $row_rf4['id_waste']; ?>">
                                    <i class="zmdi zmdi-delete"></i>
                                </a>
                            </li>
                  </ul>
               </div>
 <form action="sql_waste_rf4_update.php" method="post" id="updaterf4">
 <input type="hidden" name="id_waste" value="<? echo $row_rf4['id_waste']; ?>">
 <input type="hidden" name="id_company" value="<? echo $row_rf4['id_company']; ?>">
                  <div class="card-body card-padding">

   <div class="row">

		<div class="col-sm-6">
         <p class="c-black f-500">Weight</p>
                        <div class="input-group">
                                <div class="fg-line">
                                    <input type="text" class="form-control" placeholder="eg. 2.5" name="weight" value="<? echo $row_rf4['weight']; ?>" onKeyPress="return isNumberKey(event)">
                                </div>
                                <span class="input-group-addon first"><strong>g</strong></span>
                         </div>
       </div>

       <div class="col-sm-6">
        <p class="c-black f-500">Type of food</p>
                        <div class="form-group">
                                <div class="fg-line">
                                    <input type="text" class="form-control" placeholder="eg. Out of date" name="type_food" value="<? echo $row_rf4['type_food']; ?>">
                                </div>
        </div>
       </div>



   </div>


                  </div>
                  </div>

<button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
<i class="zmdi zmdi-check-all"></i>Update</button>
</form>
                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

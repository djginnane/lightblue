<?php
	/* Includes */
	include('mysqli.php');

	/* Variables */
	$id_waste = mysqli_real_escape_string($db,$_POST['id_waste']);
	$id_company = mysqli_real_escape_string($db,$_POST['id_company']);

	$date_waste = mysqli_real_escape_string($db,$_POST['date_waste']);
	$date_waste = date_create_from_format('d/m/Y', $date_waste);
	$date_waste = date_format($date_waste, 'Y/m/d');

	$kitchen = mysqli_real_escape_string($db,$_POST['kitchen']);
	$shift = mysqli_real_escape_string($db,$_POST['shift']);
	$type_waste = mysqli_real_escape_string($db,$_POST['type_waste']);
	$weight = mysqli_real_escape_string($db,$_POST['weight']);

	/* Query */
	if(($id_waste != "")&&($id_company == $_SESSION['id_company'])){

		$sql = "UPDATE lbc_rf3 SET
		date_waste = '$date_waste',
		kitchen = '$kitchen',
		shift = '$shift',
		type_waste = '$type_waste',
		weight = '$weight'
		WHERE id_waste = '$id_waste'";

		if ($db->query($sql) === TRUE) {header('Location: manage_rf3.php?s=updated');}
		else {echo "Error updating record: " . $db->error;}

		}
	else echo "<br><br><br><p align='center' style='font-family:arial; font-size: 14px;'><strong>Error while updating</strong><br>Please go back to the previous page, make sure all required fields are filled-in and try again.<br><br></p>";


?>

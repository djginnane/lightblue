<?
function onDemand($database,$date_waste_from,$date_waste_to,$kitchen,$shift,$id_user,$type_waste,$type_food,$reason_waste,$origin_waste, $excel) {

	// Include database
	include 'database.php'; $id_company = $_SESSION['id_company'];

	// WHERE Query

	if (($date_waste_from != "NULL") && ($date_waste_to != "NULL")) {

	$date_waste_from = date_create_from_format('d/m/Y', $date_waste_from);
	$date_waste_from = date_format($date_waste_from, 'Y/m/d');

	$date_waste_to = date_create_from_format('d/m/Y', $date_waste_to);
	$date_waste_to = date_format($date_waste_to, 'Y/m/d');

	$where = " WHERE date_waste BETWEEN '$date_waste_from' AND '$date_waste_to'";
	}

	if ($id_company != "NULL") {
		if ($where != "") {$where.= " AND id_company = '$id_company'";}
		else {$where.= " WHERE id_company = '$id_company'";}

	}

	if ($kitchen != "NULL") {
		if ($where != "") {$where.= " AND kitchen = '$kitchen'";}
		else {$where.= " WHERE kitchen = '$kitchen'";}

	}

	if ($shift != "NULL") {
		if ($where != "") {$where.= " AND shift = '$shift'";}
		else {$where.= " WHERE shift = '$shift'";}

	}

	if ($id_user != "NULL") {
		if ($where != "") {$where.= " AND id_user = '$id_user'";}
		else {$where.= " WHERE id_user = '$id_user'";}

	}

	if ($type_waste != "NULL") {
		if ($where != "") {$where.= " AND type_waste = '$type_waste'";}
		else {$where.= " WHERE type_waste = '$type_waste'";}

	}

	if (($type_food != "NULL") && ($database != "lbc_rf3")) {
		if ($where != "") {$where.= " AND type_food LIKE '%$type_food%'";}
		else {$where.= " WHERE type_food LIKE '%$type_food%'";}

	}

	if (($reason_waste != "NULL") && ($database == "lbc_rf1")) {
		if ($where != "") {$where.= " AND reason_waste LIKE '%$reason_waste%'";}
		else {$where.= " WHERE reason_waste LIKE '%$reason_waste%'";}

	}

	if (($origin_waste != "NULL") && ($database == "lbc_rf1")) {
		if ($where != "") {$where.= " AND origin_waste LIKE '%$origin_waste%'";}
		else {$where.= " WHERE origin_waste LIKE '%$origin_waste%'";}

	}


	// SQL Call
	$query = "SELECT * FROM $database $where ORDER BY date_waste ASC";
	$rf3 = mysql_query($query, $db) or die(mysql_error());
	$row_rf3 = mysql_fetch_assoc($rf3);
	$numberRowsRF3  = mysql_num_rows($rf3);

	if ($numberRowsRF3 > 0) {

						 echo "<table class='table table-hover'>
						 <thead>
                                 <tr>
									<th>Date</th>
									<th>Kitchen</th>
									<th>Shift</th>
									<th>Category</th>
									<th>Weight</th>";

									if($database == "lbc_rf4") {echo "<th>Type</th>";}
									if($database == "lbc_rf1") {echo "<th>Type</th>";}

                          echo   "</tr>
                             </thead>
                             <tbody>";

		$n=1; do {
							$date_waste = date("d M y", strtotime($row_rf3['date_waste']));

							echo "<tr>";
							echo "<td>".$date_waste."</td>";
							echo "<td>".$row_rf3['kitchen']."</td>";
							echo "<td>".$row_rf3['shift']."</td>";
							echo "<td>".$row_rf3['type_waste']."</td>";

							if($excel == "no") {

								// Convert in kg if data is in gram, keep in kg if data is in kg
								if(($database == "lbc_rf1")||($database == "lbc_rf4")){ $weight = $row_rf3['weight']/1000; echo "<td>".number_format($weight,2)." kg";}
								else {$weight = $row_rf3['weight']; echo "<td>".number_format($weight,1)." kg";}

							}

							if($excel == "yes") {

								// Convert in kg if data is in gram, keep in kg if data is in kg
								if(($database == "lbc_rf1")||($database == "lbc_rf4")){ $weight = $row_rf3['weight']/1000;}
								else {$weight = $row_rf3['weight'];}

								// Example 2,440.20
								echo "<td>".number_format($weight,2,'.',',');
							}

							if($database == "lbc_rf4") {echo "<td>".$row_rf3['type_food']."</td>";}
							if($database == "lbc_rf1") {echo "<td>".$row_rf3['type_food']."</td>";}

							echo "</td>";
							echo "</tr>";

							// Calculate total
							if ($n == 1) {$total = $row_rf3['weight'];}
							if ($n > 1) {$total = $total + $row_rf3['weight'];}

		$n++; } while ($row_rf3 = mysql_fetch_assoc($rf3));


							echo "</tbody></table>";

							if($excel == "no") {
							if ($database == "lbc_rf1"){$total = $total / 1000;}
							echo "<div align='center' style='background-color:white;'><strong>Total ".number_format($total,2,".",","); echo " kg"; echo "</strong><br><br></div>";}

	} else {


							echo '<div align="center" style="background-color:white;"><br>No records available for your search.<br><br></div>';


		}

} // end of function
?>

<? 

	// Session and database parameters
	include 'database.php';

	// Variables
	$id_user = $_POST['id_user'];
	$id_company = $_POST['id_company'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$type = $_POST['type'];
	$name = $_POST['name'];
	$surname = $_POST['surname'];
	$company = $_POST['company'];
	$department = $_POST['department'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	
	$WRRF1 = $_POST['WRRF1'];
	$WRRF2 = $_POST['WRRF2'];
	$WRRF3 = $_POST['WRRF3'];
	$WRRF4 = $_POST['WRRF4'];
	$WRCovers = $_POST['WRCovers'];
	
	$ESSummary = $_POST['ESSummary'];
	$ESOutlets = $_POST['ESOutlets'];
	$ESCanteens = $_POST['ESCanteens'];
	$ESBanquet = $_POST['ESBanquet'];
	$ESOndemand = $_POST['ESOndemand'];
	
	$SETKitchens = $_POST['SETKitchens'];
	$SETUsers = $_POST['SETUsers'];
	
	$SETFoodType = $_POST['SETFoodType'];
	$SETSimplifiedRF4 = $_POST['SETSimplifiedRF4'];
	$SETSimplifiedES = $_POST['SETSimplifiedES'];
	
	// Changes caused by simplified selection
	if($SETSimplifiedRF4 == "yes"){ $WRRF4 = 'no'; }
	
	if($SETSimplifiedES == "yes"){ 
		
		$ESSummary = 'no';
		$ESOutlets = 'no';
		$ESCanteens = 'no';
		$ESBanquet = 'no';
		$ESOndemand = 'no';
		
	}
	
		// Update database
	if(($username != "")&&($password != "")&&($_SESSION['id_company'] == "lightblue")){
		
		$sql = sprintf("UPDATE lbc_users SET 
		id_company='$id_company', 
		username='$username', 
		password='$password',
		type='$type',
		name='$name',
		surname='$surname',
		company='$company',
		department='$department',
		email='$email',
		phone='$phone',
		WRRF1 = '$WRRF1',
		WRRF2 = '$WRRF2',
		WRRF3 = '$WRRF3',
		WRRF4 = '$WRRF4',
		WRCovers = '$WRCovers',
		ESSummary = '$ESSummary',
		ESOutlets = '$ESOutlets',
		ESCanteens = '$ESCanteens',
		ESBanquet = '$ESBanquet',
		ESOndemand = '$ESOndemand',
		SETKitchens = '$SETKitchens',
		SETUsers = '$SETUsers', 
		SETTypeFood = '$SETFoodType', 
		SETSimplifiedRF4 = '$SETSimplifiedRF4', 
		SETSimplifiedES = '$SETSimplifiedES'
		WHERE id_user='$id_user'");
		mysql_query($sql) or die($sql.'<br>'.mysql_error()); 
		
		header('Location: manage_accounts_users.php?s=updated&id_company='.$id_company.'');
		}
	else echo "<br><br><br><p align='center' style='font-family:arial; font-size: 14px;'><strong>Error updating the account.</strong><br>Please go back to the previous page, make sure that username and password are entered and try again.</p>";


?>
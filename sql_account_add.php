<? 

	// Session and database parameters
	include 'database.php';

	// Variables
	$id_company = $_POST['id_company'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$type = $_POST['type'];
	$name = $_POST['name'];
	$surname = $_POST['surname'];
	$company = $_POST['company'];
	$department = $_POST['department'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	
	// User restrictions
	if($type == "admin"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'yes';
		$WRRF3 = 'yes';
		$WRRF4 = 'yes';
		$WRCovers = 'yes';
		$ESSummary = 'yes';
		$ESOutlets = 'yes';
		$ESCanteens = 'yes';
		$ESBanquet = 'yes';
		$ESOndemand = 'yes';
		$SETKitchens = 'yes';
		$SETUsers = 'yes';
		$SETTypeFood = 'yes';
	}
	
	if($type == "employee"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'no';
		$WRRF3 = 'yes';
		$WRRF4 = 'no';
		$WRCovers = 'no';
		$ESSummary = 'yes';
		$ESOutlets = 'no';
		$ESCanteens = 'no';
		$ESBanquet = 'no';
		$ESOndemand = 'no';
		$SETKitchens = 'no';
		$SETUsers = 'no';
		$SETTypeFood = 'no';
	}
	
	if($type == "steward"){	
		$WRRF1 = 'no';
		$WRRF2 = 'no';
		$WRRF3 = 'no';
		$WRRF4 = 'yes';
		$WRCovers = 'no';
		$ESSummary = 'yes';
		$ESOutlets = 'no';
		$ESCanteens = 'no';
		$ESBanquet = 'no';
		$ESOndemand = 'no';
		$SETKitchens = 'no';
		$SETUsers = 'no';
		$SETTypeFood = 'no';
	}
	
	if($type == "champion"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'yes';
		$WRRF3 = 'yes';
		$WRRF4 = 'no';
		$WRCovers = 'no';
		$ESSummary = 'no';
		$ESOutlets = 'no';
		$ESCanteens = 'no';
		$ESBanquet = 'no';
		$ESOndemand = 'no';
		$SETKitchens = 'no';
		$SETUsers = 'no';
		$SETTypeFood = 'yes';
	}
	
	if($type == "secretarygeneral"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'yes';
		$WRRF3 = 'yes';
		$WRRF4 = 'yes';
		$WRCovers = 'yes';
		$ESSummary = 'yes';
		$ESOutlets = 'yes';
		$ESCanteens = 'yes';
		$ESBanquet = 'yes';
		$ESOndemand = 'yes';
		$SETKitchens = 'yes';
		$SETUsers = 'yes';
		$SETTypeFood = 'yes';
	}

	// Save in database
	if(($username != "")&&($password != "")&&($_SESSION['id_company'] == "lightblue")){
		
	$sql = "INSERT INTO lbc_users(id_company, username, password, type, name, surname, company, department, email, phone, WRRF1, WRRF2, WRRF3, WRRF4, WRCovers, ESSummary, ESOutlets, ESCanteens, ESBanquet, ESOndemand, SETKitchens, SETUsers, SETTypeFood) VALUES('$id_company', '$username', '$password', '$type', '$name', '$surname', '$company', '$department', '$email', '$phone', '$WRRF1', '$WRRF2', '$WRRF3', '$WRRF4', '$WRCovers', '$ESSummary', '$ESOutlets', '$ESCanteens', '$ESBanquet', '$ESOndemand', '$SETKitchens', '$SETUsers', '$SETTypeFood')"; 
	mysql_query($sql) or die($sql.'<br>'.mysql_error()); header('Location: manage_accounts_users.php?s=added&id_company='.$id_company.'');
	
	}
	
	else echo "<br><br><br><p align='center' style='font-family:arial; font-size: 14px;'><strong>Error creating the new account.</strong><br>Please go back to the previous page, make sure that required information are entered and try again.</p>";

?>
<? include 'database.php'; include 'functions.php'; include 'mysqli.php'; $id_company = $_SESSION['id_company'];?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
	<!-- HighCharts -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="//code.highcharts.com/highcharts.js"></script>
    <!-- HighCharts -->

	<!-- Google Analytics -->
	<?php include ('gtag.php'); ?>

</head>

<body>

<? if($_GET['type'] == "category_total00") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: { type: 'line'},
        title: {text: 'Total Food Waste from Outlets'},
		xAxis: {categories: [<? listOfKitchens($id_company); ?>], labels: {style: {fontSize:'14px'}}},
        yAxis: { title: {text: 'KG'}, labels: {style: {fontSize:'13px'}}},
		tooltip: {
            pointFormat: '<b>{point.y} kg</b>',
        },

		colors: ['#efefef', '#000', '#FFEB3B', '#2196F3', '#3F51B5'],

	   credits: { enabled: false},

        series: [
		{
			type:'column',
            name: 'Total',
            data: [
		<? // Load data
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			alltimeWasteKitchen('lbc_rf3', $id_company, $row['kitchen']);
			if($i < $numRows) {echo ",";}	 $i++;
		}

		// Free result
		$result->free(); ?>]
        }, {

            name: 'Spoilage',
            data: [
		<? // Load data
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'], 'Spoilage');
			if($i < $numRows) {echo ",";}	 $i++;
		}

		// Free result
		$result->free(); ?>]
        },

		 {

            name: 'Preparation',
            data: [
		<? // Load data
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'], 'Preparation');
			if($i < $numRows) {echo ",";}	 $i++;
		}

		// Free result
		$result->free(); ?>]
        },

		 {

            name: 'Buffet',
            data: [
		<? // Load data
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'], 'Buffet');
			if($i < $numRows) {echo ",";}	 $i++;
		}

		// Free result
		$result->free(); ?>]
        },

		 {

            name: 'Plate',
            data: [
		<? // Load data
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'], 'Plate');
			if($i < $numRows) {echo ",";}	 $i++;
		}

		// Free result
		$result->free(); ?>]

        }]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "category_total00") { ?>
<script>$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total Food Waste from Outlets'
        },
        xAxis: {
            categories: ['Spoilage', 'Preparation', 'Buffet', 'Plate']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'KG'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
		credits: { enabled: false},
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [<?
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		$numRows = $result->num_rows;

		$i = 1;

		while($row = $result->fetch_assoc()){

			echo "{";
			echo "name : '";
				echo $row['kitchen'];
			echo "',";

			echo "data : [";
			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'],'Spoilage');
			echo ", ";
			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'],'Preparation');
			echo ", ";
			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'],'Buffet');
			echo ", ";
			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row['kitchen'],'Plate');
			echo "]";

			echo "}";
			if($i < $numRows) {echo ",";}	 $i++;

		}

		?>]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "category_total01") { ?>
<script>$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total Food Waste from Outlets'
        },
		colors: ['#000', '#FFEB3B', '#F44336', '#3F51B5'],
        xAxis: {
            categories: [<?
		$sql_kit = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result_kit = $db->query($sql_kit)){ die('There was an error running the query [' . $db->error . ']');}

		$numRows_kit = $result_kit->num_rows;

		$i_kit = 1;

		while($row_kit = $result_kit->fetch_assoc()){

			echo "'";
			echo $row_kit['kitchen'];
			echo "'";
			if($i_kit < $numRows_kit) {echo ",";}	 $i_kit++;

		}

		?>],
			labels: {style: {fontSize:'16px'}}
        },
        yAxis: {
            min: 0,
            title: {
                text: 'KG',
				style: {fontSize:'14px'}
            },
            stackLabels: {
                enabled: true,
                format: '{total:,.0f} kg',
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray',
					fontSize: '14px'
                }

            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false,
			style: {fontSize:'14px'}
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal:,.0f}'
        },
		credits: { enabled: false},
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [
		 {
			name: 'Spoilage',
		 	data : [

			<?
		$sql_kit = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result_kit = $db->query($sql_kit)){ die('There was an error running the query [' . $db->error . ']');}

		$numRows_kit = $result_kit->num_rows;

		$i_kit = 1;

		while($row_kit = $result_kit->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row_kit['kitchen'],'Spoilage');
			if($i_kit < $numRows_kit) {echo ",";}	 $i_kit++;

		}

		?>


			]
		 },

		 {
			name: 'Preparation',
		 	data : [

			<?
		$sql_kit = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result_kit = $db->query($sql_kit)){ die('There was an error running the query [' . $db->error . ']');}

		$numRows_kit = $result_kit->num_rows;

		$i_kit = 1;

		while($row_kit = $result_kit->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row_kit['kitchen'],'Preparation');
			if($i_kit < $numRows_kit) {echo ",";}	 $i_kit++;

		}

		?>


			]
		 },

		 {
			name: 'Buffet',
		 	data : [

			<?
		$sql_kit = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result_kit = $db->query($sql_kit)){ die('There was an error running the query [' . $db->error . ']');}

		$numRows_kit = $result_kit->num_rows;

		$i_kit = 1;

		while($row_kit = $result_kit->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row_kit['kitchen'],'Buffet');
			if($i_kit < $numRows_kit) {echo ",";}	 $i_kit++;

		}

		?>


			]
		 },

		 {
			name: 'Plate',
		 	data : [

			<?
		$sql_kit = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result_kit = $db->query($sql_kit)){ die('There was an error running the query [' . $db->error . ']');}

		$numRows_kit = $result_kit->num_rows;

		$i_kit = 1;

		while($row_kit = $result_kit->fetch_assoc()){

			alltimeWasteKitchenCategory('lbc_rf3', $id_company, $row_kit['kitchen'],'Plate');
			if($i_kit < $numRows_kit) {echo ",";}	 $i_kit++;
			}

		?>

			]
		 }]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "category_total02") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },

		colors: ['#000', '#FFEB3B', '#F44336', '#3F51B5', 'grey'],

		credits: { enabled: false},
        title: {
            text: 'Food Waste by Category'
        },
        tooltip: {
            pointFormat: '<b>{point.y} kg</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: <br>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
						fontSize: '14px'
                    }
                }
            }
        },
        series: [{
            name: '%',
            colorByPoint: true,
            data: [{
                name: 'Spoilage',
                y: <? ExecutiveSummaryTotalWasteCategoryV2($id_company, 'spoilage');?>
            },
			{
                name: 'Preparation',
                y: <? ExecutiveSummaryTotalWasteCategoryV2($id_company, 'preparation');?>
            },
			{
                name: 'Buffet',
                y: <? ExecutiveSummaryTotalWasteCategoryV2($id_company, 'buffet');?>
            },
			{
                name: 'Plate',
                y: <? ExecutiveSummaryTotalWasteCategoryV2($id_company, 'plate');?>
            }]
        }]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "category_total03") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'

        },
		credits: { enabled: false},
		colors: ['#fbbe40', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
             '#FF9655', '#FFF263'],
        title: {
            text: 'Food Waste by Recording Points'
        },
        tooltip: {
            pointFormat: '<b>{point.y:,.1f} kg</b>',
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: <br>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
						fontSize: '14px'
                    }
                }
            }
        },
        series: [{
            name: '%',
            colorByPoint: true,

            data: [
			<?

		// Load list of kitchens
		$sql_prep = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company'";
		if(!$result_prep = $db->query($sql_prep)){ die('There was an error running the query [' . $db->error . ']');}

	   // $row_prep = mysqli_fetch_assoc($result_prep);
		$numRows_prep = $result_prep->num_rows;

		$i = 1;

		while($row_prep = $result_prep->fetch_assoc()){

		//for($n = 1; $n <= $numRows_prep; $n++) {

			echo "{ name: '". $row_prep['kitchen'] ."', y:";
			totalReportRecordingPoint('lbc_rf3',$id_company,$row_prep['kitchen']);
			echo " }";

			if($i < $numRows_prep) {echo ",";}
			$i++;

		}

		?>]
        }]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "category_total03ARCHIVED") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            events: {
				load: function(event) {
				var total = Math.round(this.series[0].data[0].total);

				var text = this.renderer.text(
               	 'Total: ' + total + ' kg',
			   	 this.plotLeft,
			   	 this.plotTop - 20
			   	 ).attr({
			   	 	zIndex: 5
            }).add()
        }
    }
        },
		credits: { enabled: false},
		colors: ['#fbbe40', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
             '#FF9655', '#FFF263'],
        title: {
            text: 'Food Waste by Recording Points'
        },
        tooltip: {
            pointFormat: '<b>{point.y:,.2f} kg</b>',
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: <br>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
						fontSize: '14px'
                    }
                }
            }
        },
        series: [{
            name: '%',
            colorByPoint: true,

            data: [
			<?

		// Load list of kitchens
		$sql_prep = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company'";
		if(!$result_prep = $db->query($sql_prep)){ die('There was an error running the query [' . $db->error . ']');}

	   // $row_prep = mysqli_fetch_assoc($result_prep);
		$numRows_prep = $result_prep->num_rows;

		$i = 1;

		while($row_prep = $result_prep->fetch_assoc()){

		//for($n = 1; $n <= $numRows_prep; $n++) {

			echo "{ name: '". $row_prep['kitchen'] ."', y:";
			totalReportRecordingPoint('lbc_rf3',$id_company,$row_prep['kitchen']);
			echo " }";

			if($i < $numRows_prep) {echo ",";}
			$i++;

		}

		?>]
        }]
    });
});
</script>
<? } ?>

<? if($_GET['type'] == "category_total04") { ?>
<script>
$(function () {
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
		credits: { enabled: false},
		colors: ['#000', '#FFEB3B', '#2196F3', '#2196F3', '#3F51B5'],
        title: {
            text: 'Food Waste from Canteens and Banquets'
        },
        tooltip: {
            pointFormat: '<b>{point.y} kg</b>',
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: <br>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
						fontSize: '14px'
                    }
                }
            }
        },
        series: [{
            name: '%',
            colorByPoint: true,
            data: [
			{
				name: 'Spoilage (Canteen)',

				y: <? totalReportCategoryType('lbc_rf3',$id_company,'spoilage', 'Canteen'); ?>
			},
			{
				name: 'Preparation (Canteen)',
				y: <? totalReportCategoryType('lbc_rf3',$id_company,'preparation', 'Canteen'); ?>
			},{
				name: 'Buffet (Canteen)',
				y: <? totalReportCategoryType('lbc_rf3',$id_company,'buffet', 'Canteen'); ?>
			},{
				name: 'Buffet (Banquet)',
				y: <? totalReportCategoryType('lbc_rf3',$id_company,'buffet', 'Banquet'); ?>
			},{

				name: 'Plate (Canteen)',
				y: <? totalReportCategoryType('lbc_rf3',$id_company,'plate', 'Canteen'); ?>
			}]

        }]
    });
});
</script>
<? } ?>

<div id="container" style="width:100%; height:350px;"></div>

</body>
</html>

<?php
    /* INCLUDES */
    include('database.php');
    include('functions.php');
    include('mysqli.php');
    include('sidebar.php');

    /* VARIABLES */
    $id_user = $_SESSION['username'];
    $id_company = $_SESSION['id_company'];
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include('gtag.php'); ?>
    </head>

    <body>

       <?php include('header.php'); ?>

       <section id="main">

          <?php sideBar('executivesummary','report_outlet'); ?>

          <section id="content">
              <div class="container">

              <div class="block-header">
                <h2>Outlet food waste</h2>
              </div>

              <div class="card-padding card-body" align="center">
                <a href="report_outlet.php?tab=recordingpoint">
                  <button class="btn <?php if($_GET['tab'] == "recordingpoint") { echo "bgm-black"; } else { echo "btn-default"; } ?>" style="box-shadow:none;margin:10px;">Recording Points</button>
                </a>

                <a href="report_outlet.php?tab=shift">
                  <button class="btn <?php if($_GET['tab'] == "shift") { echo "bgm-black"; } else { echo "btn-default"; } ?>" style="box-shadow:none;">Shifts</button>
                </a>

                <a href="report_outlet.php?tab=category">
                  <button class="btn <?php if($_GET['tab'] == "category") { echo "bgm-black"; } else { echo "btn-default"; } ?>" style="box-shadow:none;margin:10px;">Category of Waste</button>
                </a><br><br>
              </div>

<?php if($_GET['tab'] == "recordingpoint") { ?>

    <div class="card">
      <iframe height="400px" width="100%" src="chart_outlet.php?type=recordingpoints" frameborder="0"></iframe>
    </div>

    <div class="block-header">
      <h2>Total food waste</h2>
    </div>

    <div class="card">

    <div class="table-responsive">
      <table class="table table-bordered">
      <thead>
          <tr>
            <th>Days</th>
            <?php foreach ($_SESSION['kitchens_outlets'] as $kitchens_outlets) { echo '<th>'.$kitchens_outlets."</th>"; }?>
            <th><strong>Total waste</strong></th>
          </tr>
      </thead>

      <tbody>

        <?php foreach ($_SESSION['days_all'] as $days_all) { ?>

        <tr>
            <td><?php echo date("D d M y", strtotime($days_all)) ?></td>
            <?php foreach ($_SESSION['kitchens_outlets'] as $kitchens_outlets) {
              $date_waste = date("Y-m-d", strtotime($days_all));
              $variable_name = md5($kitchens_outlets.$date_waste);
              echo '<td>'; echo array_sum($_SESSION[$variable_name]); echo ' kg</td>';
            }?>
            <td><strong><?php dailyWasteTotal('lbc_rf3', $id_company, $days_all) ?> kg</strong></td>
        </tr>
        <?php } ?>



        <tr>
        <td><strong>All days</strong></td>

        <?php foreach ($_SESSION['kitchens_outlets'] as $kitchens_outlets) { ?>
        <td><strong><?php $total_kitchen = md5($kitchens_outlets); echo array_sum($_SESSION[$total_kitchen]); ?> kg</strong></td>
        <?php } ?>

        <td><strong><?php dailyWasteTotal('lbc_rf3', $id_company, 'NULL') ?> kg</strong></td>
        </tr>

      </tbody>
      </table>
    </div>
    </div>


<!--PER COVER-->
<div class="block-header">
<h2>Food waste per cover</h2>
</div>
<div class="card">

<div class="table-responsive">
<table class="table table-bordered">
<thead>
<tr>
<th>Days</th>

<?php  // <th> Kitchens
$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}
$row=mysqli_fetch_assoc($result);

$numRows = $result->num_rows;

//while($row = $result->fetch_assoc()){
$i = 1; do {
echo '<th>'.$row['kitchen'].'</th>'	;
} while ($row = mysqli_fetch_assoc($result));

?>
<th><strong>Ratio per cover</strong></th>
</tr>
</thead>
<tbody>

<?php  // Load list of days
$sql_days = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
if(!$result_days = $db->query($sql_days)){ die('There was an error running the query [' . $db->error . ']');}

$row_days=mysqli_fetch_assoc($result_days);
$numRows_days = $result_days->num_rows;

$n=1; do { ?>

<tr>
<td><?php echo date("D d M y", strtotime($row_days['date_waste'])) ?></td>

<?php  $sql_kitchens = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
if(!$result_kitchens = $db->query($sql_kitchens)){ die('There was an error running the query [' . $db->error . ']');}

$row_kitchens = mysqli_fetch_assoc($result_kitchens);
$numRows_kitchens = $result_kitchens->num_rows;

$i = 1; do {
echo '<td>'; dailyWasteKitchenCover('lbc_rf3', $id_company, $row_days['date_waste'],$row_kitchens['kitchen']); echo ' g/cover</td>';
} while ($row_kitchens = mysqli_fetch_assoc($result_kitchens));?>

<td><strong><?php dailyWasteTotalCover('lbc_rf3', $id_company, $row_days['date_waste'], 'Outlet') ?> g/cover</strong></td>


</tr>

<?php $n++;

} while ($row_days = mysqli_fetch_assoc($result_days));
?>

<tr>
<td><strong>All days</strong></td>

<?php  $sql_kitchens_alldays = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
if(!$result_kitchens_alldays = $db->query($sql_kitchens_alldays)){ die('There was an error running the query [' . $db->error . ']');}

$row_kitchens_alldays = mysqli_fetch_assoc($result_kitchens_alldays);
$numRows_kitchens_alldays = $result_kitchens_alldays->num_rows;

$i = 1; do {
echo '<td><strong>'; dailyWasteKitchenCoverAllDays('lbc_rf3', $id_company, $row_kitchens_alldays['kitchen']); echo ' g/cover</strong> </td>';
} while ($row_kitchens_alldays = mysqli_fetch_assoc($result_kitchens_alldays));?>

<td><strong><?php dailyWasteTotalCoverAllDays('lbc_rf3', $id_company, 'Outlet') ?> g/cover</strong></td>


</tr>

</tbody>
</table>
</div>
</div>


<?php } ?>

<?php if($_GET['tab'] == "shift") { ?>

                  <div class="card">
                    <iframe height="400px" width="100%" src="chart_outlet.php?type=shift" frameborder="0"></iframe>
                  </div>

                   <div class="block-header">
                     <h2>Total food waste</h2>
                    </div>

                   <div class="card">

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Days</th>
                                        <th>Breakfast</th>
                                        <th>Lunch</th>
                                        <th>Dinner</th>
                                        <th>Night Shift</th>
                                        <th><strong>Total waste</strong></th>
                                    </tr>
                                </thead>

                                <tbody>

                 <?php // Load list of days
				$sql_days_total = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days_total = $db->query($sql_days_total)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days_total = mysqli_fetch_assoc($result_days_total);
				$numRows_days_total = $result_days_total->num_rows;

				$n=1; do { ?>

                    <tr>
                        <td><?php echo date("D d M y", strtotime($row_days_total['date_waste'])) ?></td>
                        <td><?php dailyWasteShift('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Breakfast'); ?> kg</td>
                        <td><?php dailyWasteShift('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Lunch'); ?> kg</td>
                        <td><?php dailyWasteShift('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Dinner'); ?> kg</td>
                        <td><?php dailyWasteShift('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Night Shift'); ?> kg</td>
                        <td><strong><?php dailyWasteTotal('lbc_rf3', $id_company, $row_days_total['date_waste']) ?> kg</strong></td>
                    </tr>

				<?php $n++; } while ($row_days_total = mysqli_fetch_assoc($result_days_total)); ?>

                	 <tr>
                        <td><strong>All days</strong></td>
                        <td><strong><?php dailyWasteShift('lbc_rf3', $id_company, 'NULL', 'Breakfast'); ?> kg</strong></td>
                        <td><strong><?php dailyWasteShift('lbc_rf3', $id_company, 'NULL', 'Lunch'); ?> kg</strong></td>
                        <td><strong><?php dailyWasteShift('lbc_rf3', $id_company, 'NULL', 'Dinner'); ?> kg</strong></td>
                        <td><strong><?php dailyWasteShift('lbc_rf3', $id_company, 'NULL', 'Night Shift'); ?> kg</strong></td>
                        <td><strong><?php dailyWasteTotal('lbc_rf3', $id_company, 'NULL') ?> kg</strong></td>
                    </tr>

                                </tbody>
                            </table>
                        </div>
                  </div>

                  <!-- PER COVER -->
                  <div class="block-header">
                     <h2>Food waste per cover</h2>
                    </div>
                  <div class="card">

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Days</th>
                                        <th>Breakfast</th>
                                        <th>Lunch</th>
                                        <th>Dinner</th>
                                        <th>Night Shift</th>
                                        <th><strong>Ratio per cover</strong></th>
                                    </tr>
                                </thead>
                                <tbody>

                 <?php // Load list of days
				$sql_days_total = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days_total = $db->query($sql_days_total)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days_total = mysqli_fetch_assoc($result_days_total);
				$numRows_days_total = $result_days_total->num_rows;

				$n=1; do { ?>

                    <tr>
                        <td><?php echo date("D d M y", strtotime($row_days_total['date_waste'])) ?></td>
                        <td><?php dailyWasteShiftCover('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Breakfast', 'Outlet'); ?>
                        g/cover</td>
                        <td><?php dailyWasteShiftCover('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Lunch', 'Outlet'); ?>
                        g/cover</td>
                        <td><?php dailyWasteShiftCover('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Dinner', 'Outlet'); ?>
                        g/cover</td>
                        <td><?php dailyWasteShiftCover('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Night Shift', 'Outlet'); ?>
                        g/cover</td>
                        <td><strong><?php dailyWasteTotalCover('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Outlet') ?></strong> g/cover</td>
                    </tr>

				<?php $n++; } while ($row_days_total = mysqli_fetch_assoc($result_days_total)); ?>

                 <tr>
                        <td><strong>All days</strong></td>
                        <td><strong><?php dailyWasteShiftCoverAllDays('lbc_rf3', $id_company, 'Breakfast', 'Outlet'); ?>
                        g/cover</strong></td>
                        <td><strong><?php dailyWasteShiftCoverAllDays('lbc_rf3', $id_company, 'Lunch', 'Outlet'); ?>
                        g/cover</strong></td>
                        <td><strong><?php dailyWasteShiftCoverAllDays('lbc_rf3', $id_company, 'Dinner', 'Outlet'); ?>
                        g/cover</strong></td>
                        <td><strong><?php dailyWasteShiftCoverAllDays('lbc_rf3', $id_company, 'Night Shift', 'Outlet'); ?>
                        g/cover</strong></td>
                        <td><strong><?php dailyWasteTotalCoverAllDays('lbc_rf3', $id_company, 'Outlet') ?></strong> g/cover</td>
                    </tr>

                                </tbody>
                            </table>
                        </div>
                  </div>

<?php } ?>

<?php if($_GET['tab'] == "category") { ?>

                  <div class="card">
                    <iframe height="400px" width="100%" src="chart_outlet.php?type=category" frameborder="0"></iframe>
                    </div>

                   <div class="block-header">
                     <h2>Total food waste</h2>
                    </div>
                   <div class="card">

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Days</th>
                                        <th><span style="color:#000;">Spoilage</span></th>
                                        <th><span style="color:#FFC107;">Preparation</span></th>
                                        <th><span style="color:#2196F3;">Plate</span></th>
                                        <th><span style="color:#F44336;">Buffet</span></th>
                                        <th><strong>Total waste</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php // Load list of days
				$sql_days_total = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
				if(!$result_days_total = $db->query($sql_days_total)){ die('There was an error running the query [' . $db->error . ']');}

				$row_days_total = mysqli_fetch_assoc($result_days_total);
				$numRows_days_total = $result_days_total->num_rows;

				$n=1; do { ?>

                    <tr>
                        <td><?php echo date("D d M y", strtotime($row_days_total['date_waste'])) ?></td>
                        <td><?php dailyWasteCategory('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Spoilage'); ?> kg</td>
                        <td><?php dailyWasteCategory('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Preparation'); ?> kg</td>
                        <td><?php dailyWasteCategory('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Plate'); ?> kg</td>
                        <td><?php dailyWasteCategory('lbc_rf3', $id_company, $row_days_total['date_waste'], 'Buffet'); ?> kg</td>
                        <td><strong><?php dailyWasteTotal('lbc_rf3', $id_company, $row_days_total['date_waste']) ?> kg</strong></td>
                    </tr>

				<?php $n++; } while ($row_days_total = mysqli_fetch_assoc($result_days_total)); ?>

                 <tr>
                        <td><strong>All days</strong></td>
                        <td><strong><?php dailyWasteCategory('lbc_rf3', $id_company, 'NULL', 'Spoilage'); ?> kg</strong></td>
                        <td><strong><?php dailyWasteCategory('lbc_rf3', $id_company, 'NULL', 'Preparation'); ?> kg</strong></td>
                        <td><strong><?php dailyWasteCategory('lbc_rf3', $id_company, 'NULL', 'Plate'); ?> kg</strong></td>
                        <td><strong><?php dailyWasteCategory('lbc_rf3', $id_company, 'NULL', 'Buffet'); ?> kg</strong></td>
                        <td><strong><strong><?php dailyWasteTotal('lbc_rf3', $id_company, 'NULL') ?> kg</strong></strong></td>
                    </tr>
                                </tbody>
                            </table>
                        </div>
                  </div>

  <?php } ?>

                </div>
            </section>
        </section>

        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

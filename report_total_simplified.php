<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company']; ?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include ('gtag.php'); ?>
    </head>
    <body>

       <? include 'header.php';?>

       <section id="main">

           <? include 'sidebar.php'; sideBar('simplifiedSumary','report_total'); ?>

            <section id="content">
                <div class="container">

                     <div class="block-header">
                     <h2>Simplified Summary of food waste

                     <small>Data from
        <?  // Open Mysql connection
			$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
			if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

			// From
			$sql = "SELECT MIN(date_waste) FROM lbc_rf3 WHERE id_company = '$id_company'";
			$result = $db->query($sql); $from = mysqli_fetch_array($result);

			// To
			$sql_to = "SELECT MAX(date_waste) FROM lbc_rf3 WHERE id_company = '$id_company'";
			$result_to = $db->query($sql_to); $to = mysqli_fetch_array($result_to);

			$date_from = date_create($from[0]);
			echo date_format($date_from,"d/m/Y");

			echo " to ";

			$date_to = date_create($to[0]);
			echo date_format($date_to,"d/m/Y");?>

                     </small>

                     </h2>
                     </div>


                                 <div class="card">

                                    <div class="p-20">

                              <div class="row">
                  				 <div class="col-sm-6" style="margin-bottom: 15px;">

                                        <strong>Total Food Waste
(all venues)                                       </strong>
<h3 class="m-0 f-400"><? include 'functions.php'; ExecutiveSummaryTotalFoodWaste($id_company); ?> kg</h3>

                                        <br>

                                       <strong>% Pre-consumer Food Waste
(excl. canteen)                                      </strong>
<h3 class="m-0 f-400"><? preconsumerFoodWaste($id_company); ?></h3>

                                       <br>

                                       <strong> Food Waste per cover (excl. canteen)</strong>
                                       <h3 class="m-0 f-400"><? allWasteCover('lbc_rf3', $id_company,'Canteen'); ?> g/cover</h3>

                                 </div>
                                 <div class="col-sm-6">
                                      <img src="images/co2-inside-cloud.png" width="30" height="30"/>
                                      <strong>  &nbsp;&nbsp;Co2 equivalent</strong>
                                      <h3 class="m-0 f-400"><? ExecutiveSummaryCO2Equivalent($id_company); ?> kg</h3>

                                      <br>

                                      <img src="images/touching-belly-silhouette.png" width="30" height="30"/>
                                      <strong>  &nbsp;&nbsp;Enough food to feed</strong>
                                      <h3 class="m-0 f-400"><? ExecutiveSummaryFamilyFedEquivalent($id_company); ?> people</h3>

                                      <br>

                                      <img src="images/give-money.png" width="30" height="30"/>
                                      <strong> &nbsp;&nbsp;Money loss</strong>
                                      <h3 class="m-0 f-400"><? ExecutiveSummaryCashEquivalent($id_company); ?> USD</h3>

                                 </div>
                              </div>
                                </div>
               				</div>



                 <div class="card">
                    <iframe height="400px" width="100%" src="chart.php?type=category_total01" frameborder="0"></iframe>
                 </div>


 <div class="card">
    <div class="card-body card-padding">

      	<div class="row">

                <p class="f-500 m-b-20 c-black">Most discarded type of food</p>

                <ul class="list-group">
     <? // Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		$sql = "SELECT *, SUM(weight) FROM lbc_rf1 WHERE id_company = '$id_company' GROUP BY type_food ORDER BY SUM(weight) DESC LIMIT 5";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			if($row['type_food'] == "Staple food") { $icon = '<img src="images/icons/staple_food.png" width="30">'; }
			if($row['type_food'] == "Vegetable") { $icon = '<img src="images/icons/vegetable.png" width="30">'; }
			if($row['type_food'] == "Fruit") { $icon = '<img src="images/icons/fruit.png" width="30">'; }
			if($row['type_food'] == "Meat"){ $icon = '<img src="images/icons/meat.png" width="30">'; }
			if($row['type_food'] == "Seafood") { $icon = '<img src="images/icons/seafood.png" width="30">'; }
			if($row['type_food'] == "Dairy") { $icon = '<img src="images/icons/dairy.png" width="30">'; }

			if($row['type_food'] != "") {
				echo '<li class="list-group-item">'.$icon;
				//echo "    ".$i.". ";
				echo "    ".$row['type_food']." ("; ;
				$weightKG_tf = $row['SUM(weight)'] /1000; echo number_format($weightKG_tf,2,",",".")." kg)";
				echo '</li>';
				$i++;
			}
		}


		// Free result
		$result->free(); ?>

                </ul>


            <!--<div class="col-sm-6 m-b-20">
                <p class="f-500 m-b-20 c-black">Most recurrent reason for waste</p>

                <ul class="list-group">
        <? // Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		$sql = "SELECT *, SUM(weight) FROM lbc_rf1 WHERE id_company = '$id_company' GROUP BY reason_waste ORDER BY COUNT(reason_waste) DESC LIMIT 5";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){
			if($row['reason_waste'] != "") {
				echo '<li class="list-group-item">';
				echo $i.". "; echo $row['reason_waste']." (";
				$weightKG = $row['SUM(weight)'] / 1000; echo number_format($weightKG,2,",",".")." kg)";
				echo '</li>';
				$i++;
			}
		}

		// Free result
		$result->free(); ?>
                </ul>
            </div> -->
        </div>
	</div>
</div>
               <div class="row">

                  <div class="col-sm-6">
                      <div class="card">
                        <iframe height="400px" width="100%" src="chart.php?type=category_total02" frameborder="0"></iframe>
                     </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="card">
                        <iframe height="400px" width="100%" src="chart.php?type=category_total03" frameborder="0"></iframe>
                     </div>
                  </div

                  </div>
              </div> <!-- End of row -->


              	  <!-- Deleted from request of 1st July
                  <div class="card">
                  <iframe height="400px" width="100%" src="chart.php?type=category_total04" frameborder="0"></iframe>
             	  </div>
                  -->




            </section>
        </section>

        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

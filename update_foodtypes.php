<?  include 'database.php'; $id_foodtype = $_GET['id_foodtype'];

	$query = "SELECT * FROM lbc_foodtypes WHERE id_foodtype = '$id_foodtype'";
	$foodtypes = mysql_query($query, $db) or die(mysql_error());
	$row_foodtypes = mysql_fetch_assoc($foodtypes);
	$numberRowsFoodTypes  = mysql_num_rows($foodtypes);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

				<!-- Google Analytics -->
				<?php include ('gtag.php'); ?>

    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('settings','type_food'); ?>

            <section id="content">
                <div class="container">

                    <div class="block-header">
                        <h2>Update type of food</h2>

                        <ul class="actions">
                            <li>
                                <a onclick="return confirm('Are you sure you want to delete <? echo $row_foodtypes['title']; ?>?');" href="sql_foodtypes_delete.php?id_company=<? echo $row_foodtypes['id_company']; ?>&id_foodtype=<? echo $row_foodtypes['id_foodtype']; ?>&u=<? echo $row_foodtypes['image_url']; ?>">
                                    <i class="zmdi zmdi-delete"></i>
                                </a>
                            </li>
                        </ul>

                    </div>


                  <form action="sql_foodtypes_update.php" method="post" enctype="multipart/form-data" id="updatefoodtype">
                  <input type="hidden" name="id_foodtype" value="<? echo $row_foodtypes['id_foodtype']; ?>">
                  <input type="hidden" name="id_company" value="<? echo $row_foodtypes['id_company']; ?>">

                    <div class="card">

    <div class="card-header">
    	<h2>Title</h2>

        <ul class="actions">
            <li>
                <a href="manage_foodtypes.php">
                <i class="zmdi zmdi-close"></i>
                </a>
            </li>
        </ul>
    </div>

    <div class="card-body card-padding">

        <div class="form-group">
          <div class="fg-line">
          <input type="text" class="form-control" name="title" value="<? echo $row_foodtypes['title']; ?>">
          </div>
        </div>

    </div>
</div>


<div class="card">

    <div class="card-header">
    	<h2>Upload image<small>Leave empty if unchanged</small></h2>
    </div>

    <div class="card-body card-padding">

        <div class="fileinput fileinput-new" data-provides="fileinput">
        <span class="btn bgm-lightblue btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
        <input type="file" name="url_en" id="url_en"></span>
        <span class="fileinput-filename"></span><a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
        </div>

    </div>
</div>

<div class="card">

    <div class="card-header">
    	<h2>Keywords</h2>
    </div>

    <div class="card-body card-padding">

    <div class="form-group">
    <div class="fg-line">
    <textarea class="form-control auto-size" data-autosize-on="true" name="keywords"><? echo $row_foodtypes['keywords']; ?></textarea>
    </div>
    </div>

    </div>
</div>

<button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
<i class="zmdi zmdi-check-all"></i>Update</button>
</form>


                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

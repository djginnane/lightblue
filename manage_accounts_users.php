<?php
	/* Database */
	include 'mysqli.php';

	/* Variables */
	$id_user = $_SESSION['username'];
	if($_GET['id_company']){$id_company = $_GET['id_company'];}
	elseif($_POST['id_company']){$id_company = $_POST['id_company'];}

	/* Create new company */
	if($_POST['s'] == "create_new_company") {
		$sql_company_add = "INSERT INTO lbc_companies (id_company) VALUES ('$id_company')";
		if(!$result_company_add = $db->query($sql_company_add)){ die('There was an error running the query [' . $db->error . ']');}
	}

	/* Delete company */
	if(($_POST['s'] == "delete_company") && ($_SESSION['id_company'] == "lightblue")) {
		$sql_del_company = "DELETE FROM lbc_companies WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_company) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_users = "DELETE FROM lbc_users WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_users) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_covers = "DELETE FROM lbc_covers WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_covers) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_foodtypes = "DELETE FROM lbc_foodtypes WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_foodtypes) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_kitchens = "DELETE FROM lbc_kitchens WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_kitchens) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_rf1 = "DELETE FROM lbc_rf1 WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_rf1) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_rf2 = "DELETE FROM lbc_rf2 WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_rf2) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_rf3 = "DELETE FROM lbc_rf3 WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_rf3) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		$sql_del_rf4 = "DELETE FROM lbc_rf4 WHERE id_company = '$id_company'";
	  if ($db->query($sql_del_rf4) === TRUE) {} else {echo "Error deleting record: " . $db->error;}

		echo "<script language=\"JavaScript\"> document.location.replace(\"manage_accounts.php\");</script>";
	}

	/* Update baseline */
	if($_POST['baseline_start'] || $_POST['baseline_end']){
		$baseline_start = substr($_POST['baseline_start'],6,4).'-'.substr($_POST['baseline_start'],3,2).'-'.substr($_POST['baseline_start'],0,2);
		$baseline_end = substr($_POST['baseline_end'],6,4).'-'.substr($_POST['baseline_end'],3,2).'-'.substr($_POST['baseline_end'],0,2);

		$sql_company_upd = "UPDATE lbc_companies SET baseline_start = '$baseline_start', baseline_end = '$baseline_end' WHERE id_company = '$id_company'";
	  if ($db->query($sql_company_upd) === TRUE) {} else {echo "Error updating record: " . $db->error;}
	}

	/* Queries */
	$sql = "SELECT * FROM lbc_users WHERE id_company = '$id_company' ORDER BY username ASC";
	$result = $db->query($sql);

	$sql_company = "SELECT * FROM lbc_companies WHERE id_company = '$id_company'";
	$result_company = $db->query($sql_company);
	$row_company = mysqli_fetch_assoc($result_company);

	$baseline_start_display = substr($row_company['baseline_start'],8,2).'/'.substr($row_company['baseline_start'],5,2).'/'.substr($row_company['baseline_start'],0,4);
	$baseline_end_display = substr($row_company['baseline_end'],8,2).'/'.substr($row_company['baseline_end'],5,2).'/'.substr($row_company['baseline_end'],0,4);


?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

         <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>
    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('admin','manage_accounts'); ?>

					<ol class="breadcrumb">
							<li><a href="manage_accounts.php">Accounts</a></li>
							<?php if(($id_company)&&($_GET['s'] != "addnew")) { ?><li class="active"> <?php echo $id_company; ?></li><?php } ?>
							<?php if($_GET['s'] == "addnew") { ?><li><a href="manage_accounts_users.php?id_company=<? echo $id_company; ?>"><? echo $id_company; ?></a></li><li class="active">Add new</li><?php } ?>
					</ol>

            <section id="content">
                <div class="container">

<? if($_GET['s'] == "added") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully added a new account.
</div>
<? } ?>

<? if(($_GET['s'] == "updated")||($_POST['s'] == "updated")) { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully updated the account information.
</div>
<? } ?>

<? if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Well done! You successfully deleted the account.
</div>
<? } ?>

<? if(($_GET['s'] == "addnew") || ($_POST['s'] == "create_new_company")) { ?>
                  <form action="sql_account_add.php" method="post" id="updateaccount">
                    <div class="card">

                    <div class="card-header">
                  <h2>Add new account
										<small><? if($id_company) echo $id_company; ?> <input type="hidden" name="id_company" value="<?php echo $id_company;?>"></small></h2>

                  <ul class="actions">
                            <li>
                                <a href="manage_accounts_users.php?id_company=<? echo $_SESSION['id_company']; ?>">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                  </div>

                        <div class="card-body card-padding">

                            <div class="row">

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="username"></div>
                                  <label class="fg-label">Username</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="password" class="input-sm form-control fg-input" name="password"></div>
                                  <label class="fg-label">Password</label>
                                  </div>

                                </div>
                            </div>

                             <div class="form-group">
                             <div class="fg-line">
                             <div class="select">
              <select class="form-control" name="type">
              <option value="undefined">Select a type of account</option>
              <option value="employee">Employee</option>
              <option value="steward">Steward</option>
              <option value="champion">Champion</option>
              <option value="secretarygeneral">Secretary general</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>


                            <div class="row">
                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="name"></div>
                                  <label class="fg-label">Name</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="surname"></div>
                                  <label class="fg-label">Surname</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="company"></div>
                                  <label class="fg-label">Company</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="department"></div>
                                  <label class="fg-label">Department</label>
                                  </div>

                                </div>

                                  <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="email" class="input-sm form-control fg-input" name="email"></div>
                                  <label class="fg-label">Email</label>
                                  </div>

                                </div>

                                <div class="col-sm-6">

                                  <div class="form-group fg-float">
                                  <div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="phone"></div>
                                  <label class="fg-label">Mobile</label>
                                  </div>

                                </div>

                                <div align="center">
                                <button type="submit" class="btn btn-default bgm-lightblue btn-icon-text">
                                <i class="zmdi zmdi-check-all"></i>Create account</button>
                                </div>
                            </div>

                        </div>
					</div>



</form>

		<? } else { ?>

		<div class="card">
				<div class="card-header">
						<h2>Login accounts<small><? echo $id_company; ?></small></h2>

						<ul class="actions">
								<li>
									<a href="manage_accounts_users.php?s=addnew&id_company=<? echo $id_company ;?>"><i class="zmdi zmdi-account-add"></i></a>
								</li>
						</ul>
				</div>

				<div class="card-body card-padding">
						<div class="card-body table-responsive">
								<table class="table table-hover">
										<thead>
												<tr>
													<th>Username</th>
													<th>Company ID</th>
													<th>Company</th>
													<th>Department</th>
													<th>Email</th>
													<th>Type</th>
												</tr>
										</thead>

										<tbody>
												<?php if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
												<tr>
														<td><a href="update_account.php?id_user=<? echo $row['id_user']; ?>"><? echo $row['username']; ?></a></td>
														<td><? echo $row['id_company']; ?></td>
														<td><? echo $row['company']; ?></td>
														<td><? echo $row['department']; ?></td>
														<td><? echo $row['email']; ?></td>
														<td><? echo $row['type']; ?></td>
												</tr>
												<?php } } ?>
										</tbody>
								</table>
						</div>
				</div>
		</div>

		<div class="card">
				<div class="card-header">
						<h2>Baseline<small><? echo $id_company; ?> </small></h2>
				</div>

				<div class="card-body card-padding">
						<form action="manage_accounts_users.php" method="post">
									<input type="hidden" name="s" value="updated">
									<input type="hidden" name="id_company" value="<?php echo $id_company; ?>">
						<div class="row">
								<div class="col-sm-4">
										<div class="input-group form-group" >
												<span class="input-group-addon"><i class="zmdi zmdi-calendar-alt"></i></span>
												<div class="dtp-container fg-line">
														<input type='text' class="form-control date-picker" placeholder="From" id="baseline_start" name="baseline_start" value="<?php if($baseline_start_display != "") {echo $baseline_start_display;} ?>">
												</div>
										</div>
								</div>

								<div class="col-sm-4">
										<div class="input-group form-group" >
												<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
												<div class="dtp-container fg-line">
														<input type='text' class="form-control date-picker" placeholder="To" id="baseline_end" name="baseline_end" value="<?php if($baseline_end_display != "") {echo $baseline_end_display;} ?>">
												</div>
										</div>
								</div>

								<div class="col-sm-4" align='center'>
									<button type="submit" class="btn btn-default btn-block btn-icon-text"><i class="zmdi zmdi-refresh"></i> Update</button>
								</div>
						</div>
					</form>

				</div>
	 </div>

	 <div class="card">
			 <div class="card-header">
					 <h2>Cost per kilo<small>This setting will be applied to all users of the company</small></h2>
			 </div>

			 <div class="card-body card-padding">
					 <form action="sql_account_update_costkilo.php" method="post">
								 				<input type="hidden" name="id_company" value="<?php echo $id_company; ?>">
							 <div class="row">
									 <div class="col-sm-8">
											<div class="form-group">
												 	<div class="col-sm-12">
														<input type="text" class="form-control" name="cost_kilo" value="<?php if($_SESSION['cost_kilo'] != "") {echo $_SESSION['cost_kilo'];}?>">
													</div>
											</div>
									 </div>

									 <div class="col-sm-4" align='center'>
										 	<button type="submit" class="btn btn-default btn-block btn-icon-text"><i class="zmdi zmdi-refresh"></i> Update</button>
									 </div>
							 </div>
				 </form>

			 </div>
	</div>

	 <div class="row">
		 	<div class="col-sm-6">
				 <div class="card">
						 <div class="card-header">
								 <h2>Duplicate company<small>This will copy all company's account and data for testing purposes</small></h2>
						 </div>

						 <div class="card-body card-padding">
								 <form action="duplicate_company.php" method="post">

											 <input type="hidden" name="s" value="duplicate_company">
											 <input type="hidden" name="id_company" value="<?php echo $id_company; ?>">

								 <div class="row">
										 <div class="col-sm-4" align='center'>
											 <button type="submit" class="btn btn-warning btn-block btn-icon-text" onclick="return confirm('Are you sure you want to duplicate all the data of this company? Please note that any previous copy will be deleted in the process.');"><i class="zmdi zmdi-copy"></i> Duplicate</button>
										 </div>
								 </div>
							 </form>

						 </div>
				 </div>
			</div>
			<div class="col-sm-6">
				 <div class="card">
						 <div class="card-header">
								 <h2>Delete company<small>This will delete all login accounts and data for this company</small></h2>
						 </div>

						 <div class="card-body card-padding">
								 <form action="manage_accounts_users.php" method="post">
									 <input type="hidden" name="s" value="delete_company">
									 <input type="hidden" name="id_company" value="<?php echo $id_company; ?>">

								 <div class="row">
										 <div class="col-sm-4" align='center'>
											 <button type="submit" class="btn btn-danger btn-block btn-icon-text" onclick="return confirm('Are you sure you want to delete this company and all data added for this company?');"><i class="zmdi zmdi-trash"></i> Delete</button>
										 </div>
								 </div>
							 </form>

						 </div>
				 </div>
			</div>
		</div>
		<? } ?>
                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

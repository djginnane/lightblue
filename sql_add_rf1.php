<?php
/* Includes */
include('mysqli.php');

/* Preferences */
$_SESSION['date_waste'] = $_POST['date_waste'];
$_SESSION['kitchen'] = $_POST['kitchen'];
$_SESSION['shift'] = $_POST['shift'];

/* Variables */
$id_user = mysqli_real_escape_string($db,$_POST['id_user']);
$id_company = mysqli_real_escape_string($db,$_POST['id_company']);

$date_waste = mysqli_real_escape_string($db,$_POST['date_waste']);
$date_waste = date_create_from_format('d/m/Y', $date_waste);
$date_waste = date_format($date_waste, 'Y/m/d');

$kitchen = mysqli_real_escape_string($db,$_POST['kitchen']);
$shift = mysqli_real_escape_string($db,$_POST['shift']);
$type_waste = mysqli_real_escape_string($db,$_POST['type_waste']);

$weight_1 = mysqli_real_escape_string($db,$_POST['weight_1']);
$type_food_1 = mysqli_real_escape_string($db,$_POST['type_food_1']);

$weight_2 = mysqli_real_escape_string($db,$_POST['weight_2']);
$type_food_2 = mysqli_real_escape_string($db,$_POST['type_food_2']);

$weight_3 = mysqli_real_escape_string($db,$_POST['weight_3']);
$type_food_3 = mysqli_real_escape_string($db,$_POST['type_food_3']);

$weight_4 = mysqli_real_escape_string($db,$_POST['weight_4']);
$type_food_4 = mysqli_real_escape_string($db,$_POST['type_food_4']);

$weight_5 = mysqli_real_escape_string($db,$_POST['weight_5']);
$type_food_5 = mysqli_real_escape_string($db,$_POST['type_food_5']);

$weight_6 = mysqli_real_escape_string($db,$_POST['weight_6']);
$type_food_6 = mysqli_real_escape_string($db,$_POST['type_food_6']);

$weight_7 = mysqli_real_escape_string($db,$_POST['weight_7']);
$type_food_7 = mysqli_real_escape_string($db,$_POST['type_food_7']);

$weight_8 = mysqli_real_escape_string($db,$_POST['weight_8']);
$type_food_8 = mysqli_real_escape_string($db,$_POST['type_food_8']);

$weight_9 = mysqli_real_escape_string($db,$_POST['weight_9']);
$type_food_9 = mysqli_real_escape_string($db,$_POST['type_food_9']);

$weight_10 = mysqli_real_escape_string($db,$_POST['weight_10']);
$type_food_10 = mysqli_real_escape_string($db,$_POST['type_food_10']);

$weight_11 = mysqli_real_escape_string($db,$_POST['weight_11']);
$type_food_11 = mysqli_real_escape_string($db,$_POST['type_food_11']);

$weight_12 = mysqli_real_escape_string($db,$_POST['weight_12']);
$type_food_12 = mysqli_real_escape_string($db,$_POST['type_food_12']);

$weight_13 = mysqli_real_escape_string($db,$_POST['weight_13']);
$type_food_13 = mysqli_real_escape_string($db,$_POST['type_food_13']);

$weight_14 = mysqli_real_escape_string($db,$_POST['weight_14']);
$type_food_14 = mysqli_real_escape_string($db,$_POST['type_food_14']);

$weight_15 = mysqli_real_escape_string($db,$_POST['weight_15']);
$type_food_15 = mysqli_real_escape_string($db,$_POST['type_food_15']);


/* Queries */
if($id_user == $_SESSION['username']){

	if($weight_15 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_15', '$type_food_15')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_14 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_14', '$type_food_14')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_13 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_13', '$type_food_13')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_12 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_12', '$type_food_12')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_11 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_11', '$type_food_11')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_10 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_10', '$type_food_10')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_9 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_9', '$type_food_9')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_8 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_8', '$type_food_8')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_7 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_7', '$type_food_7')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_6 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_6', '$type_food_6')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_5 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_5', '$type_food_5')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_4 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_4', '$type_food_4')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_3 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_3', '$type_food_3')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_2 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_2', '$type_food_2')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

	if($weight_1 != ""){
		$sql = "INSERT INTO lbc_rf1 (id_user, id_company, date_waste, kitchen, shift, type_waste, weight, type_food)
		VALUES ('$id_user', '$id_company', '$date_waste', '$kitchen', '$shift', '$type_waste', '$weight_1', '$type_food_1')";
		if(!$result = $db->query($sql)){ die('Error insert record [' . $db->error . ']');}
	}

}

/* Redirection */
echo "<script language=\"JavaScript\"> document.location.replace(\"add_rf1.php?s=added\");</script>";

?>

<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company'];

// Load list of kitchens from this company
	$query_kitchen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type != 'Banquet' ORDER BY kitchen ASC";
	$kitchen = mysql_query($query_kitchen, $db) or die(mysql_error());
	$row_kitchen = mysql_fetch_assoc($kitchen);
	$numberRowsKitchen  = mysql_num_rows($kitchen);


// Load list of RF1

	$date_waste = $_POST['date_waste'];
	$date_waste = date_create_from_format('d/m/Y', $date_waste);
	$date_waste = date_format($date_waste, 'Y/m/d');

	$kitchen_post = mysql_real_escape_string($_POST['kitchen']);
	$shift_post = $_POST['shift'];
	$show_approval_details = $_POST['show_approval_details'];

	if (($date_waste != "") && ($kitchen_post != "") && ($shift_post != "")) {

	// Save last record preferences
	$_SESSION['date_waste'] = $_POST['date_waste'];
	$_SESSION['kitchen'] = $kitchen_post;
	$_SESSION['shift'] = $shift_post;

	$query = "SELECT * FROM lbc_rf1 WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND kitchen = '$kitchen_post' AND shift = '$shift_post' ORDER BY date_added DESC";
	$rf1 = mysql_query($query, $db) or die(mysql_error());
	$row_rf1 = mysql_fetch_assoc($rf1);
	$numberRowsrf1  = mysql_num_rows($rf1);

	$query_rf2 = "SELECT * FROM lbc_rf2 WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND kitchen = '$kitchen_post' AND shift = '$shift_post' AND approval_status = 'yes'";
	$rf2 = mysql_query($query_rf2, $db) or die(mysql_error());
	$row_rf2 = mysql_fetch_assoc($rf2);
	$numberRowsrf2  = mysql_num_rows($rf2);



	}

	$query_rf2_no = "SELECT * FROM lbc_rf2 WHERE id_company = '$id_company' AND approval_status = 'no' ORDER BY date_waste ASC";
	$rf2_no = mysql_query($query_rf2_no, $db) or die(mysql_error());
	$row_rf2_no = mysql_fetch_assoc($rf2_no);
	$numberRowsrf2_no  = mysql_num_rows($rf2_no);

	$query_rf2_yes = "SELECT * FROM lbc_rf2 WHERE id_company = '$id_company' AND approval_status = 'yes' ORDER BY date_waste ASC";
	$rf2_yes = mysql_query($query_rf2_yes, $db) or die(mysql_error());
	$row_rf2_yes = mysql_fetch_assoc($rf2_yes);
	$numberRowsrf2_yes  = mysql_num_rows($rf2_yes);

?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">
         <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>
		 
    </head>
    <body>

       <? include 'header.php';?>

       <section id="main">

           <? include 'sidebar.php'; sideBar('wasterecords','rf2'); ?>

            <section id="content">
                <div class="container">

<? if($_GET['s'] == "added") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Successfully approved.
</div>
<? } ?>

<? if($_GET['s'] == "updated") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Updated successfully.
</div>
<? } ?>

<? if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Deleted successfully.
</div>
<? } ?>
                     <div class="block-header">
                        <h2>RF2 - Champions</h2>
                        <? //if($_POST['date_waste']) { ?>
                        <!--<ul class="actions">
                            <li>
                                <a href="add_rf2.php">
                                    <i class="zmdi zmdi-search"></i>
                                </a>
                            </li>
                  		</ul>-->
                        <? // } ?>

                    </div>

<form id="rf2" name="rf2" method="post" action="add_rf2.php">
                   <div class="card">

                        <div class="card-body card-padding">

                            <div class="row">
                                <div class="col-sm-4">

                                  <div class="input-group form-group" >
                                  <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                  <div class="dtp-container fg-line">
     							  <input type='text' class="form-control date-picker" placeholder="Date" id="date" name="date_waste" value="<? if($_SESSION['date_waste'] == "") { echo date('d/m/Y');} else {echo $_SESSION['date_waste'];}?>">
                                  </div>
                                  </div>

                                </div>

                            <div class="col-sm-4">


<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-cutlery"></i></span>
<div class="fg-line select">
<select class="form-control" name="kitchen">
<? if($_SESSION['kitchen'] == "") {?><option>Select recording station / kitchen</option><? } ?>
<? if($numberRowsKitchen != "0") { $n=1; do { ?>
<option <? if($_SESSION['kitchen'] == $row_kitchen['kitchen']) {echo "selected";} ?> value="<? echo $row_kitchen['kitchen']; ?>"><? echo $row_kitchen['kitchen']; ?></option>
<? $n++; } while ($row_kitchen = mysql_fetch_assoc($kitchen)); }?>
</select>
</div>
</div>

                            </div>

                            <div class="col-sm-4">

<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
<div class="fg-line select">
<select class="form-control" name="shift">
<? if($_SESSION['shift'] == "") {?><option >Select shift</option> <? } ?>
<option <? if($_SESSION['shift'] == "Breakfast") {echo "selected";} ?> value="Breakfast">Breakfast</option>
<option <? if($_SESSION['shift'] == "Lunch") {echo "selected";} ?> value="Lunch">Lunch</option>
<option <? if($_SESSION['shift'] == "Dinner") {echo "selected";} ?> value="Dinner">Dinner</option>
<option <? if($_SESSION['shift'] == "Night shift") {echo "selected";} ?> value="Night shift">Night shift</option>
</select>
</div>
</div>

                            </div>

<div align="center" id="update_button">
<button type="submit" class="btn btn-default btn-icon-text bgm-lightblue waves-effect">
<i class="zmdi zmdi-refresh"></i>Load
</button>
</div>
  </form>
                            </div>
                        </div>
					</div>
                <? if(!$_POST['date_waste']) { ?>

                 <? if ($numberRowsrf2_no > 0) { // List of not approved?>
                    <div class="card">

                        <div class="card-header">
                            <h2>Waiting for approval <small>List of shifts</small></h2>
                        </div>

                        <div class="table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center">Date of waste</th>
                                        <th align="center">Recording point</th>
                                        <th align="center">Shift</th>
                                        <th align="center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <? $n=1; do { ?>
                    <tr>
                    <td><? echo date("d/m/y", strtotime($row_rf2_no['date_waste'])); ?></td>
                    <td><? echo $row_rf2_no['kitchen']; ?></td>
                    <td><? echo $row_rf2_no['shift']; ?></td>
                    <td>

					<form id="rf2" name="rf2" method="post" action="add_rf2.php">
                    <input type="hidden" name="date_waste" value="<? echo date("d/m/Y", strtotime($row_rf2_no['date_waste'])); ?>" />
                    <input type="hidden" name="kitchen" value="<? echo $row_rf2_no['kitchen']; ?>" />
                    <input type="hidden" name="shift" value="<? echo $row_rf2_no['shift']; ?>" />
                    <input type="hidden" name="show_approval_details" value="true"/>

                    <button type="submit" class="btn btn-primary btn-xs bgm-lightblue waves-effect">Show records</button>
                    </form>

                    </td>
                    </tr>
                                <? $n++; } while ($row_rf2_no = mysql_fetch_assoc($rf2_no));?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                  <? } ?>

                <? //if (($numberRowsrf2_no == 0) && ($numberRowsrf2_yes > 0)) { // List of approved ?>
                <? if ($numberRowsrf2_yes > 0) { // List of approved ?>
                <div class="card">

                        <div class="card-header">
                            <h2>Approved shifts<small>List of shifts</small></h2>
                        </div>

                        <div class="table-responsive">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center">Date of waste</th>
                                        <th align="center">Recording point</th>
                                        <th align="center">Shift</th>
                                        <th align="center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <? $n=1; do { ?>
                    <tr>
                    <td><? echo date("d/m/y", strtotime($row_rf2_yes['date_waste'])); ?></td>
                    <td><? echo $row_rf2_yes['kitchen']; ?></td>
                    <td><? echo $row_rf2_yes['shift']; ?></td>

                    <td>

					<form id="rf2" name="rf2" method="post" action="add_rf2.php">
                    <input type="hidden" name="date_waste" value="<? echo date("d/m/Y", strtotime($row_rf2_yes['date_waste'])); ?>" />
                    <input type="hidden" name="kitchen" value="<? echo $row_rf2_yes['kitchen']; ?>" />
                    <input type="hidden" name="shift" value="<? echo $row_rf2_yes['shift']; ?>" />
                    <input type="hidden" name="show_approval_details" value="true"/>

                    <button type="submit" class="btn btn-primary btn-xs bgm-lightblue waves-effect">Show records</button>
                    </form>

                    </td>

                    </tr>
                                <? $n++; } while ($row_rf2_yes = mysql_fetch_assoc($rf2_yes));?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                <? } ?>




<? } else { ?>
                   <div class="card">
                   <? if ($numberRowsrf1 > 0) { ?>
                        <div class="card-header">
                            <h2>Summary of RF1
                            <? if ($numberRowsrf2 > 0) { ?>
                            <small><strong>Approved by <? echo $row_rf2['approval_user']; ?></strong><br>
							<? echo $date_waste; ?> - <? echo $kitchen_post; ?> - <? echo $shift_post; ?></small>
                            <? } else { ?>
                             <small><strong>Waiting for approval</strong><br>
							<? echo $date_waste; ?> - <? echo $kitchen_post; ?> - <? echo $shift_post; ?></small>
                            <? } ?>

                            </h2>
                        </div>
                   <? } else { ?>
                   <br><br>
                   <? } ?>

                        <div class="table-responsive">
                        <? if ($numberRowsrf1 > 0) { ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center"><span style="color:#000;"><strong>Spoilage</strong></span></th>
                                        <th align="center"><span style="color:#FFC107;"><strong>Preparation</strong></span></th>
                                        <th align="center"><span style="color:#3F51B5;"><strong>Buffet</strong></span></th>
                                        <th align="center"><span style="color:grey;"><strong>Non edible</strong></span></th>

                                        <th align="center">Type</th>
                                        <th align="center">Reason</th>
                                        <th align="center">Origin</th>
                                    </tr>
                                </thead>
                                <tbody>

                                <? $n=1; do { ?>
                    <tr>
                    <td align="center"><a href="update_rf1.php?id_record=<? echo $row_rf1['id_waste']; ?>">
					<? if($row_rf1['type_waste'] == "spoilage") { echo $row_rf1['weight']." g";} ?></a></td>

                    <td align="center"><a href="update_rf1.php?id_record=<? echo $row_rf1['id_waste']; ?>">
					<? if($row_rf1['type_waste'] == "preparation") { echo $row_rf1['weight']." g";} ?></a></td>

                    <td align="center"><a href="update_rf1.php?id_record=<? echo $row_rf1['id_waste']; ?>">
					<? if($row_rf1['type_waste'] == "buffet") { echo $row_rf1['weight']." g";} ?></a></td>

					<td align="center"><a href="update_rf1.php?id_record=<? echo $row_rf1['id_waste']; ?>">
					<? if($row_rf1['type_waste'] == "nonedible") { echo $row_rf1['weight']." g";} ?></a></td>

                    <td><? echo $row_rf1['type_food']; ?></td>
                    <!--<td><? echo $row_rf1['reason_waste']; ?></td>
                    <td><? echo $row_rf1['origin_waste']; ?></td>-->
                    </tr>
                                <? $n++; } while ($row_rf1 = mysql_fetch_assoc($rf1));?>
                                </tbody>
                            </table>
                            <? } else { ?>
                     <p class="text" align="center">

					 <? if(($show_approval_details == 'true') && ($numberRowsrf1 == "0")) {

						// DELETE entry in rf1
						$sql = "DELETE FROM lbc_rf2
						WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND kitchen = '$kitchen_post' AND shift = '$shift_post'";
						mysql_query($sql) or die($sql.'<br>'.mysql_error());

						// Display message
						 echo "<strong>The list of records you are trying to access can't be found.</strong>
						 <br>They were previously deleted by the user. We just corrected the approval list for your convenience.";

					 } else {
						 echo "No waste records available";
					 }?>

                     </p>

                     <? } ?>
                        </div>
                    </div>
<? if (($numberRowsrf1 > 0) && ($numberRowsrf2 == 0)) { ?>
					<form id="rf2_approve" name="rf2_approve" method="post" action="sql_add_rf2.php">

                    <input type="hidden" name="date_waste" value="<? echo $_POST['date_waste']; ?>">
                    <input type="hidden" name="kitchen" value="<? echo $kitchen_post; ?>">
                    <input type="hidden" name="shift" value="<? echo $shift_post; ?>">

                    <button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
<i class="zmdi zmdi-check-all"></i>Approve</button>
                    </form>
<? } ?>
 <? } ?>
                </div>
            </section>
        </section>

        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

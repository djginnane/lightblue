<header id="header">
            <ul class="header-inner">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>
            
                <li class="logo hidden-xs">
                    <!--<a href="index.php">Food Excess Monitoring</a>-->
                    <img src="logo.png" width="225" height="45"/>
                </li>
                
                <li class="pull-right">
                <ul class="top-menu">
                    <li id="toggle-width">
                        <div class="toggle-switch">
                            <input id="tw-switch" type="checkbox" hidden="hidden">
                            <label for="tw-switch" class="ts-helper"></label>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="tm-settings" href=""></a>
                        <ul class="dropdown-menu dm-icon pull-right">
                            <li class="hidden-xs">
                                <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Full screen</a>
                            </li>
                            <li>
                                <a href="index.php?s=change_password"><i class="zmdi zmdi-lock"></i> Change password</a>
                            </li>
                            <li>
                                <a href="/signin/logout_sql.php"><i class="zmdi zmdi-cloud-off"></i> Log out</a>
                            </li>
                        </ul>
                    </li>
                  </ul> 
                </li>
                </ul>
</header>
<?php
	/* Includes */
	include('mysqli.php');

	/* Variables */
	$id_user = $_SESSION['username'];
	$id_company = $_SESSION['id_company'];

	/* Query */
	$query = "SELECT * FROM lbc_rf1 WHERE id_company = '$id_company' ORDER BY date_waste DESC, kitchen ASC";
	$rf1 = $db->query($query);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

         <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>

    </head>
    <body>

	<?php include 'header.php';?>

        <section id="main">
          <?php include('sidebar.php'); sideBar('wasterecords','rf1'); ?>

            <section id="content">
                <div class="container">


<?php if($_GET['s'] == "updated") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Updated successfully.
</div>
<?php } ?>

<?php if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Deleted successfully.
</div>
<?php } ?>

                    <div class="card">

               <div class="card-header">
                  <h2>RF1 - Records list</h2>

                  <ul class="actions">
                            <li>
                                <a href="add_rf1.php">
                                    <i class="zmdi zmdi-plus"></i>
                                </a>
                            </li>
                  </ul>
               </div>

							<div class="card-body card-padding">

							<?php if($rf1->num_rows > 0) { ?>
							<div class="table-responsive">

								<table class="table table-striped">
								<thead>
									<tr>
										<th data-column-id="date">Date</th>
										<th data-column-id="kitchen">Kitchen</th>
										<th data-column-id="shift">Shift</th>
										<th data-column-id="category">Category</th>
										<th data-column-id="weight">Weight</th>
										<th data-column-id="food">Food</th>
									</tr>
								</thead>

								<tbody>
									<?php while($row_rf1 = $rf1->fetch_assoc()) { ?>
									<tr>
										<td><a href="update_rf1.php?id_record=<?php echo $row_rf1['id_waste']; ?>"><?php echo date("d/m/y", strtotime($row_rf1['date_waste'])); ?></a></td>
										<td><a href="update_rf1.php?id_record=<?php echo $row_rf1['id_waste']; ?>"><?php echo $row_rf1['kitchen']; ?></a></td>
										<td><a href="update_rf1.php?id_record=<?php echo $row_rf1['id_waste']; ?>"><?php echo $row_rf1['shift']; ?></a></td>
										<td><a href="update_rf1.php?id_record=<?php echo $row_rf1['id_waste']; ?>"><span style="color:<?php if($row_rf1['type_waste'] == "spoilage") {echo "#000";} elseif($row_rf1['type_waste'] == "preparation") {echo "#FFC107";} elseif($row_rf1['type_waste'] == "buffet") {echo "#F44336";} elseif($row_rf1['type_waste'] == "plate") {echo "#2196F3";} else {echo "#000";}?>"><?php echo $row_rf1['type_waste']; ?></span></a></td>
										<td><?php echo $row_rf1['weight']; ?> g</td>
										<td><?php echo $row_rf1['type_food']; ?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>

							</div>
							<?php } else { ?>
								<p align="center">No records added yet.</p>
							<?php } ?>

							</div>
                  </div>


                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
				<script src="scripts/vendors/bootgrid/jquery.bootgrid.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

				<!-- Data Table -->
        <script type="text/javascript">
            $(document).ready(function(){
                //Basic Example
                $("#data-table-basic").bootgrid({
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    },
                });
            });
        </script>

    </body>
  </html>

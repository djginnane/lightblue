<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company'];

// Load list of kitchens from this company
	$query = "SELECT * FROM lbc_rf3 WHERE id_user = '$id_user' AND (type_waste = 'edible' OR type_waste = 'non_edible') ORDER BY date_added DESC";
	$rf3 = mysql_query($query, $db) or die(mysql_error());
	$row_rf3 = mysql_fetch_assoc($rf3);
	$numberRowsrf3  = mysql_num_rows($rf3);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

         <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>

    </head>
    <body>

	<? include 'header.php';?>

        <section id="main">
          <? include 'sidebar.php'; sideBar('wasterecords','rf3Simplified'); ?>

            <section id="content">
                <div class="container">

<? if($_GET['s'] == "updated") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Updated successfully.
</div>
<? } ?>

<? if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Deleted successfully.
</div>
<? } ?>

                    <div class="card">

               <div class="card-header">
                  <h2>RF4 - Records list<small>Added by <? echo $_SESSION['username']; ?></small></h2>

                  <ul class="actions">
                            <li>
                                <a href="add_rf3_simplified.php">
                                    <i class="zmdi zmdi-plus"></i>
                                </a>
                            </li>
                  </ul>
               </div>

                  <div class="card-body card-padding">
                   <? if($numberRowsrf3 != "0") { ?>
                   <div class="card-body table-responsive">

                           <table class="table table-hover">
                             <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Kitchen</th>
                                        <th>Shift</th>
                                        <th>Category</th>
                                        <th>Weight</th>
                                    </tr>
                             </thead>
                                <tbody>

                     <? $n=1; do { ?>
                    <tr>
                    <td><a href="update_rf3_simplified.php?id_record=<? echo $row_rf3['id_waste']; ?>"><? echo date("d/m/y", strtotime($row_rf3['date_waste'])); ?></a>

                    </td>
                    <td><? echo $row_rf3['kitchen']; ?></td>
                    <td><? echo $row_rf3['shift']; ?></td>
                    <td><span style="color:<? if($row_rf3['type_waste'] == "spoilage") {echo "#000";} elseif($row_rf3['type_waste'] == "preparation") {echo "#FFC107";} elseif($row_rf3['type_waste'] == "buffet") {echo "#3F51B5";} elseif($row_rf3['type_waste'] == "plate") {echo "#2196F3";} else {echo "#000";}?>"><? echo $row_rf3['type_waste']; ?></span>
                    </td>
                    <td><? echo $row_rf3['weight']; ?> kg</td>
                    </tr>
                                <? $n++; } while ($row_rf3 = mysql_fetch_assoc($rf3));?>


                                </tbody>
                            </table>

                        </div>
						<? } else { ?>
                        <p align="center">No records added yet.</p>
                          <? } ?>

                  </div>
                  </div>


                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

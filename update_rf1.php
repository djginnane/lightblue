<?php  include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company']; $id_record = $_GET['id_record'];

	$query = "SELECT * FROM lbc_rf1 WHERE id_waste = '$id_record'";
	$rf1 = mysql_query($query, $db) or die(mysql_error());
	$row_rf1 = mysql_fetch_assoc($rf1);
	$numberRowsrf1  = mysql_num_rows($rf1);

	// Load list of kitchens from this company
	$query_kitchen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type != 'Banquet' ORDER BY kitchen ASC";
	$kitchen = mysql_query($query_kitchen, $db) or die(mysql_error());
	$row_kitchen = mysql_fetch_assoc($kitchen);
	$numberRowsKitchen  = mysql_num_rows($kitchen);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Accept only numbers or decimals "." -->
        <script>
		function isNumberKey(evt){
   		var charCode = (evt.which) ? evt.which : event.keyCode
    	if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
        return false;
    	return true;
		}
        </script>

				<!-- Google Analytics -->
				<?php include ('gtag.php'); ?>

    </head>
    <body>

	<?php include 'header.php';?>

        <section id="main">
          <?php include('sidebar.php'); sideBar('wasterecords','rf1'); ?>

            <section id="content">
                <div class="container">

                    <div class="card">

               <div class="card-header">
                  <h2>RF1 - Update record</h2>

                  <ul class="actions">
                            <li>
                                <a onclick="return confirm('Are you sure you want to delete this waste record?');" href="sql_waste_rf1_delete.php?id_waste=<?php echo $row_rf1['id_waste']; ?>">
                                    <i class="zmdi zmdi-delete"></i>
                                </a>
                            </li>
                  </ul>
               </div>
 <form action="sql_waste_rf1_update.php" method="post" id="updaterf1">
 <input type="hidden" name="id_waste" value="<?php echo $row_rf1['id_waste']; ?>">
 <input type="hidden" name="id_company" value="<?php echo $row_rf1['id_company']; ?>">

   <input type="hidden" name="shift" value="<?php echo $row_rf1['shift']; ?>">
    <input type="hidden" name="date_waste" value="<?php echo $row_rf1['date_waste']; ?>">

                  <div class="card-body card-padding">

               <div class="row">
								 <div class="col-sm-3">
									 <p class="c-black f-500">Date</p>
									 <div class="input-group form-group">
										 <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
										 <div class="dtp-container fg-line">
											 <input type='text' class="form-control date-picker" placeholder="Date" id="date" name="date_waste" value="<?php echo date("d/m/Y", strtotime($row_rf1['date_waste']));?>">
										 </div>
									 </div>
								 </div>


	                 <div class="col-sm-3">

                            <p class="c-black f-500">Recording station</p>
								<div class="input-group form-group">
								<span class="input-group-addon"><i class="zmdi zmdi-cutlery"></i></span>
								<div class="fg-line select">
									<select class="form-control" name="kitchen" required>
									<?php if($numberRowsKitchen != "0") { $n=1; do { ?>
									<option <?php if($row_rf1['kitchen'] == $row_kitchen['kitchen']) {echo "selected";} ?> value="<?php echo $row_kitchen['kitchen']; ?>"><?php echo $row_kitchen['kitchen']; ?></option>
									<?php $n++; } while ($row_kitchen = mysql_fetch_assoc($kitchen)); }?>
									</select>
								</div>
								</div>

                    </div>



	               <div class="col-sm-3">
                              <p class="c-black f-500">Shift</p>
								<div class="input-group form-group">
								<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
								<div class="fg-line select">
									<select class="form-control" name="shift" required>
										<option <?php if($row_rf1['shift'] == "Breakfast") {echo "selected";} ?> value="Breakfast">Breakfast</option>
										<option <?php if($row_rf1['shift'] == "Lunch") {echo "selected";} ?> value="Lunch">Lunch</option>
										<option <?php if($row_rf1['shift'] == "Dinner") {echo "selected";} ?> value="Dinner">Dinner</option>
										<option <?php if($row_rf1['shift'] == "Night shift") {echo "selected";} ?> value="Night shift">Night shift</option>
									</select>
								</div>
								</div>

                    </div>

                    <div class="col-sm-3">
                              <p class="c-black f-500">Category</p>
								<div class="input-group form-group">
								<span class="input-group-addon"><i class="zmdi zmdi-delete"></i></span>
								<div class="fg-line select">
									<select class="form-control" name="type_waste" required>
										<option <?php if($row_rf1['type_waste'] == "spoilage") {echo "selected";} ?> value="spoilage">Spoilage</option>
										<option <?php if($row_rf1['type_waste'] == "preparation") {echo "selected";} ?> value="preparation">Preparation</option>
										<option <?php if($row_rf1['type_waste'] == "buffet") {echo "selected";} ?> value="buffet">Buffet</option>
									</select>
								</div>
								</div>

                    </div>
               </div>
               <div class="row">

			   		<div class="col-sm-6">

                        <p class="c-black f-500">Weight (grams)</p>
                        <div class="input-group">
                                <div class="fg-line">
                                    <input type="text" class="form-control" placeholder="eg. 355" name="weight" value="<?php echo $row_rf1['weight']; ?>" onKeyPress="return isNumberKey(event)">
                                </div>
                                <span class="input-group-addon first"><strong>g</strong></span>
                         </div>
              		</div>

					<div class="col-sm-6">

                        <p class="c-black f-500">Type of food</p>
                         <div class="form-group">
							<div class="fg-line select">
							<select class="form-control" name="type_food" id="type_food">

												<?php if(	($row_rf1['type_food'] != "Staple food") &&
														($row_rf1['type_food'] != "Vegetable") &&
														($row_rf1['type_food'] != "Fruit") &&
														($row_rf1['type_food'] != "Meat") &&
														($row_rf1['type_food'] != "Seafood") &&
														($row_rf1['type_food'] != "Dairy") &&
														($row_rf1['type_food'] != "Others") ) {$tp = 1;} ?>


								<?php if($tp == 1){ ?><option value="<?php echo $row_rf1['type_food']; ?>" selected><?php echo $row_rf1['type_food']; ?></option> <?php } ?>

								<option value="Staple food" <?php if($row_rf1['type_food'] == "Staple food") {echo "selected";} ?>>Staple food</option>
								<option value="Vegetable" <?php if($row_rf1['type_food'] == "Vegetable") {echo "selected";} ?>>Vegetable</option>
								<option value="Fruit" <?php if($row_rf1['type_food'] == "Fruit") {echo "selected";} ?>>Fruit</option>
								<option value="Meat" <?php if($row_rf1['type_food'] == "Meat") {echo "selected";} ?>>Meat</option>
								<option value="Seafood" <?php if($row_rf1['type_food'] == "Seafood") {echo "selected";} ?>>Seafood</option>
								<option value="Dairy" <?php if($row_rf1['type_food'] == "Dairy") {echo "selected";} ?>>Dairy</option>
								<option value="Others" <?php if($row_rf1['type_food'] == "Others") {echo "selected";} ?>>Others</option>
							</select>
							</div>
						</div>
					</div>


               </div>


                  </div>
                  </div>

<button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
<i class="zmdi zmdi-check-all"></i>Update</button>
</form>
                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

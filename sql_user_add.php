<? 

	// Session and database parameters
	include 'database.php';

	// Variables
	$id_company = $_SESSION['id_company'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$type = $_POST['type'];
	$name = $_POST['name'];
	$surname = $_POST['surname'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$company = $_POST['company'];
	$department = $_POST['department'];
	
	// User restrictions
	if($type == "admin"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'yes';
		$WRRF3 = 'yes';
		$WRRF4 = 'yes';
		$WRCovers = 'yes';
		$ESSummary = 'yes';
		$ESOutlets = 'yes';
		$ESCanteens = 'yes';
		$ESBanquet = 'yes';
		$ESOndemand = 'yes';
		$SETKitchens = 'yes';
		$SETUsers = 'yes';
		$SETTypeFood = 'yes';
		$SETSimplifiedRF4 = 'no';
		$SETSimplifiedES = 'no';
	}
	
	if($type == "employee"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'no';
		$WRRF3 = 'yes';
		$WRRF4 = 'no';
		$WRCovers = 'no';
		$ESSummary = 'yes';
		$ESOutlets = 'no';
		$ESCanteens = 'no';
		$ESBanquet = 'no';
		$ESOndemand = 'no';
		$SETKitchens = 'no';
		$SETUsers = 'no';
		$SETTypeFood = 'no';
		$SETSimplifiedRF4 = 'no';
		$SETSimplifiedES = 'no';
	}
	
	if($type == "steward"){	
		$WRRF1 = 'no';
		$WRRF2 = 'no';
		$WRRF3 = 'no';
		$WRRF4 = 'yes';
		$WRCovers = 'no';
		$ESSummary = 'yes';
		$ESOutlets = 'no';
		$ESCanteens = 'no';
		$ESBanquet = 'no';
		$ESOndemand = 'no';
		$SETKitchens = 'no';
		$SETUsers = 'no';
		$SETTypeFood = 'no';
		$SETSimplifiedRF4 = 'no';
		$SETSimplifiedES = 'no';
	}
	
	if($type == "champion"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'yes';
		$WRRF3 = 'yes';
		$WRRF4 = 'no';
		$WRCovers = 'no';
		$ESSummary = 'no';
		$ESOutlets = 'no';
		$ESCanteens = 'no';
		$ESBanquet = 'no';
		$ESOndemand = 'no';
		$SETKitchens = 'no';
		$SETUsers = 'no';
		$SETTypeFood = 'no';
		$SETSimplifiedRF4 = 'no';
		$SETSimplifiedES = 'no';
	}
	
	if($type == "secretarygeneral"){	
		$WRRF1 = 'yes';
		$WRRF2 = 'yes';
		$WRRF3 = 'yes';
		$WRRF4 = 'yes';
		$WRCovers = 'yes';
		$ESSummary = 'yes';
		$ESOutlets = 'yes';
		$ESCanteens = 'yes';
		$ESBanquet = 'yes';
		$ESOndemand = 'yes';
		$SETKitchens = 'yes';
		$SETUsers = 'yes';
		$SETTypeFood = 'yes';
		$SETSimplifiedRF4 = 'no';
		$SETSimplifiedES = 'no';
	}
	
	// Save in database
	if(($username != "")&&($password != "")){
		
	$sql = "INSERT INTO lbc_users(id_company, username, password, type, name, surname, company, department, email, phone, WRRF1, WRRF2, WRRF3, WRRF4, WRCovers, ESSummary, ESOutlets, ESCanteens, ESBanquet, ESOndemand, SETKitchens, SETUsers, SETTypeFood, SETSimplifiedRF4, SETSimplifiedES) VALUES('$id_company', '$username', '$password', '$type', '$name', '$surname', '$company', '$department', '$email', '$phone', '$WRRF1', '$WRRF2', '$WRRF3', '$WRRF4', '$WRCovers', '$ESSummary', '$ESOutlets', '$ESCanteens', '$ESBanquet', '$ESOndemand', '$SETKitchens', '$SETUsers', '$SETTypeFood', '$SETSimplifiedRF4', '$SETSimplifiedES')"; 
	mysql_query($sql) or die($sql.'<br>'.mysql_error()); header('Location: manage_users.php?s=added');
	
	}
	
	else echo "<br><br><br><p align='center' style='font-family:arial; font-size: 14px;'><strong>Error creating the new user.</strong><br>Please go back to the previous page, make sure that username and password are entered and try again.</p>";

?>
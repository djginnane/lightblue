<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company'];

// Load list of kitchens from this company
	$query = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
	$kitchen = mysql_query($query, $db) or die(mysql_error());
	$row_kitchen = mysql_fetch_assoc($kitchen);
	$numberRowsKitchen  = mysql_num_rows($kitchen);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Accept only numbers or decimals "." -->
        <script>
		function isNumberKey(evt){
   		var charCode = (evt.which) ? evt.which : event.keyCode
    	if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
        return false;
    	return true;
		}
        </script>

        <!-- Form validation for empty fields -->
        <script>
	function validateForm() {
    	var kitchen = document.forms["add_waste"]["kitchen"].value;
		var shiftv = document.forms["add_waste"]["shift"].value;
		var weight_1 = document.forms["add_waste"]["weight_1"].value;

		if((document.getElementById("spoilage").checked == true) || (document.getElementById("preparation").checked == true) || (document.getElementById("plate").checked == true) || (document.getElementById("buffet").checked == true) || (document.getElementById("nonedible").checked == true)) {

			if (kitchen == null || kitchen == "" || shiftv == null || shiftv == "" || weight_1 == null || weight_1 == "") {
			alert("All required fields must be filled-in.");
			return false;}

		}

		else { alert("Please select a food waste category."); return false;}
	}
		</script>
          <!-- Hide notifications after 5 seconds -->
         <script>
         setTimeout(function() {
		 	$('#hide').fadeOut('fast');
		}, 5000);
		</script>
		<!-- Hide notifications after 5 seconds -->

		<!-- Google Analytics -->
		<?php include ('gtag.php'); ?>
		    
    </head>
    <body>

       <? include 'header.php';?>

       <section id="main">

           <? include 'sidebar.php'; sideBar('wasterecords','rf3'); ?>

            <section id="content">
                <div class="container">

 <? if($_GET['s'] == "added") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Waste(s) have been recorded successfully.
</div>
<? } ?>

<? if($_GET['s'] == "updated") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Waste(s) have been updated successfully.
</div>
<? } ?>

<? if($_GET['s'] == "deleted") { ?>
<div class="alert alert-success alert-dismissible" role="alert" id="hide">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
Waste(s) have been deleted successfully.
</div>
<? } ?>
                                          <div class="block-header">
                        <h2>RF4 - Stewards</h2>

                        <ul class="actions">
                        <li class="dropdown action-show">
                                    <a href="" data-toggle="dropdown">
                                        <i class="zmdi zmdi-info" ></i>
                                    </a>

                                    <div class="dropdown-menu pull-right" style="width:320px;">
                                        <p class="p-20">
<strong>Spoilage</strong>
<br><br>
Anything from the kitchen + STORAGE that has gone off (out of date) or has been contaminated and is unusable, including front-of house items e.g. melted butter or mouldy bread.
<br><br>
<strong>Prep waste</strong>
<br><br>
Anything that could be used but is thrown out. This includes meals cooked for customers that don’t get served (e.g. the food is overcooked and not suitable to serve, or over-preparation).
<br><br>
<strong>Buffet waste</strong>
<br><br>
Leftover food from the buffet line that cannot be safely stored and reused.
<br><br>
<strong>Non edible waste</strong>
<br><br>
Fruit and vegetables peelings, poultry and fish skin, bones, etc. Dispose of in a separate bin, as these category of waste should not be mixed with Food waste. It is useful to record this information for future plans to dispose of food waste via composting and anaerobic digestion.

                                        </p>
                                    </div>
                                </li>
                           <li>
                                <a href="manage_rf3.php">
                                    <i class="zmdi zmdi-view-list"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

<form action="sql_add_rf3.php" method="POST" id="add_waste" onsubmit="return validateForm()">

<!-- Hidden inputs -->
<input type="hidden" name="id_user" value="<? echo $_SESSION['username']; ?>">
<input type="hidden" name="id_company" value="<? echo $_SESSION['id_company']; ?>">
<!-- Hidden inputs -->

                  <div class="card">

                        <div class="card-body card-padding">

                            <div class="row">
                                <div class="col-sm-4">

                                  <div class="input-group form-group" >
                                  <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                  <div class="dtp-container fg-line">
     							  <input type='text' class="form-control date-picker" placeholder="Date" id="date" name="date_waste" value="<? if($_SESSION['date_waste'] == "") { echo date('d/m/Y');} else {echo $_SESSION['date_waste'];}?>">
                                  </div>
                                  </div>

                                </div>

                            <div class="col-sm-4">


<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-cutlery"></i></span>
<div class="fg-line select">
<select class="form-control" name="kitchen">
<? if($_SESSION['kitchen'] == "") {?><option value="">Select recording station / kitchen</option><? } ?>
<? if($numberRowsKitchen != "0") { $n=1; do { ?>
<option <? if($_SESSION['kitchen'] == $row_kitchen['kitchen']) {echo "selected";} ?> value="<? echo $row_kitchen['kitchen']; ?>"><? echo $row_kitchen['kitchen']; ?></option>
<? $n++; } while ($row_kitchen = mysql_fetch_assoc($kitchen)); }?>
</select>
</div>
</div>

                            </div>

                            <div class="col-sm-4">

<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
<div class="fg-line select">
<select class="form-control" name="shift">
<? if($_SESSION['shift'] == "") {?><option value="">Select shift</option> <? } ?>
<option <? if($_SESSION['shift'] == "Breakfast") {echo "selected";} ?> value="Breakfast">Breakfast</option>
<option <? if($_SESSION['shift'] == "Lunch") {echo "selected";} ?> value="Lunch">Lunch</option>
<option <? if($_SESSION['shift'] == "Dinner") {echo "selected";} ?> value="Dinner">Dinner</option>
<option <? if($_SESSION['shift'] == "Night shift") {echo "selected";} ?> value="Night shift">Night shift</option>
</select>
</div>
</div>

                            </div>
                            </div>
                        </div>
					</div>

                     <div class="card">

                     <div class="card-header">


                      </div>

                       <div class="card-body card-padding">
                        <div class="row">

                        <div class="col-sm-4 m-b-5">

                       		<label class="radio radio-inline m-r-20">
                                <input type="radio" name="type_waste" id="spoilage" value="spoilage">
                               <i class="input-helper"></i>
                               <div style="margin-top:-10px;">
                                <i class="zmdi zmdi-delete" style="color:#000; font-size:20px;"></i>
                                <strong><span style="color:#000; font-size:20px;">   SPOILAGE</span></strong>
                                </div>
                            </label>
                       	</div>

                        <div class="col-sm-4 m-b-5">
                            <label class="radio radio-inline m-r-20">
                               <input type="radio" name="type_waste" id="preparation" value="preparation">
                                <i class="input-helper"></i>
                                <div style="margin-top:-10px;">
                                <i class="zmdi zmdi-delete" style="color:#FFC107;  font-size:20px;"></i>
                                <strong><span style="color:#FFC107;  font-size:20px;">   PREPARATION</span></strong>
                               </div>


                            </label>
                       	</div>

                        <div class="col-sm-4 m-b-5">
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="type_waste" id="plate" value="plate">
                                <i class="input-helper"></i>
                                <div style="margin-top:-10px;">
                               <i class="zmdi zmdi-delete" style="color:#2196F3; font-size:20px;"></i>
                               <strong><span style="color:#2196F3; font-size:20px;">   PLATE</span></strong>
                               </div>
                            </label>
                       	</div>
                      </div> <br>
                      <div class="row">

                        <div class="col-sm-4 m-b-5">
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="type_waste" id="buffet" value="buffet">
                                <i class="input-helper bgm-black"></i>
                                <div style="margin-top:-10px;">
                                <i class="zmdi zmdi-delete" style="color:#3F51B5; font-size:20px;"></i>
                                <strong><span style="color:#3F51B5; font-size:20px;">   BUFFET</span></strong>
                                </div>
                            </label>
                       	</div>

                       	<div class="col-sm-4 m-b-5">
                            <label class="radio radio-inline m-r-20">
                                <input type="radio" name="type_waste" id="nonedible" value="nonedible">
                                <i class="input-helper bgm-black"></i>
                                <div style="margin-top:-10px;">
                                <i class="zmdi zmdi-delete" style="color:grey; font-size:20px;"></i>
                                <strong><span style="color:grey; font-size:20px;">   NON EDIBLE</span></strong>
                                </div>
                            </label>
                       	</div>

                        </div>
                        <br><br><br>

      <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <p class="c-black f-500">Weight (Kg)</p>

                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_1" onKeyPress="return isNumberKey(event)"  autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>

      <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_2" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_3" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_4" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_5" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_6" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_7" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_8" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_9" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_10" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_11" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_12" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_13" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_14" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

     <div class="row">
          <div class="col-sm-3"></div>
          <div class="col-sm-6">
                            <div class="input-group">
                            <div class="fg-line">
                            <input type="text" class="form-control" placeholder="eg. 2.8" name="weight_15" onKeyPress="return isNumberKey(event)" autocomplete="off">
                            </div>

                            <span class="input-group-addon last"><strong>Kg</strong></span>

                            </div>
        </div>
        <div class="col-sm-3"></div>
     </div>

        </div>
					</div>

                    <button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text">
<i class="zmdi zmdi-cloud-upload"></i>Record total waste</button>
                    </form>

                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>


        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

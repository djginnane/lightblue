<?
include 'signin/session.php';

function listOfDays($id_company, $table) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		$sql = "SELECT * FROM $table WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			echo "'" . date("D d M", strtotime($row['date_waste'])) . "'";
			if($i < $numRows) {echo ",";}	 $i++;
		}

		// Free result
		$result->free();

}

function listOfKitchens($id_company) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		$sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND type = 'Outlet' ORDER BY kitchen ASC";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			echo "'" . $row['kitchen'] . "'";
			if($i < $numRows) {echo ",";}	 $i++;
		}

		// Free result
		$result->free();

}

function dailyWasteKitchen($table, $id_company, $date_waste, $kitchen) {



		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL") {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND kitchen = '$kitchen'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND kitchen = '$kitchen'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

		if ($i == 1) {$total = $row['weight'];}
		if ($i > 1) {$total = $total + $row['weight'];}
		$i++;

		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}

function dailyWasteKitchenCover($table, $id_company, $date_waste, $kitchen) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific day, specific kitchen)
		$sql = "SELECT SUM(weight) FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND kitchen = '$kitchen'";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];

		// Calculate total covers (specific day, specific kitchen)
		$covers = "SELECT SUM(covers) FROM lbc_covers WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND kitchen = '$kitchen'";
		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];

		// Calculate total
		$ratio = ($total_weight / $total_cov)*1000;
		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,",",".");} else {echo '0';};

}

function dailyWasteKitchenCoverAllDays($table, $id_company, $kitchen) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific kitchen)
		$sql = "SELECT SUM(weight) FROM $table WHERE id_company = '$id_company' AND kitchen = '$kitchen'";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];

		// Calculate total covers (specific kitchen)
		$covers = "SELECT SUM(covers) FROM lbc_covers WHERE id_company = '$id_company' AND kitchen = '$kitchen'";
		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];

		// Calculate total
		$ratio = ($total_weight / $total_cov)*1000;
		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,",",".");} else {echo '0';}

}

function dailyWasteShift($table, $id_company, $date_waste, $shift) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL") {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND shift = '$shift'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND shift = '$shift'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

				// Load list of kitchens that are canteens
				$kitchen_name =  $row['kitchen'];
				$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
				if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
				$row_canteen = mysqli_fetch_assoc($result_canteen);
				$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Outlet") {
			if ($i == 1) {$total = $row['weight'];}
			if ($i > 1) {$total = $total + $row['weight'];}
		}
			$i++;
		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}

function dailyWasteShiftCover($table, $id_company, $date_waste, $shift, $type) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific day)
		$sql = "SELECT SUM(weight)
		FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_rf3.date_waste = '$date_waste' AND lbc_rf3.shift = '$shift' AND lbc_kitchens.type = '$type'";

		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];
		//echo $total_weight;echo '<br>';

		// Calculate total covers (specific day)
		$covers = "SELECT SUM(covers)
		FROM lbc_covers RIGHT JOIN lbc_kitchens ON lbc_covers.kitchen = lbc_kitchens.kitchen
		WHERE lbc_covers.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_covers.date_waste = '$date_waste' AND lbc_covers.shift = '$shift' AND lbc_kitchens.type = '$type'";
		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];
		//echo $total_cov;echo '<br>';

		// Calculate total
		$ratio = ($total_weight / $total_cov)*1000;
		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,",",".");}	else {echo '0';}

}

function dailyWasteShiftCoverAllDays($table, $id_company, $shift, $type) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific day)
		$sql = "SELECT SUM(weight)
		FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_rf3.shift = '$shift' AND lbc_kitchens.type = '$type'";

		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];
		//echo $total_weight;echo '<br>';

		// Calculate total covers (specific day)
		$covers = "SELECT SUM(covers)
		FROM lbc_covers RIGHT JOIN lbc_kitchens ON lbc_covers.kitchen = lbc_kitchens.kitchen
		WHERE lbc_covers.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_covers.shift = '$shift' AND lbc_kitchens.type = '$type'";
		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];
		//echo $total_cov;echo '<br>';

		// Calculate total
		$ratio = ($total_weight / $total_cov)*1000;
		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,",",".");}	else {echo '0';}

}

function dailyWasteCategory($table, $id_company, $date_waste, $category) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL") {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND type_waste= '$category'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND type_waste= '$category'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

				// Load list of kitchens that are canteens
				$kitchen_name =  $row['kitchen'];
				$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
				if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
				$row_canteen = mysqli_fetch_assoc($result_canteen);
				$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Outlet") {

			if ($i == 1) {$total = $row['weight'];}
			if ($i > 1) {$total = $total + $row['weight'];}
		}
			$i++;
		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}

function dailyWasteTotal($table, $id_company, $date_waste) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL") {$sql = "SELECT * FROM $table WHERE id_company = '$id_company'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			// Load list of kitchens that are canteens
			$kitchen_name =  $row['kitchen'];
			$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
			if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
			$row_canteen = mysqli_fetch_assoc($result_canteen);
			$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Outlet") {
			if ($i == 1) {$total = $row['weight'];}
			if ($i > 1) {$total = $total + $row['weight'];}
		}
		$i++;

		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}

function dailyWasteTotalCover($table, $id_company, $date_waste, $type) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific day)
		$sql = "SELECT SUM(weight)
		FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_rf3.date_waste = '$date_waste' AND lbc_kitchens.type = '$type'";

		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];
		//echo $total_weight;echo '<br>';

		// Calculate total covers (specific day)
		$covers = "SELECT SUM(covers)
		FROM lbc_covers RIGHT JOIN lbc_kitchens ON lbc_covers.kitchen = lbc_kitchens.kitchen
		WHERE lbc_covers.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_covers.date_waste = '$date_waste' AND lbc_kitchens.type = '$type'";
		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];
		//echo $total_cov;echo '<br>';

		// Calculate total
		$ratio = ($total_weight / $total_cov)*1000;
		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,",",".");} else {echo '0';};


}

function dailyWasteTotalCoverAllDays($table, $id_company, $type) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific day)
		$sql = "SELECT SUM(weight)
		FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_kitchens.type = '$type'";

		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];
		//echo $total_weight;echo '<br>';

		// Calculate total covers (specific day)
		$covers = "SELECT SUM(covers)
		FROM lbc_covers RIGHT JOIN lbc_kitchens ON lbc_covers.kitchen = lbc_kitchens.kitchen
		WHERE lbc_covers.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_kitchens.type = '$type'";
		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];
		//echo $total_cov;echo '<br>';

		// Calculate total
		$ratio = ($total_weight / $total_cov)*1000;
		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,",",".");}  else {echo '0';};


}

function dailyWasteTotalCanteen($table, $id_company, $date_waste) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL"){$sql = "SELECT * FROM $table WHERE id_company = '$id_company'" ;}
		else{$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			// Load list of kitchens that are canteens
			$kitchen_name =  $row['kitchen'];
			$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
			if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
			$row_canteen = mysqli_fetch_assoc($result_canteen);
			$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Canteen") {
		if ($i == 1) {$total = $row['weight'];}
		if ($i > 1) {$total = $total + $row['weight'];}
		}

		$i++;

		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}



function dailyWasteShiftCanteen($table, $id_company, $date_waste, $shift) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL"){$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND shift = '$shift'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND shift = '$shift'" ;}

		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			// Load list of kitchens that are canteens
			$kitchen_name =  $row['kitchen'];
			$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
			if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
			$row_canteen = mysqli_fetch_assoc($result_canteen);
			$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Canteen") {

			if ($i == 1) {$total = $row['weight'];}
			if ($i > 1) {$total = $total + $row['weight'];}
			}

			$i++;
		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}


function dailyWasteCategoryCanteen($table, $id_company, $date_waste, $category) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL"){$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND type_waste= '$category'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND type_waste= '$category'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			// Load list of kitchens that are canteens
			$kitchen_name =  $row['kitchen'];
			$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
			if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
			$row_canteen = mysqli_fetch_assoc($result_canteen);
			$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Canteen") {

			if ($i == 1) {$total = $row['weight'];}
			if ($i > 1) {$total = $total + $row['weight'];}
		}

			$i++;
		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}

function dailyWasteTotalBanquet($table, $id_company, $date_waste) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL"){$sql = "SELECT * FROM $table WHERE id_company = '$id_company'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			// Load list of kitchens that are canteens
			$kitchen_name =  $row['kitchen'];
			$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
			if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
			$row_canteen = mysqli_fetch_assoc($result_canteen);
			$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Banquet") {
		if ($i == 1) {$total = $row['weight'];}
		if ($i > 1) {$total = $total + $row['weight'];}
		}

		$i++;

		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();


}



function dailyWasteShiftBanquet($table, $id_company, $date_waste, $shift) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL"){$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND shift = '$shift'" ;}
		else {$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND shift = '$shift'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			// Load list of kitchens that are canteens
			$kitchen_name =  $row['kitchen'];
			$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
			if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
			$row_canteen = mysqli_fetch_assoc($result_canteen);
			$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Banquet") {

			if ($i == 1) {$total = $row['weight'];}
			if ($i > 1) {$total = $total + $row['weight'];}
			}

			$i++;
		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}


function dailyWasteCategoryBanquet($table, $id_company, $date_waste, $category) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		if($date_waste == "NULL"){$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND type_waste= '$category'" ;}
		else{$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND date_waste = '$date_waste' AND type_waste= '$category'" ;}
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

			// Load list of kitchens that are canteens
			$kitchen_name =  $row['kitchen'];
			$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name' ";
			if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
			$row_canteen = mysqli_fetch_assoc($result_canteen);
			$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == "Banquet") {

			if ($i == 1) {$total = $row['weight'];}
			if ($i > 1) {$total = $total + $row['weight'];}
		}

			$i++;
		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}


function alltimeWasteKitchen($table, $id_company, $kitchen) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND kitchen = '$kitchen'";
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;


		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

		if ($i == 1) {$total = $row['weight'];}
		if ($i > 1) {$total = $total + $row['weight'];}
		$i++;

		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2);}

		// Free result
		$result->free();

}

function alltimeWasteKitchenCategory($table, $id_company, $kitchen, $category) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Load data
		$sql = "SELECT * FROM $table WHERE id_company = '$id_company' AND kitchen = '$kitchen' AND type_waste = '$category'" ;
		if(!$result = $db->query($sql)){ die('There was an error running the query [' . $db->error . ']');}

		// SQL optional variables
		$numRows = $result->num_rows;
		$affectedRows = $db->affected_rows;

		// Display result
		$i = 1;

		while($row = $result->fetch_assoc()){

		if ($i == 1) {$total = $row['weight'];}
		if ($i > 1) {$total = $total + $row['weight'];}
		$i++;

		}

		if ($total == 0) {echo "0";} else {echo number_format($total,0,".","");}

		// Free result
		$result->free();

}

function totalReportCategory($table, $id_company, $category) {

		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Content
		$sql_category = "SELECT SUM(weight) FROM $table WHERE id_company = '$id_company' AND type_waste = '$category'";
		if(!$result_category = $db->query($sql_category)){ die('There was an error running the query [' . $db->error . ']');}

		$weight = mysqli_fetch_array($result_category); echo number_format($weight[0], 2,'.','');

		// Free result
		$result_category->free();

		mysqli_close($db);
	}

function totalReportRecordingPoint($table, $id_company, $kitchen) {

		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Content
		$sql_category = "SELECT SUM(weight) FROM $table WHERE id_company = '$id_company' AND kitchen = '$kitchen'";
		if(!$result_category = $db->query($sql_category)){ die('There was an error running the query [' . $db->error . ']');}

		$weight = mysqli_fetch_array($result_category); echo number_format($weight[0], 2, ".","");

		// Free result
		$result_category->free();

		mysqli_close($db);

}

function totalReportCategoryType($table, $id_company, $category, $type) {

		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Content
		$sql_category = "SELECT * FROM $table WHERE id_company = '$id_company' AND type_waste = '$category'";
		if(!$result_category = $db->query($sql_category)){ die('There was an error running the query [' . $db->error . ']');}

		$row_category = mysqli_fetch_assoc($result_category);
		$numRows_category = $result_category->num_rows;

		// Display result
		$i = 1;

		while($row_category = $result_category->fetch_assoc()){

				// Load list of kitchens that are canteens
				$kitchen_name =  $row_category['kitchen'];
				$sql_canteen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' AND kitchen = '$kitchen_name'";
				if(!$result_canteen = $db->query($sql_canteen)){ die('There was an error running the query [' . $db->error . ']');}
				$row_canteen = mysqli_fetch_assoc($result_canteen);
				$numRows_canteen = $result_canteen->num_rows;

		if($row_canteen['type'] == $type) {

			if ($i == 1) {$total = $row_category['weight'];}
			if ($i > 1) {$total = $total + $row_category['weight'];}
		}

		$i++;

		}

		if ($total == 0) {echo "0";} else {echo number_format($total,2,'.','');}

		// Free result
		$result_category->free();

		mysqli_close($db);

	}

function preconsumerFoodWaste($id_company) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific day)
		$sql = "SELECT SUM(weight) FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_kitchens.type != 'Canteen'
		AND (lbc_rf3.type_waste = 'spoilage' OR lbc_rf3.type_waste = 'preparation' OR lbc_rf3.type_waste = 'non_edible')";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];
		//echo $total_weight;echo '<br>';

		// Calculate total weight (specific day)
		$sql_total = "SELECT SUM(weight) FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_kitchens.type != 'Canteen'";
		$result_total = $db->query($sql_total); $weight_total = mysqli_fetch_array($result_total); $total_weight_total = $weight_total[0];
		//echo $total_weight_total;echo '<br>';

		// Calculate total
		$ratio = ($total_weight / $total_weight_total) * 100;
		if(($total_weight !== 0) && ($total_weight_total !== 0)) {echo number_format($ratio,2).' %';}

}

function allWasteCover($table, $id_company, $type) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight (specific day)
		$sql = "SELECT SUM(weight)
		FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		AND lbc_kitchens.type != '$type'";

		$result = $db->query($sql); $weight = mysqli_fetch_array($result); $total_weight = $weight[0];
		//echo $total_weight;echo '<br>';

		// Calculate total covers (specific day)
		$covers = "SELECT SUM(covers)
		FROM lbc_covers RIGHT JOIN lbc_kitchens ON lbc_covers.kitchen = lbc_kitchens.kitchen
		WHERE lbc_covers.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company'
		 AND lbc_kitchens.type != '$type'";
		$result_covers = $db->query($covers); $cov = mysqli_fetch_array($result_covers); $total_cov = $cov[0];
		//echo $total_cov;echo '<br>';

		// Calculate total
		$ratio = ($total_weight / $total_cov)*1000;
		if(($total_weight != 0) && ($total_cov != 0)) {echo number_format($ratio,0,",",".");}


}

function ExecutiveSummaryCO2Equivalent($id_company) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight
		$sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company'";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result);

		$co2 = $weight[0] * 3.8;

		echo number_format($co2,0,".",",");

}

function ExecutiveSummaryFamilyFedEquivalent($id_company) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight
		$sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company'";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result);

		$familyfed = $weight[0] * 2.2;

		echo number_format($familyfed,0,".",",");

}

function ExecutiveSummaryFamilyFedEquivalentBaseline($id_company) {

	// (baseline food waste kg per cover) – (actual food waste kg per cover) * number of covers since the end of the  period) * 3 (+ quantity of food redistributed kg * 3 - IF recorded)
		$baseline_start = $_SESSION['baseline_start'];
		$baseline_end = $_SESSION['baseline_end'];

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

				// Baseline total food waste
				$sql_b_waste = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company' AND date_waste BETWEEN '$baseline_start' AND '$baseline_end'";
				$result_b_waste = $db->query($sql_b_waste); $weight_b_waste = mysqli_fetch_array($result_b_waste);
				$baseline_foodwaste = $weight_b_waste[0];

				// Baseline total covers
				$sql_b_covers = "SELECT SUM(covers) FROM lbc_covers WHERE id_company = '$id_company' AND date_waste BETWEEN '$baseline_start' AND '$baseline_end'";
				$result_b_covers = $db->query($sql_b_covers); $b_covers = mysqli_fetch_array($result_b_covers);
				$baseline_covers = $b_covers[0];

		$baseline_foodwaste_cover = $baseline_foodwaste / $baseline_covers;

				// Total food waste
				$sql_waste = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company' AND date_waste BETWEEN '$baseline_start' AND '$baseline_end'";
				$result_waste = $db->query($sql_waste); $weight_waste = mysqli_fetch_array($result_waste);
				$total_foodwaste = $weight_waste[0];

				// Total covers
				$sql_covers = "SELECT SUM(covers) FROM lbc_covers WHERE id_company = '$id_company' AND date_waste BETWEEN '$baseline_start' AND '$baseline_end'";
				$result_covers = $db->query($sql_covers); $covers = mysqli_fetch_array($result_covers);
				$total_covers = $covers[0];

		$foodwaste_covers = $total_foodwaste / $total_covers;

				// Number of covers since the end of the period
				$sql_covers_period = "SELECT SUM(covers) FROM lbc_covers WHERE id_company = '$id_company' AND date_waste > '$baseline_end'";
				$result_covers_period = $db->query($sql_covers_period); $covers_period = mysqli_fetch_array($result_covers_period);
		$total_covers_period = $covers_period[0];

		$total_display = $baseline_foodwaste_cover - $foodwaste_covers * $total_covers_period * 3;

		echo number_format($total_display,0,".",",");

}

function ExecutiveSummaryCashEquivalent($id_company) {

		// Open Mysql connection
		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Calculate total weight
		$sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company'";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result);

		if($_SESSION['cost_kilo'] != ""){$cash = $weight[0] * $_SESSION['cost_kilo'];}
		else {$cash = $weight[0] * 4.5;}

		echo number_format($cash,0,".",",");

}

function ExecutiveSummaryTotalWasteCategory($id_company, $category) {

		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Content
		$sql_category = "SELECT SUM(weight) FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company' AND lbc_rf3.type_waste = '$category' AND lbc_kitchens.type != 'Canteen'";

		if(!$result_category = $db->query($sql_category)){ die('There was an error running the query [' . $db->error . ']');}

		$weight = mysqli_fetch_array($result_category); echo number_format($weight[0], 1,'.','');

		// Free result
		$result_category->free();

		mysqli_close($db);
	}

function ExecutiveSummaryTotalWasteCategoryV2($id_company, $category) {

		$db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
		if($db->connect_errno > 0){ die('Unable to connect to database [' . $db->connect_error . ']');}

		// Content
		$sql_category = "SELECT SUM(weight) FROM lbc_rf3 RIGHT JOIN lbc_kitchens ON lbc_rf3.kitchen = lbc_kitchens.kitchen
		WHERE lbc_rf3.id_company = '$id_company' AND lbc_kitchens.id_company = '$id_company' AND lbc_rf3.type_waste = '$category'";
		if(!$result_category = $db->query($sql_category)){ die('There was an error running the query [' . $db->error . ']');}

		$weight = mysqli_fetch_array($result_category); echo number_format($weight[0], 1,'.','');
		$result_category->free();

		mysqli_close($db);
	}

?>

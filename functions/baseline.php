<?php
  /* Includes */
  include('../mysqli.php');

  /* Parameters */
  date_default_timezone_set("Asia/Bangkok");
  $id_company = $_SESSION['id_company'];
  $baseline_start = $_SESSION['baseline_start'];
  $baseline_end = $_SESSION['baseline_end'];

  /* Total food waste during baseline */
  $sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company' AND date_waste BETWEEN '$baseline_start' AND '$baseline_end'";
  $result = $db->query($sql);
  $row = mysqli_fetch_assoc($result);

  $_SESSION['fw_baseline'] = $row['SUM(weight)'];

  /* Total covers during baseline */
  $sql = "SELECT SUM(covers) FROM lbc_covers WHERE id_company = '$id_company' AND date_waste BETWEEN '$baseline_start' AND '$baseline_end'";
  $result = $db->query($sql);
  $row = mysqli_fetch_assoc($result);

  $_SESSION['covers_baseline'] = $row['SUM(covers)'];

  /* Food waste per cover during baseline */
  $_SESSION['fw_covers_baseline'] = $_SESSION['fw_baseline'] / $_SESSION['covers_baseline'];

  /* Total food waste after baseline */
  $sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company' AND date_waste > '$baseline_end'";
  $result = $db->query($sql);
  $row = mysqli_fetch_assoc($result);

  $_SESSION['fw_after_baseline'] = $row['SUM(weight)'];

  /* Total covers after baseline */
  $sql = "SELECT SUM(covers) FROM lbc_covers WHERE id_company = '$id_company' AND date_waste > '$baseline_end'";
  $result = $db->query($sql);
  $row = mysqli_fetch_assoc($result);

  $_SESSION['covers_after_baseline'] = $row['SUM(covers)'];

  /* Food waste per cover after baseline */
  $_SESSION['fw_covers_after_baseline'] = $_SESSION['fw_after_baseline'] / $_SESSION['covers_after_baseline'];

  /* Money saved after baseline */
  $_SESSION['money_saved'] = ($_SESSION['fw_covers_baseline'] - $_SESSION['fw_covers_after_baseline']) * $_SESSION['covers_after_baseline'] * $_SESSION['cost_kilo'];
  if($_SESSION['money_saved'] < 0) {$_SESSION['money_saved'] = 0;}

  /* Meals rescued */
  $_SESSION['meals_rescued'] = ($_SESSION['fw_covers_baseline'] - $_SESSION['fw_covers_after_baseline']) * $_SESSION['covers_after_baseline'] * 3;
  if($_SESSION['meals_rescued'] < 0) {$_SESSION['meals_rescued'] = 0;}

  /* Carbon reduction */
  $_SESSION['carbon_reduction'] = ($_SESSION['fw_covers_baseline'] - $_SESSION['fw_covers_after_baseline']) * $_SESSION['covers_after_baseline'] * 2.5;
  if($_SESSION['carbon_reduction'] < 0) {$_SESSION['carbon_reduction'] = 0;}

  /* Redirection */
  header("Location: variables.php");

?>

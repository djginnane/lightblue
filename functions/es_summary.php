<?php
    /* Timezone */
    date_default_timezone_set("Asia/Bangkok");

    /* Session */
    session_start();

function AverageDailyFoodWaste() {

    /* Connect to database */
    $db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
    if ($db->connect_error) { die("Connection failed: " . $db->connect_error);}

    /* Variables */
    $id_company = $_SESSION['id_company'];
    $baseline_start = $_SESSION['baseline_start'];
		$baseline_end = $_SESSION['baseline_end'];

    /* No. of days */
    $date_start = date_create($baseline_start);
    $date_end = date_create($baseline_end);

    $days = date_diff($date_start,$date_end);
    $days = $days->format("%a"); $days = $days + 1;

		/* Query */
		$sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company' AND date_waste BETWEEN '$baseline_start' AND '$baseline_end'";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result);

    $average = $weight[0] / $days;
		echo number_format($average,0,".",",");
}

function TotalFoodWaste() {
    /* Connect to database */
    $db = new mysqli('localhost', 'lightblue', '2VssGVEmUWG8', 'lightblue');
    if ($db->connect_error) { die("Connection failed: " . $db->connect_error);}

    /* Variables */
    $id_company = $_SESSION['id_company'];

		/* Query */
		$sql = "SELECT SUM(weight) FROM lbc_rf3 WHERE id_company = '$id_company'";
		$result = $db->query($sql); $weight = mysqli_fetch_array($result);

		echo number_format($weight[0],0,".",",");
}
?>

<?php
    /* Includes */
    include('../mysqli.php');
   // include('../signin/database.php');

    /* Parameters */
    date_default_timezone_set("Asia/Bangkok");
    $id_company = $_SESSION['id_company'];

    /* Total pre-consumer food waste */
    $sql = "SELECT SUM(weight) AS fw_preconsumer FROM lbc_rf3 WHERE id_company = '$id_company' AND (type_waste = 'spoilage' OR type_waste = 'preparation')";
    $result = $db->query($sql);
    $row = mysqli_fetch_assoc($result);

    $_SESSION['fw_preconsumer'] = $row['fw_preconsumer'];

    /* Total food waste */
    $sql = "SELECT SUM(weight) AS fw_total FROM lbc_rf3 WHERE id_company = '$id_company'";
    $result = $db->query($sql);
    $row = mysqli_fetch_assoc($result);

    $_SESSION['fw_total'] = $row['fw_total'];

    /* Percentage of pre-consumer food waste */
    $_SESSION['fw_preconsumer_percent'] = number_format(($_SESSION['fw_preconsumer'] / $_SESSION['fw_total']) * 100, 2).' %';

    /* Redirection */
    header("Location: outlets.php");
 ?>

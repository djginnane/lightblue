<?php

  /* Timezone */
  date_default_timezone_set("Asia/Bangkok");

  /* Includes */
  include('../mysqli.php');

  /* Variables */
  $id_company = $_SESSION['id_company'];


  /* Create kitchens types arrays */
  $sql = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
  $result = $db->query($sql);

  $_SESSION['kitchens_all'] = array();
  $_SESSION['kitchens_outlets'] = array();
  $_SESSION['kitchens_canteen'] = array();
  $_SESSION['kitchens_banquet'] = array();

  while($row = $result->fetch_assoc()) {
    if($row['type'] == 'Outlet'){
      array_push($_SESSION['kitchens_outlets'], $row['kitchen']);
      array_push($_SESSION['kitchens_all'], $row['kitchen']);
    }
    elseif($row['type'] == 'Canteen'){
      array_push($_SESSION['kitchens_canteen'], $row['kitchen']);
      array_push($_SESSION['kitchens_all'], $row['kitchen']);
    }
    elseif($row['type'] == 'Banquet'){
      array_push($_SESSION['kitchens_banquet'], $row['kitchen']);
      array_push($_SESSION['kitchens_all'], $row['kitchen']);
    }
  }

  /* Create list of days arrays */
  $sql = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company' GROUP BY date_waste ORDER BY date_waste ASC";
  $result = $db->query($sql);

  $_SESSION['days_all'] = array();

  while($row = $result->fetch_assoc()) {
    array_push($_SESSION['days_all'], $row['date_waste']);
  }

  /* Daily waste per kitchen */
  $sql = "SELECT * FROM lbc_rf3 WHERE id_company = '$id_company'";
  $result = $db->query($sql);

  while($row = $result->fetch_assoc()) {
    $date_waste = date("Y-m-d", strtotime($row['date_waste']));
    $date_kitchen = md5($row['kitchen'].$date_waste);
    $total_kitchen = md5($row['kitchen']);

    if(isset($_SESSION[$date_kitchen])){
      array_push($_SESSION[$date_kitchen], $row['weight']);
    } else {
      $_SESSION[$date_kitchen] = array();
      array_push($_SESSION[$date_kitchen], $row['weight']);
    }

    if(isset($_SESSION[$total_kitchen])){
      array_push($_SESSION[$total_kitchen], $row['weight']);
    } else {
      $_SESSION[$total_kitchen] = array();
      array_push($_SESSION[$total_kitchen], $row['weight']);
    }
  }

  //echo '<pre>';
  //var_dump($_SESSION);
  //echo '</pre>';

  /* Redirection */
  header("Location: ../index.php");

 ?>

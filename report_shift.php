<? include 'database.php'; $id_user = $_SESSION['username']; ?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include ('gtag.php'); ?>

    </head>
    <body>

       <? include 'header.php';?>

       <section id="main">

           <? include 'sidebar.php'; sideBar('executivesummary','report_shift'); ?>

            <section id="content">
                <div class="container">

                     <div class="block-header">
                     <h2>Food waste by shift</h2>
                     </div>

                    <div class="card">
                    <iframe height="400px" width="100%" src="chart.php?type=category_shift" frameborder="0"></iframe>
                    </div>

                   <!--<div class="card">
                        <div class="card-header">
                            <h2>Total Food Waste<small>Spoilage, Prep, plate and buffet wastes</small></h2>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Recording point</th>
                                        <th>Total waste</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>Fresh in the garden</td>
                                        <td>56.58 kg</td>
                                    </tr>
                                    <tr>
                                        <td>By the beach</td>
                                        <td>45.12 kg</td>
                                    </tr>
                                    <tr>
                                        <td>Bar(a) Bara</td>
                                        <td>14.81 kg</td>
                                    </tr>
                                    <tr>
                                        <td>Main kitchen</td>
                                        <td>433.17 kg</td>
                                    </tr>
                                    <tr>
                                        <td>Malafaaiy</td>
                                        <td>523.08 kg</td>
                                    </tr>
                                    <tr>
                                        <td>Cajun City</td>
                                        <td>935.2 kg</td>
                                    </tr>
                                    <tr>
                                        <td>Staff Bar</td>
                                        <td>0.84 kg</td>
                                    </tr>

                                     <tr>
                                        <td><strong>All</strong></td>
                                        <td><strong>2008.8 kg</strong></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                  </div>-->


                </div>
            </section>
        </section>

        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

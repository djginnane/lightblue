<? include 'database.php'; $id_kitchen = $_GET['id_kitchen'];

// Load list of users from this company
	$query = "SELECT * FROM lbc_kitchens WHERE id_kitchen = '$id_kitchen' ORDER BY kitchen ASC";
	$kitchen = mysql_query($query, $db) or die(mysql_error());
	$row_kitchen = mysql_fetch_assoc($kitchen);
	$numberRowsKitchen  = mysql_num_rows($kitchen);
?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

				<!-- Google Analytics -->
				<?php include ('gtag.php'); ?>

    </head>
    <body>

	<?php include 'header.php';?>

        <section id="main">
          <?php include 'sidebar.php'; sideBar('settings','manage_kitchens'); ?>

            <section id="content">
                <div class="container">

                    <div class="block-header">
                        <h2>Update recording station / kitchen</h2>

                        <ul class="actions">
                            <li>
                                <a onclick="return confirm('Are you sure you want to delete <?php echo $row_kitchen['kitchen']; ?>?');" href="sql_kitchen_delete.php?id_company=<? echo $row_kitchen['id_company']; ?>&id_kitchen=<? echo $row_kitchen['id_kitchen']; ?>">
                                    <i class="zmdi zmdi-delete"></i>
                                </a>
                            </li>
                        </ul>

                    </div>


									<form action="sql_kitchen_update.php" method="post" id="updatekitchen">

									<input type="hidden" name="id_kitchen" value="<?php echo $row_kitchen['id_kitchen']; ?>">
									<input type="hidden" name="id_company" value="<?php echo $row_kitchen['id_company']; ?>">
									<input type="hidden" name="original_kitchen" value="<?php echo $row_kitchen['kitchen']; ?>">

									<div class="card">
										<div class="card-body card-padding">
										<div class="row">

											<div class="col-sm-6">
												<div class="form-group fg-float">
													<div class="fg-line"><input type="text" class="input-sm form-control fg-input" name="kitchen" value="<?php echo $row_kitchen['kitchen']; ?>" style="color:black;"></div>
												</div>
											</div>

											<div class="col-sm-6">
												<div class="form-group">
													<div class="select">
														<select class="form-control" name="type">
															<option value="Outlet" <?php if ($row_kitchen['type'] == "Outlet") {echo "selected";} ?> disabled>Outlet</option>
															<option value="Canteen" <?php if ($row_kitchen['type'] == "Canteen") {echo "selected";} ?> disabled>Canteen</option>
															<option value="Banquet" <?php if ($row_kitchen['type'] == "Banquet") {echo "selected";} ?> disabled>Banquet</option>
														</select>
													</div>
												</div>
											</div>

										</div>
										</div>

									</div>

									<button type="submit" class="btn btn-primary btn-block bgm-lightblue btn-icon-text"><i class="zmdi zmdi-check-all"></i>Update</button>
									</form>

                </div>
            </section>
        </section>

        <footer id="footer">
            &copy; Copyright 2014 - 2017 LightBlue Environmental Consulting. All rights reserved.
        </footer>

        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

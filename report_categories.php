<? include 'database.php'; $id_user = $_SESSION['username']; ?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

        <!-- Google Analytics -->
        <?php include ('gtag.php'); ?>

    </head>
    <body>

       <? include 'header.php';?>

       <section id="main">

           <? include 'sidebar.php'; sideBar('executivesummary','report_categories'); ?>

            <section id="content">
                <div class="container">

                     <div class="block-header">
                     <h2>Food waste per category</h2>
                     </div>

                    <div class="card">
                    <iframe height="400px" width="100%" src="chart.php?type=category_categories" frameborder="0"></iframe>
                    </div>

                   <div class="card">
                        <div class="card-header">
                            <h2>Waste type<small>Spoilage, Prep, plate and buffet wastes</small></h2>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Days</th>
                                        <th align="center">Spoilage (KG)</th>
                                        <th align="center">Prep Waste (KG)</th>
                                        <th align="center">Plate Waste (KG)</th>
                                        <th align="center">Buffet (KG)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Day 1</td>
                                        <td align="center">11.12</td>
                                        <td align="center">15.1</td>
                                        <td align="center">18.25</td>
                                        <td align="center">15.37</td>
                                    </tr>
                                     <tr>
                                        <td>Day 2</td>
                                        <td align="center">11.94</td>
                                        <td align="center">12.46</td>
                                        <td align="center">16.82</td>
                                        <td align="center">9.3</td>
                                    </tr>
                                     <tr>
                                        <td>Day 3</td>
                                        <td align="center">16.15</td>
                                        <td align="center">14.89</td>
                                        <td align="center">21.43</td>
                                        <td align="center">10.55</td>
                                    </tr>
                                     <tr>
                                        <td>Day 4</td>
                                        <td align="center">11.8</td>
                                        <td align="center">31.34</td>
                                        <td align="center">44.542</td>
                                        <td align="center">15.14</td>
                                    </tr>
                                     <tr>
                                        <td>Day 5</td>
                                        <td align="center">21.56</td>
                                        <td align="center">24.36</td>
                                        <td align="center">33.08</td>
                                        <td align="center">5.92</td>
                                    </tr>
                                     <tr>
                                        <td>Day 6</td>
                                        <td align="center">21.14</td>
                                        <td align="center">34.6</td>
                                        <td align="center">30.93</td>
                                        <td align="center">5.07</td>
                                    </tr>
                                     <tr>
                                        <td>Day 7</td>
                                        <td align="center">7.54</td>
                                        <td align="center">16.62</td>
                                        <td align="center">28.04</td>
                                        <td align="center">44.72</td>
                                    </tr>
                                     <tr>
                                        <td><strong>Total (KG)</strong></td>
                                        <td align="center"><strong>101.25</strong></td>
                                        <td align="center"><strong>149.27</strong></td>
                                        <td align="center"><strong>193.092</strong></td>
                                        <td align="center"><strong>106.07</strong></td>
                                    </tr>
                                     <tr>
                                        <td><strong>Total in %</strong></td>
                                        <td align="center"><strong>18.4%</strong></td>
                                        <td align="center"><strong>27.2%</strong></td>
                                        <td align="center"><strong>35.1%</strong></td>
                                        <td align="center"><strong>19.3%</strong></td>
                                    </tr>
                                     <tr>
                                        <td><strong>Daily Av. (KG)</strong></td>
                                       <td align="center"><strong>14.46</strong></td>
                                       <td align="center"><strong>21.32</strong></td>
                                       <td align="center"><strong>27.57</strong></td>
                                       <td align="center"><strong>15.15</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                  </div>


                </div>
            </section>
        </section>

        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

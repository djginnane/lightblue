<? include 'database.php'; $id_user = $_SESSION['username']; $id_company = $_SESSION['id_company'];

// Load list of kitchens from this company
	$query_kitchen = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
	$kitchen = mysql_query($query_kitchen, $db) or die(mysql_error());
	$row_kitchen = mysql_fetch_assoc($kitchen);
	$numberRowsKitchen  = mysql_num_rows($kitchen);

// Load list of kitchens from this company
	$query_kitchen_copy = "SELECT * FROM lbc_kitchens WHERE id_company = '$id_company' ORDER BY kitchen ASC";
	$kitchen_copy = mysql_query($query_kitchen_copy, $db) or die(mysql_error());
	$row_kitchen_copy = mysql_fetch_assoc($kitchen_copy);
	$numberRowsKitchen_copy  = mysql_num_rows($kitchen_copy);

// Load list of users from this company
	$query_users = "SELECT * FROM lbc_users WHERE id_company = '$id_company' ORDER BY username ASC";
	$users = mysql_query($query_users, $db) or die(mysql_error());
	$row_users = mysql_fetch_assoc($users);
	$numberRowsUsers  = mysql_num_rows($users);

?>

<? // Page loaded after search

if ($_POST['search'] == "true") {

	if($_POST['database'] != "") {$database = $_POST['database'];} else {$database = "NULL";}

	if($_POST['date_waste_from'] != "") {$date_waste_from = $_POST['date_waste_from'];} else {$date_waste_from = "NULL";}
	if($_POST['date_waste_to'] != "") {$date_waste_to = $_POST['date_waste_to'];} else {$date_waste_to = "NULL";}
	if($_POST['kitchen'] != "") {$kitchen = $_POST['kitchen'];} else {$kitchen = "NULL";}
	if($_POST['shift'] != "") {$shift = $_POST['shift'];} else {$shift = "NULL";}
	if($_POST['id_user'] != "") {$id_user = $_POST['id_user'];} else {$id_user = "NULL";}
	if($_POST['type_waste'] != "") {$type_waste = $_POST['type_waste'];} else {$type_waste = "NULL";}

	if($_POST['type_food'] != "") {$type_food = $_POST['type_food'];} else {$type_food = "NULL";}
	if($_POST['reason_waste'] != "") {$reason_waste = $_POST['reason_waste'];} else {$reason_waste = "NULL";}
	if($_POST['origin_waste'] != "") {$origin_waste = $_POST['origin_waste'];} else {$origin_waste = "NULL";}

}

?>

<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Food Excess Monitoring | LightBlue Environment Consulting</title>

         <!-- Vendor CSS -->
        <link href="scripts/vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">

         <link href="scripts/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.min.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
        <link href="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="scripts/vendors/farbtastic/farbtastic.css" rel="stylesheet">
        <link href="scripts/vendors/chosen_v1.4.2/chosen.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="scripts/css/app.min.1.css" rel="stylesheet">
        <link href="scripts/css/app.min.2.css" rel="stylesheet">

				<!-- Google Analytics -->
				<?php include ('gtag.php'); ?>

    </head>
    <body>

       <? include 'header.php';?>

       <section id="main">

           <? include 'sidebar.php'; sideBar('executivesummary','report_demand'); ?>

            <section id="content">
                <div class="container">




 			<div class="block-header">
                     <h2>Report on demand</h2>

										 <ul class="actions">
                        <li>
                            <a href="#" onclick="window.print();">
                            <i class="zmdi zmdi-print"></i>
                            </a>
                        </li>
                     </ul>
            </div>

              <div class="card">
<form action="report_demand.php" method="post" id="ondemand">

<input type="hidden" name="search" value="true">

                        <div class="card-body card-padding">

                            <div class="row">
                             <div class="col-sm-4">

<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-dns"></i></span>
<div class="fg-line select">
<select class="form-control" name="database">
<option value="lbc_rf1" <? if($database == "lbc_rf1") {echo "selected";} ?>>RF1 - Waste records</option>
<!--<option value="lbc_rf4" <? //if($database == "lbc_rf4") {echo "selected";} ?>>RF3 - Banqueting</option>-->
<option value="lbc_rf3" <? if(($database == "lbc_rf3") || ($database == "NULL") || ($database == "")){echo "selected";} ?>>RF4 - Stewards</option>
<option value="lbc_covers" <? if($database == "lbc_covers") {echo "selected";} ?>>Covers</option>
</select>
</div>
</div>

                            </div>


                                <div class="col-sm-4">

                                  <div class="input-group form-group" >
                                  <span class="input-group-addon"><i class="zmdi zmdi-calendar-alt"></i></span>
                                  <div class="dtp-container fg-line">
     							  <input type='text' class="form-control date-picker" placeholder="From" id="date_waste_from" name="date_waste_from" value="<? if(($date_waste_from != "")||($date_waste_from != "NULL")) {echo $date_waste_from;} ?>">
                                  </div>
                                  </div>

                                </div>

                                 <div class="col-sm-4">

                                  <div class="input-group form-group" >
                                  <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                                  <div class="dtp-container fg-line">
     							  <input type='text' class="form-control date-picker" placeholder="To" id="date_waste_to" name="date_waste_to" value="<? if(($date_waste_to != "")||($date_waste_to != "NULL")) {echo $date_waste_to;} ?>">
                                  </div>
                                  </div>

                                </div>
                            </div>
                            <div class="row">

                            <div class="col-sm-4">


<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-cutlery"></i></span>
<div class="fg-line select">

<? if ($_POST['search'] == "true") { ?>

    <select class="form-control" name="kitchen">
    <option <? if(($_POST['kitchen'] == "")||($_POST['kitchen'] == "NULL")) { echo "selected";} ?> value="NULL">All recording stations</option>
    <? if($numberRowsKitchen_copy != "0") { $n=1; do { ?>
    <option <? if($_POST['kitchen'] == $row_kitchen_copy['kitchen']) {echo "selected";} ?> value="<? echo $row_kitchen_copy['kitchen']; ?>"><? echo $row_kitchen_copy['kitchen']; ?></option>
    <? $n++; } while ($row_kitchen_copy = mysql_fetch_assoc($kitchen_copy)); }?>
    </select>

<? } else { ?>

    <select class="form-control" name="kitchen">
    <option <? if(($_POST['kitchen'] == "")||($_POST['kitchen'] == "NULL")) { echo "selected";} ?> value="NULL">All recording stations</option>
    <? if($numberRowsKitchen != "0") { $n=1; do { ?>
    <option <? if($_POST['kitchen'] == $row_kitchen['kitchen']) {echo "selected";} ?> value="<? echo $row_kitchen['kitchen']; ?>"><? echo $row_kitchen['kitchen']; ?></option>
    <? $n++; } while ($row_kitchen = mysql_fetch_assoc($kitchen)); }?>
    </select>

<? }?>
</div>
</div>

                            </div>

                            <div class="col-sm-4">

<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
<div class="fg-line select">
<select class="form-control" name="shift">
<option <? if(($shift == "") || ($shift == "NULL")) { echo "selected";} ?> value="NULL">All shifts</option>
<option <? if($shift == "Breakfast") {echo "selected";} ?> value="Breakfast">Breakfast</option>
<option <? if($shift == "Lunch") {echo "selected";} ?> value="Lunch">Lunch</option>
<option <? if($shift == "Dinner") {echo "selected";} ?> value="Dinner">Dinner</option>
<option <? if($shift == "Night shift") {echo "selected";} ?> value="Night shift">Night shift</option>
</select>
</div>
</div>

                            </div>

                           <!-- <div class="col-sm-3">


<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-accounts"></i></span>
<div class="fg-line select">
<select class="form-control" name="id_user">
<option selected value="NULL">All users</option>
<? //if($numberRowsUsers != "0") { $n=1; do { ?>
<option value="<? //echo $row_users['username']; ?>"><? //echo $row_users['username']; ?></option>
<? // $n++; } while ($row_users = mysql_fetch_assoc($users)); }?>
</select>
</div>
</div>

                            </div>-->

                                      <div class="col-sm-4">
<div class="input-group form-group">
<span class="input-group-addon"><i class="zmdi zmdi-label"></i></span>
<div class="fg-line select">
<select class="form-control" name="type_waste">
<option <? if(($type_waste == "") || ($type_waste == "NULL")) { echo "selected";} ?> value="NULL">All categories</option>
<option <? if($type_waste == "spoilage") {echo "selected";} ?> value="spoilage">Spoilage</option>
<option <? if($type_waste == "preparation") {echo "selected";} ?> value="preparation">Preparation</option>
<option <? if($type_waste == "plate") {echo "selected";} ?> value="plate">Plate</option>
<option <? if($type_waste == "buffet") {echo "selected";} ?> value="buffet">Buffet</option>
<!--<option <? // if($type_waste == "non_edible") {echo "selected";} ?> value="non_edible">Non edible</option>-->
</select>
</div>
</div>

                            </div>


                            </div>

                            <!--<div class="row">


                               <div class="col-sm-4">

  <p class="c-black f-500">Type of food</p>
  <div class="form-group">
    <div class="fg-line">
    <input type="text" class="form-control" placeholder="eg. Greek yogurt" name="type_food" value="">
    </div>
  </div>

                            </div>

                                        <div class="col-sm-4">

  <p class="c-black f-500">Reason for waste</p>
  <div class="form-group">
    <div class="fg-line">
    <input type="text" class="form-control" placeholder="eg. Out of date" name="reason_waste" value="">
    </div>
  </div>

                            </div>

                                       <div class="col-sm-4">

  <p class="c-black f-500">Origin of waste</p>
  <div class="form-group">
    <div class="fg-line">
    <input type="text" class="form-control" placeholder="eg. Cold kitchen" name="origin_waste" value="">
    </div>
  </div>

                            </div>

                           </div>-->
<div align="center" id="update_button">
<button type="submit" class="btn btn-default btn-icon-text bgm-lightblue waves-effect">
<i class="zmdi zmdi-refresh"></i>Load
</button>
</div>


                        </div>
  </form>
					</div>



<? if ($_POST['search'] ==  "true") { ?>
<div class="block-header">
                     <h2>Results</h2>

                     <ul class="actions">
                     <li>

                        <? if($database == "lbc_covers") { ?>

                        <a href="report_demand_excel_covers.php?database=<? echo $database; ?>&date_waste_from=<? echo $date_waste_from; ?>&date_waste_to=<? echo $date_waste_to; ?>&kitchen=<? echo $kitchen; ?>&shift=<? echo $shift; ?>&id_user=<? echo $id_user; ?>">

                        <? } else { ?>

                        <a href="report_demand_excel.php?database=<? echo $database; ?>&date_waste_from=<? echo $date_waste_from; ?>&date_waste_to=<? echo $date_waste_to; ?>&kitchen=<? echo $kitchen; ?>&shift=<? echo $shift; ?>&id_user=<? echo $id_user; ?>&type_waste=<? echo $type_waste; ?>&type_food=<? echo $type_food; ?>&reason_waste=<? echo $reason_waste; ?>&origin_waste=<? echo $origin_waste; ?>">


                        <? } ?>

                                    <i class="zmdi zmdi-download"></i>
                                </a>


                            </li>

                           <!--<li>
                                <a href="report_demand.php">
                                    <i class="zmdi zmdi-search"></i>
                                </a>
                            </li>-->

                        </ul>
</div>
               <div class="card-body card-padding">

                   <div class="card-body table-responsive">



                  <?

				  if($database == "lbc_covers") {

					include 'function_ondemand_covers.php';
				  onDemand($database,$date_waste_from,$date_waste_to,$kitchen,$shift,$id_user);
				}

				  else {

				  include 'function_ondemand.php';
				  onDemand($database,$date_waste_from,$date_waste_to,$kitchen,$shift,$id_user,$type_waste,$type_food,$reason_waste,$origin_waste, 'no');

				  }?>



                        </div>


                  </div>
<? } ?>
                </div>
            </section>
        </section>

        <!-- Javascript Libraries -->
        <script src="scripts/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="scripts/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script src="scripts/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="scripts/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="scripts/vendors/bower_components/autosize/dist/autosize.min.js"></script>

        <script src="scripts/vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="scripts/vendors/bower_components/flot/jquery.flot.pie.js"></script>
        <script src="scripts/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>
        <script src="scripts/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="scripts/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js"></script>

        <script src="scripts/vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="scripts/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
        <script src="scripts/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js"></script>
        <script src="scripts/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        <script src="scripts/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <script src="scripts/vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js"></script>

        <script src="scripts/vendors/chosen_v1.4.2/chosen.jquery.min.js"></script>
        <script src="scripts/vendors/fileinput/fileinput.min.js"></script>
        <script src="scripts/vendors/input-mask/input-mask.min.js"></script>
        <script src="scripts/vendors/farbtastic/farbtastic.min.js"></script>

        <script src="scripts/js/functions.js"></script>

    </body>
  </html>

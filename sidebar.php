<? function sideBar($submenu, $sublist) { ?>
 <aside id="sidebar">
  <div class="sidebar-inner c-overflow">

  <div style="background: rgba(0, 0, 0, 0.37); padding:10px; padding-left:20px; color: #fff;position: relative;">
   <strong><? echo $_SESSION['name']; ?></strong><br>
	<? echo $_SESSION['company']; ?>
   </div>

      <ul class="main-menu">


      <li <? if($submenu == "home") { echo 'class="active"';} ?>>
       	<a href="index.php"><i class="zmdi zmdi-home"></i> Home</a>
      </li>

         <? if(($_SESSION['WRRF1'] == "yes") || ($_SESSION['WRRF2']== "yes") || ($_SESSION['WRRF3']== "yes") || ($_SESSION['WRRF4']== "yes") || ($_SESSION['WRCovers']== "yes")){?>
          <li class="sub-menu <? if($submenu == "wasterecords") { echo "active toggled";} ?>">
              <a href="#"><i class="zmdi zmdi zmdi-delete"></i>Waste records</a>

               <ul>
      <? if($_SESSION['WRRF1'] == "yes"){?>
      <li><a <? if($sublist == "rf1") { echo 'class="active"';} ?> href="add_rf1.php" >RF1 - Waste records</a></li>
      <? } ?>

      <!--
      <? // if($_SESSION['WRRF2'] == "yes"){?>
      <li><a <? //if($sublist == "rf2") { echo 'class="active"';} ?> href="add_rf2.php">RF2 - Champions</a></li>
      <? // } ?>

      <? // if($_SESSION['WRRF3'] == "yes"){?>
      <li><a <? //if($sublist == "rf4") { echo 'class="active"';} ?> href="add_rf4.php">RF3 - Banqueting</a></li>
      <? // } ?>
    -->

      <? if($_SESSION['WRRF4'] == "yes"){?>
      <li><a <? if($sublist == "rf3") { echo 'class="active"';} ?> href="add_rf3.php">RF4 - Stewards</a></li>
      <? } ?>

      <? if($_SESSION['SETSimplifiedRF4'] == "yes"){?>
      <li><a <? if($sublist == "rf3Simplified") { echo 'class="active"';} ?> href="add_rf3_simplified.php">RF4 - Simplified</a></li>
      <? } ?>

      <? if($_SESSION['WRCovers'] == "yes"){?>
      <li><a <? if($sublist == "addcovers") { echo 'class="active"';} ?> href="add_covers.php">Add covers</a></li>
      <? } ?>
              </ul>

          </li>
           <? } ?>
          <? if(($_SESSION['ESSummary'] == "yes") || ($_SESSION['ESOutlets']== "yes") || ($_SESSION['ESCanteens']== "yes") || ($_SESSION['ESBanquet']== "yes") || ($_SESSION['ESOndemand']== "yes")){?>
          <li class="sub-menu <? if($submenu == "executivesummary") { echo "active toggled";} ?>">
              <a href=""><i class="zmdi zmdi-trending-up"></i> Executive Summary</a>

             <ul>
             <? if($_SESSION['ESSummary'] == "yes"){?>
              <li><a <? if($sublist == "report_total") { echo 'class="active"';} ?> href="report_total.php">Summary</a></li>
              <? } ?>

             <? if($_SESSION['ESOutlets'] == "yes"){?>
              <li><a <? if($sublist == "report_outlet") { echo 'class="active"';} ?> href="report_outlet.php?tab=recordingpoint">Outlets</a></li>
              <? } ?>

              <? if($_SESSION['ESCanteens'] == "yes"){?>
              <li><a <? if($sublist == "report_canteens") { echo 'class="active"';} ?> href="report_canteens.php?tab=shift" >Canteens</a></li>
              <? } ?>

              <? if($_SESSION['ESBanquet'] == "yes"){?>
              <li><a <? if($sublist == "report_banquet") { echo 'class="active"';} ?> href="report_banquet.php?tab=shift" >Banquet</a></li>
              <? } ?>

              <? if($_SESSION['ESOndemand'] == "yes"){?>
              <li><a <? if($sublist == "report_demand") { echo 'class="active"';} ?> href="report_demand.php" >On demand</a></li>
              <? } ?>

			 </ul>
          </li>
           <? } ?>

            <? if(($_SESSION['SETSimplifiedES'] == "yes")){?>

          <li class="sub-menu <? if($submenu == "simplifiedSumary") { echo "active toggled";} ?>">
              <a href=""><i class="zmdi zmdi-trending-up"></i> Simplified Summary</a>

             <ul>

              <li><a <? if($sublist == "report_total") { echo 'class="active"';} ?> href="report_total_simplified.php">Summary</a></li>

              <!--<li><a <? if($sublist == "report_outlet") { echo 'class="active"';} ?> href="report_outlet_simplified.php?tab=recordingpoint">Outlets</a></li>

              <li><a <? if($sublist == "report_canteens") { echo 'class="active"';} ?> href="report_canteens_simplified.php?tab=shift" >Canteens</a></li>

              <li><a <? if($sublist == "report_banquet") { echo 'class="active"';} ?> href="report_banquet_simplified.php?tab=shift" >Banquet</a></li>

              <li><a <? if($sublist == "report_demand") { echo 'class="active"';} ?> href="report_demand_simplified.php" >On demand</a></li>-->


			 </ul>
          </li>
           <? } ?>


          <? if(($_SESSION['SETKitchens'] == "yes") || ($_SESSION['SETUsers']== "yes")){?>
          <li class="sub-menu <? if($submenu == "settings") { echo "active toggled";} ?>">
              <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>

              <ul>
                 <!-- <li><a href="#">Manage ingredients</a></li>
                  <li><a href="#">Manage waste reasons</a></li>-->

                   <? if($_SESSION['SETKitchens'] == "yes"){?>
                  <li><a <? if($sublist == "manage_kitchens") { echo 'class="active"';} ?> href="manage_kitchens.php">Manage Recording Stations/Kitchens</a></li>
                  <? } ?>

                   <? if(($_SESSION['SETTypeFood'] == "yes")&&($_SESSION['id_company'] != "lightblue")){?>
                  <li><a <? if($sublist == "type_food") { echo 'class="active"';} ?> href="manage_foodtypes.php">Manage type of food</a></li>
                  <? } ?>

                   <? if($_SESSION['SETUsers'] == "yes"){?>
                  <li><a <? if($sublist == "manage_users") { echo 'class="active"';} ?> href="manage_users.php">Manage users</a></li>
                  <? } ?>



              </ul>
          </li>
          <? } ?>


          <? if($_SESSION['id_company'] == "lightblue") { ?>
          <li class="sub-menu <? if($submenu == "admin") { echo "active toggled";} ?>">
              <a href=""><i class="zmdi zmdi-account-circle"></i> Admin</a>

              <ul>
                  <li><a <? if($sublist == "manage_accounts") { echo 'class="active"';} ?> href="manage_accounts.php">Manage accounts</a></li>

                  <? //if($_SESSION['SETTypeFood'] == "yes") {?>
                  <!-- <li><a <? if($sublist == "type_food") { echo 'class="active"';} ?> href="manage_foodtypes.php">Manage type of food</a></li> -->
                  <? //} ?>

                  <li><a href="http://projects.frogslab.co" target="new">Support</a></li>
              </ul>
          </li>
          <? } ?>


        <li <? if($submenu == "help") { echo 'class="active"';} ?>>
       	<a href="help.php"><i class="zmdi zmdi-help-outline"></i> Help</a>
      	</li>

      </ul>
  </div>
</aside>
 <? } ?>
